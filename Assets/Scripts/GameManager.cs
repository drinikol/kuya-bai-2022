using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Firestore;
using Firebase.Extensions;
using Google;
public enum GameMode
{
    INTRO,
    VOWEL,
    CONSONANT
}

public enum ThemeMode
{
    LIGHT,
    DARK
}
public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public List<BaybayinBase> vowelsBaybayin;
    public List<BaybayinBase> consonantsBaybayin;
    private BaybayinBase selectedBaybayin;
    GameMode selectedMode = GameMode.CONSONANT;
    ThemeMode selectedTheme = ThemeMode.LIGHT;
    [SerializeField]
    private int maxBaybayinIndex = 0;
    [SerializeField]
    private int currentBaybayinIndex = 0;
    //GOOGle
    private GoogleSignInConfiguration configuration;
    public string webClientId = "68672479529-g1o2abe2mfhvcv81vonh99sldhjartv4.apps.googleusercontent.com";
    //FIREBASE 
    Firebase.FirebaseApp firebaseApp;
    Firebase.Auth.FirebaseAuth firebaseAuth;
    FirebaseFirestore firebaseStore;
    Firebase.Auth.FirebaseUser firebaseUser;
    void Awake()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);

        configuration = new GoogleSignInConfiguration
        {
            WebClientId = webClientId,
            RequestIdToken = true
        };

    }
    void Start()
    {
        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            var dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                // Create and hold a reference to your FirebaseApp,
                // where app is a Firebase.FirebaseApp property of your application class.
                firebaseApp = Firebase.FirebaseApp.DefaultInstance;
                firebaseStore = FirebaseFirestore.DefaultInstance;
                firebaseAuth = Firebase.Auth.FirebaseAuth.DefaultInstance;
            }
            else
            {
                UnityEngine.Debug.LogError(System.String.Format(
                  "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                // Firebase Unity SDK is not safe to use here.
            }
        });


    }
    public GameMode GameMode
    {
        set { selectedMode = value; }
        get { return selectedMode; }
    }
    public ThemeMode ThemeMode
    {
        set { selectedTheme = value; }
        get { return selectedTheme; }
    }
    public List<BaybayinBase> ConsonantsBaybayin
    {
        get { return consonantsBaybayin; }
    }

    public List<BaybayinBase> VowelsBaybayin
    {
        get { return vowelsBaybayin; }
    }

    public int CurrentBaybayinIndex
    {
        set { currentBaybayinIndex = value; }
        get { return currentBaybayinIndex; }
    }
    public int MaxBaybayinIndex
    {
        set { maxBaybayinIndex = value; }
        get { return maxBaybayinIndex; }
    }
    public List<BaybayinBase> GetBaybayinBaseList()
    {
        List<BaybayinBase> sortedBaybayinBase = new List<BaybayinBase>();
        switch (GameMode)
        {
            case GameMode.VOWEL:
                {
                    sortedBaybayinBase = GameManager.instance.VowelsBaybayin;
                    break;
                }
            case GameMode.CONSONANT:
                {
                    sortedBaybayinBase = GameManager.instance.ConsonantsBaybayin;
                    break;
                }
        }
        MaxBaybayinIndex = sortedBaybayinBase.Count;
        return sortedBaybayinBase;
    }
    public BaybayinBase SelectedBaybayinBase
    {
        set { selectedBaybayin = value; }
        get { return selectedBaybayin; }
    }
    public Firebase.FirebaseApp FirebaseApp
    {
        get { return firebaseApp; }
    }

    public FirebaseFirestore FirebaseStore
    {
        get { return firebaseStore; }
    }

    public Firebase.Auth.FirebaseAuth FirebaseAuth
    {
        get { return firebaseAuth; }
    }

    public void HandleLoadedBaybayinList()
    {
        switch (GameMode)
        {
            case GameMode.VOWEL:
                {
                    SelectedBaybayinBase = vowelsBaybayin[currentBaybayinIndex];
                    MaxBaybayinIndex = vowelsBaybayin.Count;
                    break;
                }
            case GameMode.CONSONANT:
                {
                    SelectedBaybayinBase = consonantsBaybayin[currentBaybayinIndex];
                    MaxBaybayinIndex = consonantsBaybayin.Count;
                    break;
                }
        }
    }
    public void LoadNextBaybayin()
    {
        GameManager.instance.CurrentBaybayinIndex++;
        if (currentBaybayinIndex < maxBaybayinIndex)
        {
            HandleLoadedBaybayinList();
            LoadingManager.instance.LoadScene("LessonScreen");
        }
        else if (currentBaybayinIndex >= maxBaybayinIndex)
        {
            ResetLoadedData();
            LoadingManager.instance.LoadScene("MainMenuScreen");
        }
        else
        {
            Debug.Log("Something went wrong");
            ResetLoadedData();
        }
    }
    private void ResetLoadedData()
    {
        selectedBaybayin = null;
        selectedMode = GameMode.INTRO;
        currentBaybayinIndex = 0;
        maxBaybayinIndex = 0;
    }
    public void SetFirestore()
    {

        DocumentReference docRef = firebaseStore.Collection("users").Document("alovelace");
        Dictionary<string, object> user = new Dictionary<string, object>
{
        { "First", "Ada" },
        { "Last", "Lovelace" },
        { "Born", 1815 },
};
        docRef.SetAsync(user).ContinueWithOnMainThread(task =>
        {
            Debug.Log("Added data to the alovelace document in the users collection.");
        });
    }

    public void CreateUser(string email, string password)
    {
        firebaseAuth.CreateUserWithEmailAndPasswordAsync(email, password).ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                Debug.LogError("CreateUserWithEmailAndPasswordAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("CreateUserWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                return;
            }

            // Firebase user has been created.
            firebaseUser = task.Result;
            Debug.LogFormat("Firebase user created successfully: {0} ({1})",
                firebaseUser.DisplayName, firebaseUser.UserId);
        });
    }
    public void SignInWithEmail(string email, string password)
    {
        firebaseAuth.SignInWithEmailAndPasswordAsync(email, password).ContinueWith(task =>
         {
             if (task.IsCanceled)
             {
                 Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
                 return;
             }
             if (task.IsFaulted)
             {
                 Debug.LogError("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                 return;
             }

             // Firebase user has been created.
             firebaseUser = task.Result;
             Debug.LogFormat("Firebase user Logged In successfully: {0} ({1})",
                 firebaseUser.DisplayName, firebaseUser.UserId);
         });
    }
    public void SignInWithGoogle()
    {
        GoogleSignIn.DefaultInstance.SignIn().ContinueWith(task =>
         {
             if (task.IsFaulted)
             {
                 using (IEnumerator<System.Exception> enumerator =
                         task.Exception.InnerExceptions.GetEnumerator())
                 {
                     if (enumerator.MoveNext())
                     {
                         GoogleSignIn.SignInException error =
                                 (GoogleSignIn.SignInException)enumerator.Current;
                         Debug.Log("Got Error: " + error.Status + " " + error.Message);
                     }
                     else
                     {
                         Debug.Log("Got Unexpected Exception?!?" + task.Exception);
                     }
                 }
             }
             else if (task.IsCanceled)
             {
                 Debug.Log("Canceled");
             }
             else
             {
                 Debug.Log("Welcome: " + task.Result.DisplayName + "!");
             }
         });
    }
    public void SignInWithFacebook()
    {

    }

}
