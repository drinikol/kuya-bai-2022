using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class IntroManager : MonoBehaviour
{
    public ScrollRect scrollRect;
    public List<RectTransform> contentsRect = new List<RectTransform>();
    public TextMeshProUGUI contentTitle;
    public TextMeshProUGUI nextButtonText;
    public Button nextButton;
    List<string> contentTitles = new List<string>() { "What is Baybayin?", "Letters of Baybayin", "Vowels", "Vowels" };
    private int currentContentIndex = 0;
    void Start()
    {
        nextButton.onClick.AddListener(NextContent);
    }
    public void OpenLink()
    {
        Application.OpenURL("https://kuyabai.com/");
    }
    public void NextContent()
    {
        if (scrollRect)
        {
            currentContentIndex++;
            if (currentContentIndex < contentsRect.Count)
            {

                scrollRect.content = contentsRect[currentContentIndex];
                contentTitle.text = contentTitles[currentContentIndex];
                contentsRect[currentContentIndex].gameObject.SetActive(true);
                contentsRect[currentContentIndex - 1].gameObject.SetActive(false);
                if (currentContentIndex == contentsRect.Count - 1)
                {
                    nextButtonText.text = "Finish Lesson";
                    nextButton.onClick.RemoveListener(NextContent);
                    nextButton.onClick.AddListener(GoHome);
                }
            }

        }
    }
    public void GoHome()
    {
        LoadingManager.instance.LoadScene("MainMenuScreen");
    }
}
