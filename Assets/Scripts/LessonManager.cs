using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class LessonManager : MonoBehaviour
{
    public List<IllustrationsBase> vowelsBase;
    public List<IllustrationsBase> consonantBase;
    public IllustrationsBase selectedBase;
    [SerializeField]
    Image lessonLetterImageBG;
    [SerializeField]
    Image lessonLetterImage;
    [SerializeField]
    TextMeshProUGUI lessonLetterText;
    [SerializeField]
    GameObject lessonLetter;
    [SerializeField]
    GameObject lessonWordsContainer;
    [SerializeField]
    Image secondContainerImage;
    public Button nextButton;
    void Start()
    {
        SetupBaybayinLetter();
    }
    private void SetupBaybayinLetter()
    {
        lessonLetterText.text = GameManager.instance.SelectedBaybayinBase.baseName;
        lessonLetterImage.sprite = GameManager.instance.SelectedBaybayinBase.baseImage[1];
        selectedBase = vowelsBase[GameManager.instance.CurrentBaybayinIndex];
        nextButton.onClick.AddListener(LoadNextInfo);

    }
    private void LoadNextInfo()
    {

        nextButton.onClick.RemoveListener(LoadNextInfo);
        nextButton.onClick.AddListener(LoadNextLetter);
        lessonLetter.SetActive(false);
        lessonWordsContainer.SetActive(true);
        lessonLetterImage.transform.gameObject.SetActive(false);
        lessonLetterImageBG.sprite = selectedBase.illustrationSprite;
        secondContainerImage.sprite = selectedBase.illustrationTranslationImage;
        //hide first info
        //show next infor
    }
    private void LoadNextLetter()
    {
        //TODO check if increment now
        // GameManager.instance.CurrentBaybayinIndex++;

        //

        //LOAD to TRacing or quiz

        LoadingManager.instance.LoadScene("MultipleChoiceQuiz");
        // if (GameManager.instance.CurrentBaybayinIndex < GameManager.instance.MaxBaybayinIndex)
        // {
        //     nextButton.onClick.RemoveListener(LoadNextLetter);
        //     lessonLetter.SetActive(true);
        //     lessonWordsContainer.SetActive(false);
        //     SetupBaybayinLetter();
        // }
        // else
        // {
        //     Debug.Log("We are done");
        //     //Show done?
        // }
    }
}
