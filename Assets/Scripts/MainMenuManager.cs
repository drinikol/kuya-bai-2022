using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuManager : MonoBehaviour
{
    public void LoadIntro()
    {
        if (GameManager.instance)
        {
            GameManager.instance.GameMode = GameMode.INTRO;
            LoadingManager.instance.LoadScene("IntroScreen");
        }
    }
    public void LoadVowels()
    {
        if (GameManager.instance)
        {
            GameManager.instance.GameMode = GameMode.VOWEL;
            GameManager.instance.HandleLoadedBaybayinList();
            LoadingManager.instance.LoadScene("LessonScreen");
        }
    }
    public void LoadConsonants()
    {
        if (GameManager.instance)
        {
            GameManager.instance.GameMode = GameMode.CONSONANT;
            GameManager.instance.HandleLoadedBaybayinList();
            LoadingManager.instance.LoadScene("LessonScreen");
        }
    }
}
