using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "KuyaBai/Holder/ObjectsData")]
public class IllustrationsBase : ScriptableObject
{
    public Sprite illustrationSprite;
    public string illustrationName;
    public AudioClip illustrationVoice;
    //baybayin, tagalog, english sprite
    public Sprite illustrationTranslationImage;
}
