using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;
public class MultipleChoiceManager : MonoBehaviour
{
    public Image questionImage;
    public TextMeshProUGUI questionText;
    //holder of questions
    public List<MultipleChoiceScriptable> vowelsQuestionData;
    public List<MultipleChoiceScriptable> consonantsQuestionData;
    public List<TextMeshProUGUI> choicesText;
    private BaybayinBase selectedBase;
    [SerializeField]
    private List<string> baseNames = new List<string>();

    public Sprite correctAnswerSprite;
    public Sprite wrongAnswerSprite;
    void Start()
    {
        //Load baybayin base
        //create two random shit for choices
        List<BaybayinBase> tempList = new List<BaybayinBase>(GameManager.instance.VowelsBaybayin);
        foreach (BaybayinBase bases in tempList)
        {
            baseNames.Add(bases.baseName);
        }
        tempList = new List<BaybayinBase>(GameManager.instance.ConsonantsBaybayin);
        foreach (BaybayinBase bases in tempList)
        {
            baseNames.Add(bases.baseName);
        }


        if (GameManager.instance.SelectedBaybayinBase)
        {
            selectedBase = GameManager.instance.SelectedBaybayinBase;
        }
        else
        {
            Debug.Log("No base");
        }
        SetupQuiz();
    }
    public void SetupQuiz()
    {
        Shuffler.Shuffle(choicesText);

        questionImage.sprite = selectedBase.baseImage[1];


        for (int x = 0; x < 3; x++)
        {
            if (x == 0)
            {
                choicesText[x].text = selectedBase.baseName;
                baseNames.Remove(selectedBase.baseName);
            }
            else
            {
                int randomIdx = Random.Range(0, baseNames.Count - 1);
                Shuffler.Shuffle(baseNames);
                choicesText[x].text = baseNames[randomIdx];
                baseNames.RemoveAt(randomIdx);
            }
        }
    }
    public void CheckAnswer(TextMeshProUGUI value)
    {
        Image inputBox = value.transform.GetComponentInParent<Image>();
        if (value.text == selectedBase.baseName)
        {

            Timing.RunCoroutine(LoadNextLetter(inputBox).CancelWith(gameObject));
        }
        else
        {
            Timing.RunCoroutine(WrongAnswer(inputBox).CancelWith(gameObject));
        }
    }
    IEnumerator<float> LoadNextLetter(Image choiceImage)
    {
        choiceImage.sprite = correctAnswerSprite;
        yield return Timing.WaitForSeconds(1f);
        GameManager.instance.LoadNextBaybayin();
    }
    IEnumerator<float> WrongAnswer(Image choiceImage)
    {
        Sprite prevSprite = choiceImage.sprite;
        choiceImage.sprite = wrongAnswerSprite;
        yield return Timing.WaitForSeconds(0.25f);
        choiceImage.sprite = prevSprite;
    }
}
