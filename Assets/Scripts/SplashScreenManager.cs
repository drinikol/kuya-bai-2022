using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;
public class SplashScreenManager : MonoBehaviour
{
    public List<GameObject> sponsors = new List<GameObject>();
    void Start()
    {
        Timing.RunCoroutine(_ShowSponsors().CancelWith(gameObject));
    }

    IEnumerator<float> _ShowSponsors()
    {
        for (int x = 0; x < sponsors.Count; x++)
        {

            if (x == 0)
            {
                yield return Timing.WaitForSeconds(0.5f);
            }
            sponsors[x].SetActive(true);
            yield return Timing.WaitForSeconds(1.25f);
            sponsors[x].SetActive(false);
        }
        if (sponsors.Count == 0)
        {
            yield return Timing.WaitForSeconds(1.25f);
        }
        LoadingManager.instance.LoadScene("MainMenuScreen");
    }
}
