﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename R>
struct VirtualFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Action`1<UnityEngine.AsyncOperation>
struct Action_1_tE8693FF0E67CDBA52BAFB211BFF1844D076ABAFB;
// System.Action`1<System.Exception>
struct Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04;
// System.Action`1<System.Object>
struct Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87;
// System.Threading.AsyncLocal`1<System.Globalization.CultureInfo>
struct AsyncLocal_1_t1D3339EA4C8650D2DEDDF9553E5C932B3DC2CCFD;
// System.Collections.Generic.Dictionary`2<MEC.CoroutineHandle,System.Collections.Generic.HashSet`1<MEC.CoroutineHandle>>
struct Dictionary_2_t586C869F4A815C42AD5BEFA81A6BB784048C02BE;
// System.Collections.Generic.Dictionary`2<MEC.CoroutineHandle,System.Int32>
struct Dictionary_2_tDB458AE091EEF27A65EAF46DEB69B3AF739F3B51;
// System.Collections.Generic.Dictionary`2<MEC.CoroutineHandle,System.Object>
struct Dictionary_2_tA42ADF6FB18EDFFF7A9EFFECF8E63CA7E5AEF64E;
// System.Collections.Generic.Dictionary`2<MEC.CoroutineHandle,System.String>
struct Dictionary_2_t372C5C14CE2EA3086C9578E11977BF13E9F6F0DC;
// System.Collections.Generic.Dictionary`2<MEC.CoroutineHandle,MEC.Timing/ProcessIndex>
struct Dictionary_2_t291649C39F6CA5089BFF0B44E07C402F820DCD32;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.HashSet`1<MEC.CoroutineHandle>>
struct Dictionary_2_tFC47384FE9F6624AFC7658C39EF43A32B15FA8E3;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.HashSet`1<MEC.CoroutineHandle>>
struct Dictionary_2_t69E5ECF7FAD51BF19A125BD35B1923F4B8FD5611;
// System.Collections.Generic.Dictionary`2<MEC.Timing/ProcessIndex,MEC.CoroutineHandle>
struct Dictionary_2_t8C5B8A51DE825AB5FC3EDF04011A63CE33EBF2DB;
// System.Func`1<System.Boolean>
struct Func_1_t2BE7F58348C9CC544A8973B3A9E55541DE43C457;
// System.Func`2<System.Single,System.Single>
struct Func_2_t2A7432CC4F64D0DF6D8629208B154CF139B39AF2;
// System.Func`3<System.Collections.Generic.IEnumerator`1<System.Single>,MEC.CoroutineHandle,System.Collections.Generic.IEnumerator`1<System.Single>>
struct Func_3_tEDDCF49DDA3D0F9DCB147948F764E9514FC2EB04;
// System.Func`3<System.Object,MEC.CoroutineHandle,System.Object>
struct Func_3_t54E59EE3AB750BDCF167CEAAB996EAD80E127845;
// System.Collections.Generic.HashSet`1<MEC.CoroutineHandle>
struct HashSet_1_tAF8A7108F75B7F92D958C3E14539DF4EFB7BFB4E;
// System.Collections.Generic.IEnumerator`1<System.Single>
struct IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010;
// System.Collections.Generic.IEqualityComparer`1<MEC.CoroutineHandle>
struct IEqualityComparer_1_t56EEE600F2B450F7C493BBC17F1CA774B829F30F;
// System.Collections.Generic.IEqualityComparer`1<MEC.Timing/ProcessIndex>
struct IEqualityComparer_1_tE23D5A43B0C53BDA49AE5CB103E29FDD0E93B22B;
// System.Collections.Generic.Dictionary`2/KeyCollection<MEC.CoroutineHandle,System.Collections.Generic.HashSet`1<MEC.CoroutineHandle>>
struct KeyCollection_t9F7CB0FE2CB1909A2BC0176AF6E6BB3C6B49B1DE;
// System.Collections.Generic.Dictionary`2/KeyCollection<MEC.Timing/ProcessIndex,MEC.CoroutineHandle>
struct KeyCollection_t3EA0D9AC957940C6F0132DAFE6CA3860BC56EB39;
// System.Collections.Generic.Dictionary`2/ValueCollection<MEC.CoroutineHandle,System.Collections.Generic.HashSet`1<MEC.CoroutineHandle>>
struct ValueCollection_tF79182B406A6805FA9AA4D6ABFC74DA1A94BDC0C;
// System.Collections.Generic.Dictionary`2/ValueCollection<MEC.Timing/ProcessIndex,MEC.CoroutineHandle>
struct ValueCollection_tE89CCD430D99C87F0846B8232435C433AAF2E394;
// System.Collections.Generic.Dictionary`2/Entry<MEC.CoroutineHandle,System.Collections.Generic.HashSet`1<MEC.CoroutineHandle>>[]
struct EntryU5BU5D_tCE180FEE73FCC74B4ABB3EFE90159EC4F1C7C634;
// System.Collections.Generic.Dictionary`2/Entry<MEC.Timing/ProcessIndex,MEC.CoroutineHandle>[]
struct EntryU5BU5D_t76E3F3D3DA2CC2D017C0B082D0DCD07AA5219C93;
// System.Collections.Generic.IEnumerator`1<System.Single>[]
struct IEnumerator_1U5BU5D_tA2EF2FFDBA098C53558E3177C143C4DCFB0E5B88;
// System.Boolean[]
struct BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4;
// System.Delegate[]
struct DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771;
// System.Int32[]
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C;
// System.IntPtr[]
struct IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF;
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;
// MEC.Timing[]
struct TimingU5BU5D_t67041BE600EC8358EF51AADDB3F90A999868AB1C;
// System.Action
struct Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07;
// UnityEngine.AsyncOperation
struct AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C;
// UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B;
// System.Globalization.CultureInfo
struct CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0;
// UnityEngine.CustomYieldInstruction
struct CustomYieldInstruction_t6B81A50D5D210C1ACAAE247FB53B65CDFFEB7617;
// System.DelegateData
struct DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E;
// System.Exception
struct Exception_t;
// System.Threading.ExecutionContext
struct ExecutionContext_t9D6EDFD92F0B2D391751963E2D77A8B03CB81710;
// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F;
// System.Collections.IDictionary
struct IDictionary_t6D03155AF1FA9083817AA5B6AD7DEEACC26AB220;
// System.Collections.IEnumerator
struct IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA;
// System.Security.Principal.IPrincipal
struct IPrincipal_tE7AF5096287F6C3472585E124CB38FF2A51EAB5F;
// System.Threading.InternalThread
struct InternalThread_tF40B7BFCBD60C82BD8475A22FF5186CA10293687;
// System.LocalDataStoreHolder
struct LocalDataStoreHolder_t789DD474AE5141213C2105CE57830ECFC2D3C03F;
// System.LocalDataStoreMgr
struct LocalDataStoreMgr_t205F1783D5CC2B148E829B5882E5406FF9A3AC1E;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71;
// System.MulticastDelegate
struct MulticastDelegate_t;
// System.NotSupportedException
struct NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A;
// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6;
// System.String
struct String_t;
// System.Threading.Thread
struct Thread_t0A773B9DE873D2DCAA7D229EAB36757B500E207F;
// MEC.Timing
struct Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663;
// MEC.CoroutineHandle/<_OnDestroy>d__32
struct U3C_OnDestroyU3Ed__32_tC237D236161175615D4FE20E0390919C6A0C1E1A;
// MEC.CoroutineHandle/<_OnDestroy>d__33
struct U3C_OnDestroyU3Ed__33_tBEBAF5C594D7B23973CC4FD41D86F37596AAA5F2;
// MECExtensionMethods2/<Append>d__13
struct U3CAppendU3Ed__13_tDA8C39AB4BF8F167235AE8A8F64E254A4BFF32E9;
// MECExtensionMethods2/<Append>d__14
struct U3CAppendU3Ed__14_t0E472D9D963516368FE446E17DB0A22607539C15;
// MECExtensionMethods2/<CancelWith>d__4
struct U3CCancelWithU3Ed__4_t8B8BAEC6F1E6A2547D9B597A4343E3863B73203C;
// MECExtensionMethods2/<CancelWith>d__5
struct U3CCancelWithU3Ed__5_t77CB416A3F92FD3BD432622F2D4FA46C9D1155F5;
// MECExtensionMethods2/<CancelWith>d__7
struct U3CCancelWithU3Ed__7_t40CED40B36C8708B3F6BDFA2C421F36EE95CDAF7;
// MECExtensionMethods2/<Delay>d__0
struct U3CDelayU3Ed__0_tB8C42C4BE4F8BDBD9A8EFC17DAF05BA5C06045BC;
// MECExtensionMethods2/<Delay>d__1
struct U3CDelayU3Ed__1_tBD016C33189EAA123E44405BC6EDE65761067DFC;
// MECExtensionMethods2/<DelayFrames>d__3
struct U3CDelayFramesU3Ed__3_t59DE39C1E9B371904A1FA321BF5737F51BAA58E4;
// MECExtensionMethods2/<Hijack>d__19
struct U3CHijackU3Ed__19_t57ED3438E560C43D619C0606A97C7FB3B547EBB1;
// MECExtensionMethods2/<KillWith>d__12
struct U3CKillWithU3Ed__12_tC9D795656444FD23FDAD2843605F1E95509C8546;
// MECExtensionMethods2/<PauseWith>d__11
struct U3CPauseWithU3Ed__11_t5456BFBA17F259728246179E74467D248D6907EE;
// MECExtensionMethods2/<PauseWith>d__8
struct U3CPauseWithU3Ed__8_tF0B847125D7EE5CE9C57E7DC7FF416D0E5C794C0;
// MECExtensionMethods2/<PauseWith>d__9
struct U3CPauseWithU3Ed__9_t5B5FAC5525B8593A92FA9BF46781B2DA2B8DC5ED;
// MECExtensionMethods2/<Prepend>d__15
struct U3CPrependU3Ed__15_t52DE12BFF91D5776D73148D2B29F7B452020EADB;
// MECExtensionMethods2/<Prepend>d__16
struct U3CPrependU3Ed__16_t023603E04E5CBE20E04C0E48452C5CA1597C446E;
// MECExtensionMethods2/<RerouteExceptions>d__20
struct U3CRerouteExceptionsU3Ed__20_t34352A3C593E8FF83A90E02879CEAD71D7417184;
// MECExtensionMethods2/<Superimpose>d__18
struct U3CSuperimposeU3Ed__18_tE9CABB59D41E90F0F203FB1529715342294EF1B6;
// MEC.Timing/<_CallContinuously>d__335
struct U3C_CallContinuouslyU3Ed__335_t7F2527CCC86565E516FFF4EEEE2B4966BEA16368;
// MEC.Timing/<_DelayedCall>d__313
struct U3C_DelayedCallU3Ed__313_tBF4CBB957951173CC872155112E8791D838F1FBF;
// MEC.Timing/<_EOFPump>d__133
struct U3C_EOFPumpU3Ed__133_tE02D8D1E07F743410384C7B9F487FC45070B6C01;
// MEC.Timing/<_EOFPumpWatcher>d__132
struct U3C_EOFPumpWatcherU3Ed__132_t083D79457AC8E327332F8E9614E1F0625985BBED;
// MEC.Timing/<_InjectDelay>d__288
struct U3C_InjectDelayU3Ed__288_t68BAE838FFF0D6A5D77DD945629395CB307A7C47;
// MEC.Timing/<_StartWhenDone>d__275
struct U3C_StartWhenDoneU3Ed__275_t27CB96517B4094CEC4B93433D970071A204798F8;
// MEC.Timing/<_StartWhenDone>d__280
struct U3C_StartWhenDoneU3Ed__280_t97BCE744B518341C8288ADDDBFAB856E7277142A;
// MEC.Timing/<_StartWhenDone>d__282
struct U3C_StartWhenDoneU3Ed__282_tE9D05D0D91E0AA510B531696B3373824F8299163;
// MEC.Timing/<_StartWhenDone>d__287
struct U3C_StartWhenDoneU3Ed__287_tAE4B8324916A8A4250BF3343E47E25811AF2D40F;
// MEC.Timing/<_WatchCall>d__334
struct U3C_WatchCallU3Ed__334_t7772F9C12997D6A2EF678E1FBD5FF42FC86C41AA;

IL2CPP_EXTERN_C RuntimeClass* BitConverter_t6E99605185963BC12B3D369E13F2B88997E64A27_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_3_tEDDCF49DDA3D0F9DCB147948F764E9514FC2EB04_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MissingReferenceException_t35FD5FA7551453057398F69DC338ADBC09B7581B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CAppendU3Ed__13_tDA8C39AB4BF8F167235AE8A8F64E254A4BFF32E9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CAppendU3Ed__14_t0E472D9D963516368FE446E17DB0A22607539C15_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CCancelWithU3Ed__4_t8B8BAEC6F1E6A2547D9B597A4343E3863B73203C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CCancelWithU3Ed__5_t77CB416A3F92FD3BD432622F2D4FA46C9D1155F5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CCancelWithU3Ed__7_t40CED40B36C8708B3F6BDFA2C421F36EE95CDAF7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CDelayFramesU3Ed__3_t59DE39C1E9B371904A1FA321BF5737F51BAA58E4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CDelayU3Ed__0_tB8C42C4BE4F8BDBD9A8EFC17DAF05BA5C06045BC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CDelayU3Ed__1_tBD016C33189EAA123E44405BC6EDE65761067DFC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CHijackU3Ed__19_t57ED3438E560C43D619C0606A97C7FB3B547EBB1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CKillWithU3Ed__12_tC9D795656444FD23FDAD2843605F1E95509C8546_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CPauseWithU3Ed__11_t5456BFBA17F259728246179E74467D248D6907EE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CPauseWithU3Ed__8_tF0B847125D7EE5CE9C57E7DC7FF416D0E5C794C0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CPauseWithU3Ed__9_t5B5FAC5525B8593A92FA9BF46781B2DA2B8DC5ED_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CPrependU3Ed__15_t52DE12BFF91D5776D73148D2B29F7B452020EADB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CPrependU3Ed__16_t023603E04E5CBE20E04C0E48452C5CA1597C446E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CRerouteExceptionsU3Ed__20_t34352A3C593E8FF83A90E02879CEAD71D7417184_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CSuperimposeU3Ed__18_tE9CABB59D41E90F0F203FB1529715342294EF1B6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3C_OnDestroyU3Ed__32_tC237D236161175615D4FE20E0390919C6A0C1E1A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3C_OnDestroyU3Ed__33_tBEBAF5C594D7B23973CC4FD41D86F37596AAA5F2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral37407E1CCD62BFCCE0980D9087E09AE63E8534D2;
IL2CPP_EXTERN_C String_t* _stringLiteral37812EA687D99D91FFF18BD0937BED8AD0E13ED4;
IL2CPP_EXTERN_C String_t* _stringLiteral7461050B85255933384A142D9959A57EEFAB5448;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_ContainsKey_m8466E7777FED2755A8F5EF371415CBCF59589873_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_ContainsKey_mD41C34E43A72893DA9509BA2C25ED1F65DED90E8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Item_mEE876E1A07892EE036FBCB85F5C96E283EADBEB7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1_ToString_m5FDC0CBE068DB2893454257CE6E29846D47B3038_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1_get_HasValue_mCF2FD8B3055FA87FC9C504F2122B3B0FAEDE3EC9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1_get_Value_m0DF3B826A745419D26A168689AEB4BE6F8698ECA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Timing_ReturnTmpRefForRepFunc_m1FEA4D102C9DD648C661C9A6503443AF9015CDAC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CAppendU3Ed__13_System_Collections_IEnumerator_Reset_m53165567602358B22AFB3317C8DE1C1A1E649824_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CAppendU3Ed__14_System_Collections_IEnumerator_Reset_m44792D85988AC0A44FE6BDA0EC19DAEE1F181582_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CCancelWithU3Ed__4_System_Collections_IEnumerator_Reset_m655AEE33429EC98E86F2707FBF28446A62FCAFC6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CCancelWithU3Ed__5_System_Collections_IEnumerator_Reset_m99E7F97F6C0BD792FD3F300A8A2D3A13A0F6DC4A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CCancelWithU3Ed__7_System_Collections_IEnumerator_Reset_mBF3A27178CCE6D84607BE54AF608AEADA8776863_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CDelayFramesU3Ed__3_System_Collections_IEnumerator_Reset_mA243F12B3EAEC24744A1CAB6AE77DD69F264ED17_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CDelayU3Ed__0_System_Collections_IEnumerator_Reset_m61429A4ED99BA4E915AF45EE14EF5E99CE52B618_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CDelayU3Ed__1_System_Collections_IEnumerator_Reset_m8DFD84218D4EF4A1AB8F0D5CB1F077BC24935E47_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CHijackU3Ed__19_System_Collections_IEnumerator_Reset_m009F853065024DFB852BCBF17965E120B8A93247_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CKillWithU3Ed__12_System_Collections_IEnumerator_Reset_mE25758EF248A8B30D3D39785EAC1F99D5B34ECA6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CPauseWithU3Ed__11_System_Collections_IEnumerator_Reset_m184D4FE68946399934736D8E8A275A2CE252DC5C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CPauseWithU3Ed__8_System_Collections_IEnumerator_Reset_m18E92390DD8C84073811F72E56BF5A338395F69A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CPauseWithU3Ed__9_System_Collections_IEnumerator_Reset_mB369AC85BC8DF2634270D64EB43CFAC81243011A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CPrependU3Ed__15_System_Collections_IEnumerator_Reset_mA49D8E8865EB9AB231E0FB3CCE31AAB6DB372C65_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CPrependU3Ed__16_System_Collections_IEnumerator_Reset_m35208B6A2CEB689B4E8B1BDE56332A6139A0A1EF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CRerouteExceptionsU3Ed__20_System_Collections_IEnumerator_Reset_m13856FEAEC1D9666C40A681C2C796634ABDE89D3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CSuperimposeU3Ed__18_System_Collections_IEnumerator_Reset_m0D4D07DF6253C85A2E560CE041AC14FD1BCA3D2B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3C_CallContinuouslyU3Ed__335_System_Collections_IEnumerator_Reset_m425F3856E9D8B543F86885427C7A91DA8AB012C7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3C_DelayedCallU3Ed__313_System_Collections_IEnumerator_Reset_m9223F0C17129D85FAE544A3A67C6E4F831049243_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3C_EOFPumpU3Ed__133_System_Collections_IEnumerator_Reset_mA584345D587C9DDB8394E095ACC4311435B8CFCC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3C_EOFPumpWatcherU3Ed__132_System_Collections_IEnumerator_Reset_m15E6D5092EF7E27BAC2B1CA1106976EAC0F5F0BD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3C_InjectDelayU3Ed__288_System_Collections_IEnumerator_Reset_mB7DD920810F96F2A2299354FB8CA75032B9860B1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3C_OnDestroyU3Ed__32_System_Collections_IEnumerator_Reset_m03890BCA29362C3F164A603A01E22E2DB6496725_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3C_OnDestroyU3Ed__33_System_Collections_IEnumerator_Reset_m545DAC89CB85230BFB51A9D5F78D7C450F0ED353_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3C_StartWhenDoneU3Ed__275_System_Collections_IEnumerator_Reset_m0825ACABC6908F6EC451DA4F80879DFC4F1026B1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3C_StartWhenDoneU3Ed__280_System_Collections_IEnumerator_Reset_mE155E978CC6C00E9C68D941549CAD5BF05E8570D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3C_StartWhenDoneU3Ed__282_System_Collections_IEnumerator_Reset_m44F075C9EFBD9182513D3490235D7DC582C44447_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3C_StartWhenDoneU3Ed__287_System_Collections_IEnumerator_Reset_m4D9AC2A778EF63FFBA8B1AD1B65B59A7A7E59A19_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3C_WatchCallU3Ed__334_System_Collections_IEnumerator_Reset_m6E5B1313F721247D6775473FDC048DAABDDB1620_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct IEnumerator_1U5BU5D_tA2EF2FFDBA098C53558E3177C143C4DCFB0E5B88;
struct BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4;
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C;
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tF062866229C4952B8051AD32AB6E9D931142CC95 
{
};

// System.Collections.Generic.Dictionary`2<MEC.CoroutineHandle,System.Collections.Generic.HashSet`1<MEC.CoroutineHandle>>
struct Dictionary_2_t586C869F4A815C42AD5BEFA81A6BB784048C02BE  : public RuntimeObject
{
	// System.Int32[] System.Collections.Generic.Dictionary`2::_buckets
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::_entries
	EntryU5BU5D_tCE180FEE73FCC74B4ABB3EFE90159EC4F1C7C634* ____entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::_count
	int32_t ____count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeList
	int32_t ____freeList_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeCount
	int32_t ____freeCount_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::_version
	int32_t ____version_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::_comparer
	RuntimeObject* ____comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_keys
	KeyCollection_t9F7CB0FE2CB1909A2BC0176AF6E6BB3C6B49B1DE* ____keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_values
	ValueCollection_tF79182B406A6805FA9AA4D6ABFC74DA1A94BDC0C* ____values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject* ____syncRoot_9;
};

// System.Collections.Generic.Dictionary`2<MEC.Timing/ProcessIndex,MEC.CoroutineHandle>
struct Dictionary_2_t8C5B8A51DE825AB5FC3EDF04011A63CE33EBF2DB  : public RuntimeObject
{
	// System.Int32[] System.Collections.Generic.Dictionary`2::_buckets
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::_entries
	EntryU5BU5D_t76E3F3D3DA2CC2D017C0B082D0DCD07AA5219C93* ____entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::_count
	int32_t ____count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeList
	int32_t ____freeList_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeCount
	int32_t ____freeCount_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::_version
	int32_t ____version_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::_comparer
	RuntimeObject* ____comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_keys
	KeyCollection_t3EA0D9AC957940C6F0132DAFE6CA3860BC56EB39* ____keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_values
	ValueCollection_tE89CCD430D99C87F0846B8232435C433AAF2E394* ____values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject* ____syncRoot_9;
};
struct Il2CppArrayBounds;

// System.Runtime.ConstrainedExecution.CriticalFinalizerObject
struct CriticalFinalizerObject_t1DCAB623CAEA6529A96F5F3EDE3C7048A6E313C9  : public RuntimeObject
{
};

// UnityEngine.CustomYieldInstruction
struct CustomYieldInstruction_t6B81A50D5D210C1ACAAE247FB53B65CDFFEB7617  : public RuntimeObject
{
};

// MEC.MECExtensionMethods1
struct MECExtensionMethods1_tFBD6409B8DB44F5D1E7E5FD47FB74362FEF3EC20  : public RuntimeObject
{
};

// MECExtensionMethods2
struct MECExtensionMethods2_t044D194FBDBE2CEBF8769BEA39408FEC06ABB456  : public RuntimeObject
{
};

// System.String
struct String_t  : public RuntimeObject
{
	// System.Int32 System.String::_stringLength
	int32_t ____stringLength_4;
	// System.Char System.String::_firstChar
	Il2CppChar ____firstChar_5;
};

struct String_t_StaticFields
{
	// System.String System.String::Empty
	String_t* ___Empty_6;
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_com
{
};

// MECExtensionMethods2/<Append>d__13
struct U3CAppendU3Ed__13_tDA8C39AB4BF8F167235AE8A8F64E254A4BFF32E9  : public RuntimeObject
{
	// System.Int32 MECExtensionMethods2/<Append>d__13::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Single MECExtensionMethods2/<Append>d__13::<>2__current
	float ___U3CU3E2__current_1;
	// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2/<Append>d__13::coroutine
	RuntimeObject* ___coroutine_2;
	// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2/<Append>d__13::nextCoroutine
	RuntimeObject* ___nextCoroutine_3;
};

// MECExtensionMethods2/<Append>d__14
struct U3CAppendU3Ed__14_t0E472D9D963516368FE446E17DB0A22607539C15  : public RuntimeObject
{
	// System.Int32 MECExtensionMethods2/<Append>d__14::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Single MECExtensionMethods2/<Append>d__14::<>2__current
	float ___U3CU3E2__current_1;
	// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2/<Append>d__14::coroutine
	RuntimeObject* ___coroutine_2;
	// System.Action MECExtensionMethods2/<Append>d__14::onDone
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___onDone_3;
};

// MECExtensionMethods2/<CancelWith>d__4
struct U3CCancelWithU3Ed__4_t8B8BAEC6F1E6A2547D9B597A4343E3863B73203C  : public RuntimeObject
{
	// System.Int32 MECExtensionMethods2/<CancelWith>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Single MECExtensionMethods2/<CancelWith>d__4::<>2__current
	float ___U3CU3E2__current_1;
	// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2/<CancelWith>d__4::coroutine
	RuntimeObject* ___coroutine_2;
	// UnityEngine.GameObject MECExtensionMethods2/<CancelWith>d__4::gameObject
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___gameObject_3;
};

// MECExtensionMethods2/<CancelWith>d__5
struct U3CCancelWithU3Ed__5_t77CB416A3F92FD3BD432622F2D4FA46C9D1155F5  : public RuntimeObject
{
	// System.Int32 MECExtensionMethods2/<CancelWith>d__5::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Single MECExtensionMethods2/<CancelWith>d__5::<>2__current
	float ___U3CU3E2__current_1;
	// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2/<CancelWith>d__5::coroutine
	RuntimeObject* ___coroutine_2;
	// UnityEngine.GameObject MECExtensionMethods2/<CancelWith>d__5::gameObject1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___gameObject1_3;
	// UnityEngine.GameObject MECExtensionMethods2/<CancelWith>d__5::gameObject2
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___gameObject2_4;
};

// MECExtensionMethods2/<CancelWith>d__7
struct U3CCancelWithU3Ed__7_t40CED40B36C8708B3F6BDFA2C421F36EE95CDAF7  : public RuntimeObject
{
	// System.Int32 MECExtensionMethods2/<CancelWith>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Single MECExtensionMethods2/<CancelWith>d__7::<>2__current
	float ___U3CU3E2__current_1;
	// System.Func`1<System.Boolean> MECExtensionMethods2/<CancelWith>d__7::condition
	Func_1_t2BE7F58348C9CC544A8973B3A9E55541DE43C457* ___condition_2;
	// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2/<CancelWith>d__7::coroutine
	RuntimeObject* ___coroutine_3;
};

// MECExtensionMethods2/<Delay>d__0
struct U3CDelayU3Ed__0_tB8C42C4BE4F8BDBD9A8EFC17DAF05BA5C06045BC  : public RuntimeObject
{
	// System.Int32 MECExtensionMethods2/<Delay>d__0::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Single MECExtensionMethods2/<Delay>d__0::<>2__current
	float ___U3CU3E2__current_1;
	// System.Single MECExtensionMethods2/<Delay>d__0::timeToDelay
	float ___timeToDelay_2;
	// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2/<Delay>d__0::coroutine
	RuntimeObject* ___coroutine_3;
};

// MECExtensionMethods2/<Delay>d__1
struct U3CDelayU3Ed__1_tBD016C33189EAA123E44405BC6EDE65761067DFC  : public RuntimeObject
{
	// System.Int32 MECExtensionMethods2/<Delay>d__1::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Single MECExtensionMethods2/<Delay>d__1::<>2__current
	float ___U3CU3E2__current_1;
	// System.Func`1<System.Boolean> MECExtensionMethods2/<Delay>d__1::condition
	Func_1_t2BE7F58348C9CC544A8973B3A9E55541DE43C457* ___condition_2;
	// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2/<Delay>d__1::coroutine
	RuntimeObject* ___coroutine_3;
};

// MECExtensionMethods2/<DelayFrames>d__3
struct U3CDelayFramesU3Ed__3_t59DE39C1E9B371904A1FA321BF5737F51BAA58E4  : public RuntimeObject
{
	// System.Int32 MECExtensionMethods2/<DelayFrames>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Single MECExtensionMethods2/<DelayFrames>d__3::<>2__current
	float ___U3CU3E2__current_1;
	// System.Int32 MECExtensionMethods2/<DelayFrames>d__3::framesToDelay
	int32_t ___framesToDelay_2;
	// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2/<DelayFrames>d__3::coroutine
	RuntimeObject* ___coroutine_3;
};

// MECExtensionMethods2/<Hijack>d__19
struct U3CHijackU3Ed__19_t57ED3438E560C43D619C0606A97C7FB3B547EBB1  : public RuntimeObject
{
	// System.Int32 MECExtensionMethods2/<Hijack>d__19::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Single MECExtensionMethods2/<Hijack>d__19::<>2__current
	float ___U3CU3E2__current_1;
	// System.Func`2<System.Single,System.Single> MECExtensionMethods2/<Hijack>d__19::newReturn
	Func_2_t2A7432CC4F64D0DF6D8629208B154CF139B39AF2* ___newReturn_2;
	// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2/<Hijack>d__19::coroutine
	RuntimeObject* ___coroutine_3;
};

// MECExtensionMethods2/<PauseWith>d__11
struct U3CPauseWithU3Ed__11_t5456BFBA17F259728246179E74467D248D6907EE  : public RuntimeObject
{
	// System.Int32 MECExtensionMethods2/<PauseWith>d__11::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Single MECExtensionMethods2/<PauseWith>d__11::<>2__current
	float ___U3CU3E2__current_1;
	// System.Func`1<System.Boolean> MECExtensionMethods2/<PauseWith>d__11::condition
	Func_1_t2BE7F58348C9CC544A8973B3A9E55541DE43C457* ___condition_2;
	// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2/<PauseWith>d__11::coroutine
	RuntimeObject* ___coroutine_3;
};

// MECExtensionMethods2/<PauseWith>d__8
struct U3CPauseWithU3Ed__8_tF0B847125D7EE5CE9C57E7DC7FF416D0E5C794C0  : public RuntimeObject
{
	// System.Int32 MECExtensionMethods2/<PauseWith>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Single MECExtensionMethods2/<PauseWith>d__8::<>2__current
	float ___U3CU3E2__current_1;
	// UnityEngine.GameObject MECExtensionMethods2/<PauseWith>d__8::gameObject
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___gameObject_2;
	// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2/<PauseWith>d__8::coroutine
	RuntimeObject* ___coroutine_3;
};

// MECExtensionMethods2/<PauseWith>d__9
struct U3CPauseWithU3Ed__9_t5B5FAC5525B8593A92FA9BF46781B2DA2B8DC5ED  : public RuntimeObject
{
	// System.Int32 MECExtensionMethods2/<PauseWith>d__9::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Single MECExtensionMethods2/<PauseWith>d__9::<>2__current
	float ___U3CU3E2__current_1;
	// UnityEngine.GameObject MECExtensionMethods2/<PauseWith>d__9::gameObject1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___gameObject1_2;
	// UnityEngine.GameObject MECExtensionMethods2/<PauseWith>d__9::gameObject2
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___gameObject2_3;
	// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2/<PauseWith>d__9::coroutine
	RuntimeObject* ___coroutine_4;
};

// MECExtensionMethods2/<Prepend>d__15
struct U3CPrependU3Ed__15_t52DE12BFF91D5776D73148D2B29F7B452020EADB  : public RuntimeObject
{
	// System.Int32 MECExtensionMethods2/<Prepend>d__15::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Single MECExtensionMethods2/<Prepend>d__15::<>2__current
	float ___U3CU3E2__current_1;
	// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2/<Prepend>d__15::lastCoroutine
	RuntimeObject* ___lastCoroutine_2;
	// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2/<Prepend>d__15::coroutine
	RuntimeObject* ___coroutine_3;
};

// MECExtensionMethods2/<Prepend>d__16
struct U3CPrependU3Ed__16_t023603E04E5CBE20E04C0E48452C5CA1597C446E  : public RuntimeObject
{
	// System.Int32 MECExtensionMethods2/<Prepend>d__16::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Single MECExtensionMethods2/<Prepend>d__16::<>2__current
	float ___U3CU3E2__current_1;
	// System.Action MECExtensionMethods2/<Prepend>d__16::onStart
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___onStart_2;
	// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2/<Prepend>d__16::coroutine
	RuntimeObject* ___coroutine_3;
};

// MECExtensionMethods2/<RerouteExceptions>d__20
struct U3CRerouteExceptionsU3Ed__20_t34352A3C593E8FF83A90E02879CEAD71D7417184  : public RuntimeObject
{
	// System.Int32 MECExtensionMethods2/<RerouteExceptions>d__20::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Single MECExtensionMethods2/<RerouteExceptions>d__20::<>2__current
	float ___U3CU3E2__current_1;
	// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2/<RerouteExceptions>d__20::coroutine
	RuntimeObject* ___coroutine_2;
	// System.Action`1<System.Exception> MECExtensionMethods2/<RerouteExceptions>d__20::exceptionHandler
	Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* ___exceptionHandler_3;
};

// MECExtensionMethods2/<Superimpose>d__18
struct U3CSuperimposeU3Ed__18_tE9CABB59D41E90F0F203FB1529715342294EF1B6  : public RuntimeObject
{
	// System.Int32 MECExtensionMethods2/<Superimpose>d__18::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Single MECExtensionMethods2/<Superimpose>d__18::<>2__current
	float ___U3CU3E2__current_1;
	// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2/<Superimpose>d__18::coroutineA
	RuntimeObject* ___coroutineA_2;
	// MEC.Timing MECExtensionMethods2/<Superimpose>d__18::instance
	Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* ___instance_3;
	// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2/<Superimpose>d__18::coroutineB
	RuntimeObject* ___coroutineB_4;
};

// MEC.Timing/<_CallContinuously>d__335
struct U3C_CallContinuouslyU3Ed__335_t7F2527CCC86565E516FFF4EEEE2B4966BEA16368  : public RuntimeObject
{
	// System.Int32 MEC.Timing/<_CallContinuously>d__335::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Single MEC.Timing/<_CallContinuously>d__335::<>2__current
	float ___U3CU3E2__current_1;
	// MEC.Timing MEC.Timing/<_CallContinuously>d__335::<>4__this
	Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* ___U3CU3E4__this_2;
	// System.Single MEC.Timing/<_CallContinuously>d__335::period
	float ___period_3;
	// UnityEngine.GameObject MEC.Timing/<_CallContinuously>d__335::gObject
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___gObject_4;
	// System.Action MEC.Timing/<_CallContinuously>d__335::action
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___action_5;
};

// MEC.Timing/<_DelayedCall>d__313
struct U3C_DelayedCallU3Ed__313_tBF4CBB957951173CC872155112E8791D838F1FBF  : public RuntimeObject
{
	// System.Int32 MEC.Timing/<_DelayedCall>d__313::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Single MEC.Timing/<_DelayedCall>d__313::<>2__current
	float ___U3CU3E2__current_1;
	// MEC.Timing MEC.Timing/<_DelayedCall>d__313::<>4__this
	Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* ___U3CU3E4__this_2;
	// System.Single MEC.Timing/<_DelayedCall>d__313::delay
	float ___delay_3;
	// UnityEngine.GameObject MEC.Timing/<_DelayedCall>d__313::cancelWith
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___cancelWith_4;
	// System.Action MEC.Timing/<_DelayedCall>d__313::action
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___action_5;
};

// MEC.Timing/<_EOFPump>d__133
struct U3C_EOFPumpU3Ed__133_tE02D8D1E07F743410384C7B9F487FC45070B6C01  : public RuntimeObject
{
	// System.Int32 MEC.Timing/<_EOFPump>d__133::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MEC.Timing/<_EOFPump>d__133::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// MEC.Timing MEC.Timing/<_EOFPump>d__133::<>4__this
	Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* ___U3CU3E4__this_2;
};

// MEC.Timing/<_EOFPumpWatcher>d__132
struct U3C_EOFPumpWatcherU3Ed__132_t083D79457AC8E327332F8E9614E1F0625985BBED  : public RuntimeObject
{
	// System.Int32 MEC.Timing/<_EOFPumpWatcher>d__132::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Single MEC.Timing/<_EOFPumpWatcher>d__132::<>2__current
	float ___U3CU3E2__current_1;
	// MEC.Timing MEC.Timing/<_EOFPumpWatcher>d__132::<>4__this
	Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* ___U3CU3E4__this_2;
};

// MEC.Timing/<_InjectDelay>d__288
struct U3C_InjectDelayU3Ed__288_t68BAE838FFF0D6A5D77DD945629395CB307A7C47  : public RuntimeObject
{
	// System.Int32 MEC.Timing/<_InjectDelay>d__288::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Single MEC.Timing/<_InjectDelay>d__288::<>2__current
	float ___U3CU3E2__current_1;
	// MEC.Timing MEC.Timing/<_InjectDelay>d__288::<>4__this
	Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* ___U3CU3E4__this_2;
	// System.Single MEC.Timing/<_InjectDelay>d__288::waitTime
	float ___waitTime_3;
	// System.Collections.Generic.IEnumerator`1<System.Single> MEC.Timing/<_InjectDelay>d__288::proc
	RuntimeObject* ___proc_4;
};

// MEC.Timing/<_StartWhenDone>d__280
struct U3C_StartWhenDoneU3Ed__280_t97BCE744B518341C8288ADDDBFAB856E7277142A  : public RuntimeObject
{
	// System.Int32 MEC.Timing/<_StartWhenDone>d__280::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Single MEC.Timing/<_StartWhenDone>d__280::<>2__current
	float ___U3CU3E2__current_1;
	// UnityEngine.AsyncOperation MEC.Timing/<_StartWhenDone>d__280::operation
	AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C* ___operation_2;
	// System.Collections.Generic.IEnumerator`1<System.Single> MEC.Timing/<_StartWhenDone>d__280::pausedProc
	RuntimeObject* ___pausedProc_3;
};

// MEC.Timing/<_StartWhenDone>d__282
struct U3C_StartWhenDoneU3Ed__282_tE9D05D0D91E0AA510B531696B3373824F8299163  : public RuntimeObject
{
	// System.Int32 MEC.Timing/<_StartWhenDone>d__282::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Single MEC.Timing/<_StartWhenDone>d__282::<>2__current
	float ___U3CU3E2__current_1;
	// UnityEngine.CustomYieldInstruction MEC.Timing/<_StartWhenDone>d__282::operation
	CustomYieldInstruction_t6B81A50D5D210C1ACAAE247FB53B65CDFFEB7617* ___operation_2;
	// System.Collections.Generic.IEnumerator`1<System.Single> MEC.Timing/<_StartWhenDone>d__282::pausedProc
	RuntimeObject* ___pausedProc_3;
};

// MEC.Timing/<_StartWhenDone>d__287
struct U3C_StartWhenDoneU3Ed__287_tAE4B8324916A8A4250BF3343E47E25811AF2D40F  : public RuntimeObject
{
	// System.Int32 MEC.Timing/<_StartWhenDone>d__287::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Single MEC.Timing/<_StartWhenDone>d__287::<>2__current
	float ___U3CU3E2__current_1;
	// System.Func`1<System.Boolean> MEC.Timing/<_StartWhenDone>d__287::evaluatorFunc
	Func_1_t2BE7F58348C9CC544A8973B3A9E55541DE43C457* ___evaluatorFunc_2;
	// System.Boolean MEC.Timing/<_StartWhenDone>d__287::continueOn
	bool ___continueOn_3;
	// System.Collections.Generic.IEnumerator`1<System.Single> MEC.Timing/<_StartWhenDone>d__287::pausedProc
	RuntimeObject* ___pausedProc_4;
};

// System.Nullable`1<System.Int32>
struct Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28 
{
	// System.Boolean System.Nullable`1::hasValue
	bool ___hasValue_0;
	// T System.Nullable`1::value
	int32_t ___value_1;
};

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;
};

struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;
};

// System.Byte
struct Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3 
{
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;
};

// MEC.CoroutineHandle
struct CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 
{
	// System.Int32 MEC.CoroutineHandle::_id
	int32_t ____id_2;
};

struct CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772_StaticFields
{
	// System.Int32[] MEC.CoroutineHandle::NextIndex
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___NextIndex_1;
};

// System.Int32
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

struct IntPtr_t_StaticFields
{
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;
};

// System.Single
struct Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C 
{
	// System.Single System.Single::m_value
	float ___m_value_0;
};

// System.Threading.Thread
struct Thread_t0A773B9DE873D2DCAA7D229EAB36757B500E207F  : public CriticalFinalizerObject_t1DCAB623CAEA6529A96F5F3EDE3C7048A6E313C9
{
	// System.Threading.InternalThread System.Threading.Thread::internal_thread
	InternalThread_tF40B7BFCBD60C82BD8475A22FF5186CA10293687* ___internal_thread_6;
	// System.Object System.Threading.Thread::m_ThreadStartArg
	RuntimeObject* ___m_ThreadStartArg_7;
	// System.Object System.Threading.Thread::pending_exception
	RuntimeObject* ___pending_exception_8;
	// System.MulticastDelegate System.Threading.Thread::m_Delegate
	MulticastDelegate_t* ___m_Delegate_10;
	// System.Threading.ExecutionContext System.Threading.Thread::m_ExecutionContext
	ExecutionContext_t9D6EDFD92F0B2D391751963E2D77A8B03CB81710* ___m_ExecutionContext_11;
	// System.Boolean System.Threading.Thread::m_ExecutionContextBelongsToOuterScope
	bool ___m_ExecutionContextBelongsToOuterScope_12;
	// System.Security.Principal.IPrincipal System.Threading.Thread::principal
	RuntimeObject* ___principal_13;
	// System.Int32 System.Threading.Thread::principal_version
	int32_t ___principal_version_14;
};

struct Thread_t0A773B9DE873D2DCAA7D229EAB36757B500E207F_StaticFields
{
	// System.LocalDataStoreMgr System.Threading.Thread::s_LocalDataStoreMgr
	LocalDataStoreMgr_t205F1783D5CC2B148E829B5882E5406FF9A3AC1E* ___s_LocalDataStoreMgr_0;
	// System.Threading.AsyncLocal`1<System.Globalization.CultureInfo> System.Threading.Thread::s_asyncLocalCurrentCulture
	AsyncLocal_1_t1D3339EA4C8650D2DEDDF9553E5C932B3DC2CCFD* ___s_asyncLocalCurrentCulture_4;
	// System.Threading.AsyncLocal`1<System.Globalization.CultureInfo> System.Threading.Thread::s_asyncLocalCurrentUICulture
	AsyncLocal_1_t1D3339EA4C8650D2DEDDF9553E5C932B3DC2CCFD* ___s_asyncLocalCurrentUICulture_5;
};

struct Thread_t0A773B9DE873D2DCAA7D229EAB36757B500E207F_ThreadStaticFields
{
	// System.LocalDataStoreHolder System.Threading.Thread::s_LocalDataStore
	LocalDataStoreHolder_t789DD474AE5141213C2105CE57830ECFC2D3C03F* ___s_LocalDataStore_1;
	// System.Globalization.CultureInfo System.Threading.Thread::m_CurrentCulture
	CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* ___m_CurrentCulture_2;
	// System.Globalization.CultureInfo System.Threading.Thread::m_CurrentUICulture
	CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* ___m_CurrentUICulture_3;
	// System.Threading.Thread System.Threading.Thread::current_thread
	Thread_t0A773B9DE873D2DCAA7D229EAB36757B500E207F* ___current_thread_9;
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663  : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D
{
};

// MEC.Timing/ProcessIndex
struct ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 
{
	// MEC.Segment MEC.Timing/ProcessIndex::seg
	int32_t ___seg_0;
	// System.Int32 MEC.Timing/ProcessIndex::i
	int32_t ___i_1;
};

// UnityEngine.AsyncOperation
struct AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C  : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D
{
	// System.IntPtr UnityEngine.AsyncOperation::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Action`1<UnityEngine.AsyncOperation> UnityEngine.AsyncOperation::m_completeCallback
	Action_1_tE8693FF0E67CDBA52BAFB211BFF1844D076ABAFB* ___m_completeCallback_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C_marshaled_pinvoke : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
// Native definition for COM marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C_marshaled_com : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};

// UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B  : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D
{
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B_marshaled_pinvoke : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B_marshaled_com : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// System.Delegate
struct Delegate_t  : public RuntimeObject
{
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject* ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.IntPtr System.Delegate::interp_method
	intptr_t ___interp_method_7;
	// System.IntPtr System.Delegate::interp_invoke_impl
	intptr_t ___interp_invoke_impl_8;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t* ___method_info_9;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t* ___original_method_info_10;
	// System.DelegateData System.Delegate::data
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_12;
};
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t* ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject* ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject* ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832* ___native_trace_ips_15;
	// System.Int32 System.Exception::caught_in_unmanaged
	int32_t ___caught_in_unmanaged_16;
};

struct Exception_t_StaticFields
{
	// System.Object System.Exception::s_EDILock
	RuntimeObject* ___s_EDILock_0;
};
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;
};

struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_StaticFields
{
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// MEC.CoroutineHandle/<_OnDestroy>d__32
struct U3C_OnDestroyU3Ed__32_tC237D236161175615D4FE20E0390919C6A0C1E1A  : public RuntimeObject
{
	// System.Int32 MEC.CoroutineHandle/<_OnDestroy>d__32::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Single MEC.CoroutineHandle/<_OnDestroy>d__32::<>2__current
	float ___U3CU3E2__current_1;
	// MEC.CoroutineHandle MEC.CoroutineHandle/<_OnDestroy>d__32::watched
	CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___watched_2;
	// System.Action MEC.CoroutineHandle/<_OnDestroy>d__32::action
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___action_3;
};

// MEC.CoroutineHandle/<_OnDestroy>d__33
struct U3C_OnDestroyU3Ed__33_tBEBAF5C594D7B23973CC4FD41D86F37596AAA5F2  : public RuntimeObject
{
	// System.Int32 MEC.CoroutineHandle/<_OnDestroy>d__33::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Single MEC.CoroutineHandle/<_OnDestroy>d__33::<>2__current
	float ___U3CU3E2__current_1;
	// MEC.CoroutineHandle MEC.CoroutineHandle/<_OnDestroy>d__33::watched
	CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___watched_2;
	// System.Collections.Generic.IEnumerator`1<System.Single> MEC.CoroutineHandle/<_OnDestroy>d__33::action
	RuntimeObject* ___action_3;
};

// MECExtensionMethods2/<KillWith>d__12
struct U3CKillWithU3Ed__12_tC9D795656444FD23FDAD2843605F1E95509C8546  : public RuntimeObject
{
	// System.Int32 MECExtensionMethods2/<KillWith>d__12::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Single MECExtensionMethods2/<KillWith>d__12::<>2__current
	float ___U3CU3E2__current_1;
	// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2/<KillWith>d__12::coroutine
	RuntimeObject* ___coroutine_2;
	// MEC.CoroutineHandle MECExtensionMethods2/<KillWith>d__12::otherCoroutine
	CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___otherCoroutine_3;
};

// MEC.Timing/<_StartWhenDone>d__275
struct U3C_StartWhenDoneU3Ed__275_t27CB96517B4094CEC4B93433D970071A204798F8  : public RuntimeObject
{
	// System.Int32 MEC.Timing/<_StartWhenDone>d__275::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Single MEC.Timing/<_StartWhenDone>d__275::<>2__current
	float ___U3CU3E2__current_1;
	// MEC.Timing MEC.Timing/<_StartWhenDone>d__275::<>4__this
	Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* ___U3CU3E4__this_2;
	// MEC.CoroutineHandle MEC.Timing/<_StartWhenDone>d__275::handle
	CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___handle_3;
	// System.Collections.Generic.IEnumerator`1<System.Single> MEC.Timing/<_StartWhenDone>d__275::proc
	RuntimeObject* ___proc_4;
};

// MEC.Timing/<_WatchCall>d__334
struct U3C_WatchCallU3Ed__334_t7772F9C12997D6A2EF678E1FBD5FF42FC86C41AA  : public RuntimeObject
{
	// System.Int32 MEC.Timing/<_WatchCall>d__334::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Single MEC.Timing/<_WatchCall>d__334::<>2__current
	float ___U3CU3E2__current_1;
	// MEC.Timing MEC.Timing/<_WatchCall>d__334::<>4__this
	Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* ___U3CU3E4__this_2;
	// System.Single MEC.Timing/<_WatchCall>d__334::timeframe
	float ___timeframe_3;
	// MEC.CoroutineHandle MEC.Timing/<_WatchCall>d__334::handle
	CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___handle_4;
	// System.Action MEC.Timing/<_WatchCall>d__334::onDone
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___onDone_5;
	// UnityEngine.GameObject MEC.Timing/<_WatchCall>d__334::gObject
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___gObject_6;
};

// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.MissingReferenceException
struct MissingReferenceException_t35FD5FA7551453057398F69DC338ADBC09B7581B  : public Exception_t
{
};

// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771* ___delegates_13;
};
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_13;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_13;
};

// System.SystemException
struct SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295  : public Exception_t
{
};

// System.Action`1<System.Exception>
struct Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04  : public MulticastDelegate_t
{
};

// System.Action`1<System.Object>
struct Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87  : public MulticastDelegate_t
{
};

// System.Func`1<System.Boolean>
struct Func_1_t2BE7F58348C9CC544A8973B3A9E55541DE43C457  : public MulticastDelegate_t
{
};

// System.Func`2<System.Single,System.Single>
struct Func_2_t2A7432CC4F64D0DF6D8629208B154CF139B39AF2  : public MulticastDelegate_t
{
};

// System.Func`3<System.Collections.Generic.IEnumerator`1<System.Single>,MEC.CoroutineHandle,System.Collections.Generic.IEnumerator`1<System.Single>>
struct Func_3_tEDDCF49DDA3D0F9DCB147948F764E9514FC2EB04  : public MulticastDelegate_t
{
};

// System.Func`3<System.Object,MEC.CoroutineHandle,System.Object>
struct Func_3_t54E59EE3AB750BDCF167CEAAB996EAD80E127845  : public MulticastDelegate_t
{
};

// System.Action
struct Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07  : public MulticastDelegate_t
{
};

// UnityEngine.Behaviour
struct Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// System.NotSupportedException
struct NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
};

// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// MEC.Timing
struct Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Single MEC.Timing::TimeBetweenSlowUpdateCalls
	float ___TimeBetweenSlowUpdateCalls_4;
	// MEC.DebugInfoType MEC.Timing::ProfilerDebugAmount
	int32_t ___ProfilerDebugAmount_5;
	// System.Boolean MEC.Timing::AutoTriggerManualTimeframe
	bool ___AutoTriggerManualTimeframe_6;
	// System.Int32 MEC.Timing::UpdateCoroutines
	int32_t ___UpdateCoroutines_7;
	// System.Int32 MEC.Timing::FixedUpdateCoroutines
	int32_t ___FixedUpdateCoroutines_8;
	// System.Int32 MEC.Timing::LateUpdateCoroutines
	int32_t ___LateUpdateCoroutines_9;
	// System.Int32 MEC.Timing::SlowUpdateCoroutines
	int32_t ___SlowUpdateCoroutines_10;
	// System.Int32 MEC.Timing::RealtimeUpdateCoroutines
	int32_t ___RealtimeUpdateCoroutines_11;
	// System.Int32 MEC.Timing::EditorUpdateCoroutines
	int32_t ___EditorUpdateCoroutines_12;
	// System.Int32 MEC.Timing::EditorSlowUpdateCoroutines
	int32_t ___EditorSlowUpdateCoroutines_13;
	// System.Int32 MEC.Timing::EndOfFrameCoroutines
	int32_t ___EndOfFrameCoroutines_14;
	// System.Int32 MEC.Timing::ManualTimeframeCoroutines
	int32_t ___ManualTimeframeCoroutines_15;
	// System.Single MEC.Timing::localTime
	float ___localTime_16;
	// System.Single MEC.Timing::deltaTime
	float ___deltaTime_17;
	// System.Func`2<System.Single,System.Single> MEC.Timing::SetManualTimeframeTime
	Func_2_t2A7432CC4F64D0DF6D8629208B154CF139B39AF2* ___SetManualTimeframeTime_18;
	// MEC.CoroutineHandle MEC.Timing::<currentCoroutine>k__BackingField
	CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___U3CcurrentCoroutineU3Ek__BackingField_23;
	// System.Int32 MEC.Timing::_currentUpdateFrame
	int32_t ____currentUpdateFrame_29;
	// System.Int32 MEC.Timing::_currentLateUpdateFrame
	int32_t ____currentLateUpdateFrame_30;
	// System.Int32 MEC.Timing::_currentSlowUpdateFrame
	int32_t ____currentSlowUpdateFrame_31;
	// System.Int32 MEC.Timing::_currentRealtimeUpdateFrame
	int32_t ____currentRealtimeUpdateFrame_32;
	// System.Int32 MEC.Timing::_currentEndOfFrameFrame
	int32_t ____currentEndOfFrameFrame_33;
	// System.Int32 MEC.Timing::_nextUpdateProcessSlot
	int32_t ____nextUpdateProcessSlot_34;
	// System.Int32 MEC.Timing::_nextLateUpdateProcessSlot
	int32_t ____nextLateUpdateProcessSlot_35;
	// System.Int32 MEC.Timing::_nextFixedUpdateProcessSlot
	int32_t ____nextFixedUpdateProcessSlot_36;
	// System.Int32 MEC.Timing::_nextSlowUpdateProcessSlot
	int32_t ____nextSlowUpdateProcessSlot_37;
	// System.Int32 MEC.Timing::_nextRealtimeUpdateProcessSlot
	int32_t ____nextRealtimeUpdateProcessSlot_38;
	// System.Int32 MEC.Timing::_nextEditorUpdateProcessSlot
	int32_t ____nextEditorUpdateProcessSlot_39;
	// System.Int32 MEC.Timing::_nextEditorSlowUpdateProcessSlot
	int32_t ____nextEditorSlowUpdateProcessSlot_40;
	// System.Int32 MEC.Timing::_nextEndOfFrameProcessSlot
	int32_t ____nextEndOfFrameProcessSlot_41;
	// System.Int32 MEC.Timing::_nextManualTimeframeProcessSlot
	int32_t ____nextManualTimeframeProcessSlot_42;
	// System.Int32 MEC.Timing::_lastUpdateProcessSlot
	int32_t ____lastUpdateProcessSlot_43;
	// System.Int32 MEC.Timing::_lastLateUpdateProcessSlot
	int32_t ____lastLateUpdateProcessSlot_44;
	// System.Int32 MEC.Timing::_lastFixedUpdateProcessSlot
	int32_t ____lastFixedUpdateProcessSlot_45;
	// System.Int32 MEC.Timing::_lastSlowUpdateProcessSlot
	int32_t ____lastSlowUpdateProcessSlot_46;
	// System.Int32 MEC.Timing::_lastRealtimeUpdateProcessSlot
	int32_t ____lastRealtimeUpdateProcessSlot_47;
	// System.Int32 MEC.Timing::_lastEndOfFrameProcessSlot
	int32_t ____lastEndOfFrameProcessSlot_48;
	// System.Int32 MEC.Timing::_lastManualTimeframeProcessSlot
	int32_t ____lastManualTimeframeProcessSlot_49;
	// System.Single MEC.Timing::_lastUpdateTime
	float ____lastUpdateTime_50;
	// System.Single MEC.Timing::_lastLateUpdateTime
	float ____lastLateUpdateTime_51;
	// System.Single MEC.Timing::_lastFixedUpdateTime
	float ____lastFixedUpdateTime_52;
	// System.Single MEC.Timing::_lastSlowUpdateTime
	float ____lastSlowUpdateTime_53;
	// System.Single MEC.Timing::_lastRealtimeUpdateTime
	float ____lastRealtimeUpdateTime_54;
	// System.Single MEC.Timing::_lastEndOfFrameTime
	float ____lastEndOfFrameTime_55;
	// System.Single MEC.Timing::_lastManualTimeframeTime
	float ____lastManualTimeframeTime_56;
	// System.Single MEC.Timing::_lastSlowUpdateDeltaTime
	float ____lastSlowUpdateDeltaTime_57;
	// System.Single MEC.Timing::_lastEditorUpdateDeltaTime
	float ____lastEditorUpdateDeltaTime_58;
	// System.Single MEC.Timing::_lastEditorSlowUpdateDeltaTime
	float ____lastEditorSlowUpdateDeltaTime_59;
	// System.Single MEC.Timing::_lastManualTimeframeDeltaTime
	float ____lastManualTimeframeDeltaTime_60;
	// System.UInt16 MEC.Timing::_framesSinceUpdate
	uint16_t ____framesSinceUpdate_61;
	// System.UInt16 MEC.Timing::_expansions
	uint16_t ____expansions_62;
	// System.Byte MEC.Timing::_instanceID
	uint8_t ____instanceID_63;
	// System.Boolean MEC.Timing::_EOFPumpRan
	bool ____EOFPumpRan_64;
	// System.Collections.Generic.Dictionary`2<MEC.CoroutineHandle,System.Collections.Generic.HashSet`1<MEC.CoroutineHandle>> MEC.Timing::_waitingTriggers
	Dictionary_2_t586C869F4A815C42AD5BEFA81A6BB784048C02BE* ____waitingTriggers_67;
	// System.Collections.Generic.HashSet`1<MEC.CoroutineHandle> MEC.Timing::_allWaiting
	HashSet_1_tAF8A7108F75B7F92D958C3E14539DF4EFB7BFB4E* ____allWaiting_68;
	// System.Collections.Generic.Dictionary`2<MEC.CoroutineHandle,MEC.Timing/ProcessIndex> MEC.Timing::_handleToIndex
	Dictionary_2_t291649C39F6CA5089BFF0B44E07C402F820DCD32* ____handleToIndex_69;
	// System.Collections.Generic.Dictionary`2<MEC.Timing/ProcessIndex,MEC.CoroutineHandle> MEC.Timing::_indexToHandle
	Dictionary_2_t8C5B8A51DE825AB5FC3EDF04011A63CE33EBF2DB* ____indexToHandle_70;
	// System.Collections.Generic.Dictionary`2<MEC.CoroutineHandle,System.String> MEC.Timing::_processTags
	Dictionary_2_t372C5C14CE2EA3086C9578E11977BF13E9F6F0DC* ____processTags_71;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.HashSet`1<MEC.CoroutineHandle>> MEC.Timing::_taggedProcesses
	Dictionary_2_t69E5ECF7FAD51BF19A125BD35B1923F4B8FD5611* ____taggedProcesses_72;
	// System.Collections.Generic.Dictionary`2<MEC.CoroutineHandle,System.Int32> MEC.Timing::_processLayers
	Dictionary_2_tDB458AE091EEF27A65EAF46DEB69B3AF739F3B51* ____processLayers_73;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.HashSet`1<MEC.CoroutineHandle>> MEC.Timing::_layeredProcesses
	Dictionary_2_tFC47384FE9F6624AFC7658C39EF43A32B15FA8E3* ____layeredProcesses_74;
	// System.Collections.Generic.IEnumerator`1<System.Single>[] MEC.Timing::UpdateProcesses
	IEnumerator_1U5BU5D_tA2EF2FFDBA098C53558E3177C143C4DCFB0E5B88* ___UpdateProcesses_75;
	// System.Collections.Generic.IEnumerator`1<System.Single>[] MEC.Timing::LateUpdateProcesses
	IEnumerator_1U5BU5D_tA2EF2FFDBA098C53558E3177C143C4DCFB0E5B88* ___LateUpdateProcesses_76;
	// System.Collections.Generic.IEnumerator`1<System.Single>[] MEC.Timing::FixedUpdateProcesses
	IEnumerator_1U5BU5D_tA2EF2FFDBA098C53558E3177C143C4DCFB0E5B88* ___FixedUpdateProcesses_77;
	// System.Collections.Generic.IEnumerator`1<System.Single>[] MEC.Timing::SlowUpdateProcesses
	IEnumerator_1U5BU5D_tA2EF2FFDBA098C53558E3177C143C4DCFB0E5B88* ___SlowUpdateProcesses_78;
	// System.Collections.Generic.IEnumerator`1<System.Single>[] MEC.Timing::RealtimeUpdateProcesses
	IEnumerator_1U5BU5D_tA2EF2FFDBA098C53558E3177C143C4DCFB0E5B88* ___RealtimeUpdateProcesses_79;
	// System.Collections.Generic.IEnumerator`1<System.Single>[] MEC.Timing::EditorUpdateProcesses
	IEnumerator_1U5BU5D_tA2EF2FFDBA098C53558E3177C143C4DCFB0E5B88* ___EditorUpdateProcesses_80;
	// System.Collections.Generic.IEnumerator`1<System.Single>[] MEC.Timing::EditorSlowUpdateProcesses
	IEnumerator_1U5BU5D_tA2EF2FFDBA098C53558E3177C143C4DCFB0E5B88* ___EditorSlowUpdateProcesses_81;
	// System.Collections.Generic.IEnumerator`1<System.Single>[] MEC.Timing::EndOfFrameProcesses
	IEnumerator_1U5BU5D_tA2EF2FFDBA098C53558E3177C143C4DCFB0E5B88* ___EndOfFrameProcesses_82;
	// System.Collections.Generic.IEnumerator`1<System.Single>[] MEC.Timing::ManualTimeframeProcesses
	IEnumerator_1U5BU5D_tA2EF2FFDBA098C53558E3177C143C4DCFB0E5B88* ___ManualTimeframeProcesses_83;
	// System.Boolean[] MEC.Timing::UpdatePaused
	BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4* ___UpdatePaused_84;
	// System.Boolean[] MEC.Timing::LateUpdatePaused
	BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4* ___LateUpdatePaused_85;
	// System.Boolean[] MEC.Timing::FixedUpdatePaused
	BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4* ___FixedUpdatePaused_86;
	// System.Boolean[] MEC.Timing::SlowUpdatePaused
	BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4* ___SlowUpdatePaused_87;
	// System.Boolean[] MEC.Timing::RealtimeUpdatePaused
	BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4* ___RealtimeUpdatePaused_88;
	// System.Boolean[] MEC.Timing::EditorUpdatePaused
	BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4* ___EditorUpdatePaused_89;
	// System.Boolean[] MEC.Timing::EditorSlowUpdatePaused
	BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4* ___EditorSlowUpdatePaused_90;
	// System.Boolean[] MEC.Timing::EndOfFramePaused
	BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4* ___EndOfFramePaused_91;
	// System.Boolean[] MEC.Timing::ManualTimeframePaused
	BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4* ___ManualTimeframePaused_92;
	// System.Boolean[] MEC.Timing::UpdateHeld
	BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4* ___UpdateHeld_93;
	// System.Boolean[] MEC.Timing::LateUpdateHeld
	BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4* ___LateUpdateHeld_94;
	// System.Boolean[] MEC.Timing::FixedUpdateHeld
	BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4* ___FixedUpdateHeld_95;
	// System.Boolean[] MEC.Timing::SlowUpdateHeld
	BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4* ___SlowUpdateHeld_96;
	// System.Boolean[] MEC.Timing::RealtimeUpdateHeld
	BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4* ___RealtimeUpdateHeld_97;
	// System.Boolean[] MEC.Timing::EditorUpdateHeld
	BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4* ___EditorUpdateHeld_98;
	// System.Boolean[] MEC.Timing::EditorSlowUpdateHeld
	BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4* ___EditorSlowUpdateHeld_99;
	// System.Boolean[] MEC.Timing::EndOfFrameHeld
	BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4* ___EndOfFrameHeld_100;
	// System.Boolean[] MEC.Timing::ManualTimeframeHeld
	BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4* ___ManualTimeframeHeld_101;
	// MEC.CoroutineHandle MEC.Timing::_eofWatcherHandle
	CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ____eofWatcherHandle_102;
};

struct Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_StaticFields
{
	// System.Func`3<System.Collections.Generic.IEnumerator`1<System.Single>,MEC.CoroutineHandle,System.Collections.Generic.IEnumerator`1<System.Single>> MEC.Timing::ReplacementFunction
	Func_3_tEDDCF49DDA3D0F9DCB147948F764E9514FC2EB04* ___ReplacementFunction_19;
	// System.Action MEC.Timing::OnPreExecute
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___OnPreExecute_20;
	// System.Threading.Thread MEC.Timing::<MainThread>k__BackingField
	Thread_t0A773B9DE873D2DCAA7D229EAB36757B500E207F* ___U3CMainThreadU3Ek__BackingField_22;
	// System.Object MEC.Timing::_tmpRef
	RuntimeObject* ____tmpRef_24;
	// System.Int32 MEC.Timing::_tmpInt
	int32_t ____tmpInt_25;
	// System.Boolean MEC.Timing::_tmpBool
	bool ____tmpBool_26;
	// MEC.Segment MEC.Timing::_tmpSegment
	int32_t ____tmpSegment_27;
	// MEC.CoroutineHandle MEC.Timing::_tmpHandle
	CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ____tmpHandle_28;
	// System.Collections.Generic.Dictionary`2<MEC.CoroutineHandle,System.Collections.Generic.HashSet`1<MEC.CoroutineHandle>> MEC.Timing::Links
	Dictionary_2_t586C869F4A815C42AD5BEFA81A6BB784048C02BE* ___Links_65;
	// UnityEngine.WaitForEndOfFrame MEC.Timing::EofWaitObject
	WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663* ___EofWaitObject_66;
	// MEC.Timing[] MEC.Timing::ActiveInstances
	TimingU5BU5D_t67041BE600EC8358EF51AADDB3F90A999868AB1C* ___ActiveInstances_108;
	// MEC.Timing MEC.Timing::_instance
	Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* ____instance_109;
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Boolean[]
struct BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4  : public RuntimeArray
{
	ALIGN_FIELD (8) bool m_Items[1];

	inline bool GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline bool* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, bool value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline bool GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline bool* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, bool value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.IEnumerator`1<System.Single>[]
struct IEnumerator_1U5BU5D_tA2EF2FFDBA098C53558E3177C143C4DCFB0E5B88  : public RuntimeArray
{
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Int32[]
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C  : public RuntimeArray
{
	ALIGN_FIELD (8) int32_t m_Items[1];

	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248  : public RuntimeArray
{
	ALIGN_FIELD (8) String_t* m_Items[1];

	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// TResult System.Func`1<System.Boolean>::Invoke()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Func_1_Invoke_mBB7F37C468451AF57FAF31635C544D6B8C4373B2_gshared_inline (Func_1_t2BE7F58348C9CC544A8973B3A9E55541DE43C457* __this, const RuntimeMethod* method) ;
// TResult System.Func`2<System.Single,System.Single>::Invoke(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Func_2_Invoke_m5728ECFB038CFC6FEF889DC2D566EEF49D0E24B9_gshared_inline (Func_2_t2A7432CC4F64D0DF6D8629208B154CF139B39AF2* __this, float ___arg0, const RuntimeMethod* method) ;
// System.Void System.Action`1<System.Object>::Invoke(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_1_Invoke_mF2422B2DD29F74CE66F791C3F68E288EC7C3DB9E_gshared_inline (Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* __this, RuntimeObject* ___obj0, const RuntimeMethod* method) ;
// TValue System.Collections.Generic.Dictionary`2<MEC.Timing/ProcessIndex,MEC.CoroutineHandle>::get_Item(TKey)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 Dictionary_2_get_Item_mEE876E1A07892EE036FBCB85F5C96E283EADBEB7_gshared (Dictionary_2_t8C5B8A51DE825AB5FC3EDF04011A63CE33EBF2DB* __this, ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 ___key0, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.Dictionary`2<MEC.Timing/ProcessIndex,MEC.CoroutineHandle>::ContainsKey(TKey)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_ContainsKey_mD41C34E43A72893DA9509BA2C25ED1F65DED90E8_gshared (Dictionary_2_t8C5B8A51DE825AB5FC3EDF04011A63CE33EBF2DB* __this, ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 ___key0, const RuntimeMethod* method) ;
// TResult System.Func`3<System.Object,MEC.CoroutineHandle,System.Object>::Invoke(T1,T2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* Func_3_Invoke_m050A64479D76DDDE27033C49DBE096F3E5045E80_gshared_inline (Func_3_t54E59EE3AB750BDCF167CEAAB996EAD80E127845* __this, RuntimeObject* ___arg10, CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___arg21, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.Dictionary`2<MEC.CoroutineHandle,System.Object>::ContainsKey(TKey)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_ContainsKey_mBE3ACA58B3CAB2D1077AD725E8775525876D4F54_gshared (Dictionary_2_tA42ADF6FB18EDFFF7A9EFFECF8E63CA7E5AEF64E* __this, CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___key0, const RuntimeMethod* method) ;
// System.Void System.Func`3<System.Object,MEC.CoroutineHandle,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_3__ctor_m361C834A7E771E23547306659374BC4F75E01F42_gshared (Func_3_t54E59EE3AB750BDCF167CEAAB996EAD80E127845* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Boolean System.Nullable`1<System.Int32>::get_HasValue()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Nullable_1_get_HasValue_mCF2FD8B3055FA87FC9C504F2122B3B0FAEDE3EC9_gshared_inline (Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28* __this, const RuntimeMethod* method) ;
// System.String System.Nullable`1<System.Int32>::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Nullable_1_ToString_m5FDC0CBE068DB2893454257CE6E29846D47B3038_gshared (Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28* __this, const RuntimeMethod* method) ;
// T System.Nullable`1<System.Int32>::get_Value()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Nullable_1_get_Value_m0DF3B826A745419D26A168689AEB4BE6F8698ECA_gshared (Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28* __this, const RuntimeMethod* method) ;

// System.Void MECExtensionMethods2/<Delay>d__0::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDelayU3Ed__0__ctor_m10C85E378645F967756C6BEA612ADC16AFCEF598 (U3CDelayU3Ed__0_tB8C42C4BE4F8BDBD9A8EFC17DAF05BA5C06045BC* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void MECExtensionMethods2/<Delay>d__1::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDelayU3Ed__1__ctor_m2D974F25CF6320C7643BC594E73F5954E6CB7772 (U3CDelayU3Ed__1_tBD016C33189EAA123E44405BC6EDE65761067DFC* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void MECExtensionMethods2/<DelayFrames>d__3::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDelayFramesU3Ed__3__ctor_mE82DFCFD4E3073EA3695FC59341AA77F17F68E26 (U3CDelayFramesU3Ed__3_t59DE39C1E9B371904A1FA321BF5737F51BAA58E4* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void MECExtensionMethods2/<CancelWith>d__4::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCancelWithU3Ed__4__ctor_mBB2782280575A40C8D97441358D6DBA9DE0AA1D2 (U3CCancelWithU3Ed__4_t8B8BAEC6F1E6A2547D9B597A4343E3863B73203C* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void MECExtensionMethods2/<CancelWith>d__5::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCancelWithU3Ed__5__ctor_mE5CB9E3E5503412BCE13273FF4CEAF71146BF818 (U3CCancelWithU3Ed__5_t77CB416A3F92FD3BD432622F2D4FA46C9D1155F5* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void MECExtensionMethods2/<CancelWith>d__7::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCancelWithU3Ed__7__ctor_m36061336ECC2EE68DAF34C20E44F945038A636FC (U3CCancelWithU3Ed__7_t40CED40B36C8708B3F6BDFA2C421F36EE95CDAF7* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void MECExtensionMethods2/<PauseWith>d__8::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CPauseWithU3Ed__8__ctor_m21665018B1B93A435AC8685065568235841FCD01 (U3CPauseWithU3Ed__8_tF0B847125D7EE5CE9C57E7DC7FF416D0E5C794C0* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void MECExtensionMethods2/<PauseWith>d__9::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CPauseWithU3Ed__9__ctor_m174F914F1085EA2EBE33BF8FB35FE64AB510D5E5 (U3CPauseWithU3Ed__9_t5B5FAC5525B8593A92FA9BF46781B2DA2B8DC5ED* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void MECExtensionMethods2/<PauseWith>d__11::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CPauseWithU3Ed__11__ctor_m7B8EC6973FC3CB3AD38FA624D09DC60D3BB15292 (U3CPauseWithU3Ed__11_t5456BFBA17F259728246179E74467D248D6907EE* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void MECExtensionMethods2/<KillWith>d__12::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CKillWithU3Ed__12__ctor_mB27B4A9FE8CED4EA6EE6A0BED66CE4C2A7B78AC5 (U3CKillWithU3Ed__12_tC9D795656444FD23FDAD2843605F1E95509C8546* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void MECExtensionMethods2/<Append>d__13::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CAppendU3Ed__13__ctor_mE8C26EF341860BFCF2663634BC15276D43D402AD (U3CAppendU3Ed__13_tDA8C39AB4BF8F167235AE8A8F64E254A4BFF32E9* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void MECExtensionMethods2/<Append>d__14::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CAppendU3Ed__14__ctor_m8F179A0583E8FFB49E2D69516A59513115B9D42B (U3CAppendU3Ed__14_t0E472D9D963516368FE446E17DB0A22607539C15* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void MECExtensionMethods2/<Prepend>d__15::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CPrependU3Ed__15__ctor_mD88241B0D265A3F5FD24539D3EA596962A08F63F (U3CPrependU3Ed__15_t52DE12BFF91D5776D73148D2B29F7B452020EADB* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void MECExtensionMethods2/<Prepend>d__16::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CPrependU3Ed__16__ctor_m64A2A21EFC706D087AA24B51726031DD34D9B127 (U3CPrependU3Ed__16_t023603E04E5CBE20E04C0E48452C5CA1597C446E* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// MEC.Timing MEC.Timing::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* Timing_get_Instance_mF42E49C49C3AFADB18C9AEA52FE27BB5A3362C11 (const RuntimeMethod* method) ;
// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2::Superimpose(System.Collections.Generic.IEnumerator`1<System.Single>,System.Collections.Generic.IEnumerator`1<System.Single>,MEC.Timing)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MECExtensionMethods2_Superimpose_mE7408CF7D1A39CED17C7F9D9307A008150E75B28 (RuntimeObject* ___coroutineA0, RuntimeObject* ___coroutineB1, Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* ___instance2, const RuntimeMethod* method) ;
// System.Void MECExtensionMethods2/<Superimpose>d__18::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSuperimposeU3Ed__18__ctor_m14A89B8EF2C764E26ADDDCCE0DA6A3AEC00AB3BA (U3CSuperimposeU3Ed__18_tE9CABB59D41E90F0F203FB1529715342294EF1B6* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void MECExtensionMethods2/<Hijack>d__19::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CHijackU3Ed__19__ctor_m00BFFC3B2E7C61F564D9A62A3508833FF491DEAB (U3CHijackU3Ed__19_t57ED3438E560C43D619C0606A97C7FB3B547EBB1* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void MECExtensionMethods2/<RerouteExceptions>d__20::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CRerouteExceptionsU3Ed__20__ctor_mA318DE4F8F2A5AB948440CB6B7F63C2F41E3670D (U3CRerouteExceptionsU3Ed__20_t34352A3C593E8FF83A90E02879CEAD71D7417184* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2 (RuntimeObject* __this, const RuntimeMethod* method) ;
// System.Single MEC.Timing::WaitForSeconds(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Timing_WaitForSeconds_m54FB367D2A4605DF51BDBBD01D02208B25B57E2D (float ___waitTime0, const RuntimeMethod* method) ;
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* __this, const RuntimeMethod* method) ;
// TResult System.Func`1<System.Boolean>::Invoke()
inline bool Func_1_Invoke_mBB7F37C468451AF57FAF31635C544D6B8C4373B2_inline (Func_1_t2BE7F58348C9CC544A8973B3A9E55541DE43C457* __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Func_1_t2BE7F58348C9CC544A8973B3A9E55541DE43C457*, const RuntimeMethod*))Func_1_Invoke_mBB7F37C468451AF57FAF31635C544D6B8C4373B2_gshared_inline)(__this, method);
}
// System.Threading.Thread MEC.Timing::get_MainThread()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Thread_t0A773B9DE873D2DCAA7D229EAB36757B500E207F* Timing_get_MainThread_mFEE85EB21F421371C788BFF8E2D61D0FF21FFEAF_inline (const RuntimeMethod* method) ;
// System.Threading.Thread System.Threading.Thread::get_CurrentThread()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Thread_t0A773B9DE873D2DCAA7D229EAB36757B500E207F* Thread_get_CurrentThread_m835AD1DF1C0D10BABE1A5427CC4B357C991B25AB (const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Implicit_m18E1885C296CC868AC918101523697CFE6413C79 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___exists0, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.GameObject::get_activeInHierarchy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GameObject_get_activeInHierarchy_m49250F4F168DCC5388D5BE4F6A5681386907B109 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method) ;
// System.Boolean MEC.CoroutineHandle::get_IsRunning()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CoroutineHandle_get_IsRunning_m8E91075AE685F04BC4569D0E455CD4B10AE9B5AF (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, const RuntimeMethod* method) ;
// System.Void System.Action::Invoke()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_Invoke_m7126A54DACA72B845424072887B5F3A51FC3808E_inline (Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* __this, const RuntimeMethod* method) ;
// System.Boolean System.Single::IsNaN(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Single_IsNaN_m684B090AA2F895FD91821CA8684CBC11D784E4DD_inline (float ___f0, const RuntimeMethod* method) ;
// TResult System.Func`2<System.Single,System.Single>::Invoke(T)
inline float Func_2_Invoke_m5728ECFB038CFC6FEF889DC2D566EEF49D0E24B9_inline (Func_2_t2A7432CC4F64D0DF6D8629208B154CF139B39AF2* __this, float ___arg0, const RuntimeMethod* method)
{
	return ((  float (*) (Func_2_t2A7432CC4F64D0DF6D8629208B154CF139B39AF2*, float, const RuntimeMethod*))Func_2_Invoke_m5728ECFB038CFC6FEF889DC2D566EEF49D0E24B9_gshared_inline)(__this, ___arg0, method);
}
// System.Void System.Action`1<System.Exception>::Invoke(T)
inline void Action_1_Invoke_m43B5C4C0F292CE3E07CB03B46D8F960ACF7D6A58_inline (Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* __this, Exception_t* ___obj0, const RuntimeMethod* method)
{
	((  void (*) (Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04*, Exception_t*, const RuntimeMethod*))Action_1_Invoke_mF2422B2DD29F74CE66F791C3F68E288EC7C3DB9E_gshared_inline)(__this, ___obj0, method);
}
// System.Boolean MEC.Timing/ProcessIndex::Equals(MEC.Timing/ProcessIndex)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ProcessIndex_Equals_m0606720D3D1A042B4FEE328E47520C1F54FAAAFC (ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369* __this, ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 ___other0, const RuntimeMethod* method) ;
// System.Boolean MEC.Timing/ProcessIndex::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ProcessIndex_Equals_mE98E3DCDA417344D6838C798774A6B41F375D201 (ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369* __this, RuntimeObject* ___other0, const RuntimeMethod* method) ;
// System.Int32 MEC.Timing/ProcessIndex::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ProcessIndex_GetHashCode_m4AAF9C4E8445B9A0FBF925F8E51032FBA39250D7 (ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369* __this, const RuntimeMethod* method) ;
// System.Collections.IEnumerator MEC.Timing::_EOFPump()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Timing__EOFPump_m11B1F13D2395603365EE502A583B9397737F39F3 (Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* __this, const RuntimeMethod* method) ;
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* MonoBehaviour_StartCoroutine_m4CAFF732AA28CD3BDC5363B44A863575530EC812 (MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* __this, RuntimeObject* ___routine0, const RuntimeMethod* method) ;
// System.Boolean MEC.Timing::UpdateTimeValues(MEC.Segment)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Timing_UpdateTimeValues_m99DABFAC068CCA50B69C9C9F60405097A453EF72 (Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* __this, int32_t ___segment0, const RuntimeMethod* method) ;
// TValue System.Collections.Generic.Dictionary`2<MEC.Timing/ProcessIndex,MEC.CoroutineHandle>::get_Item(TKey)
inline CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 Dictionary_2_get_Item_mEE876E1A07892EE036FBCB85F5C96E283EADBEB7 (Dictionary_2_t8C5B8A51DE825AB5FC3EDF04011A63CE33EBF2DB* __this, ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 ___key0, const RuntimeMethod* method)
{
	return ((  CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 (*) (Dictionary_2_t8C5B8A51DE825AB5FC3EDF04011A63CE33EBF2DB*, ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369, const RuntimeMethod*))Dictionary_2_get_Item_mEE876E1A07892EE036FBCB85F5C96E283EADBEB7_gshared)(__this, ___key0, method);
}
// System.Void MEC.Timing::set_currentCoroutine(MEC.CoroutineHandle)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Timing_set_currentCoroutine_m3DBBE23CAA1674B5622501475023ECCF1BFAD433_inline (Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* __this, CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___value0, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.Dictionary`2<MEC.Timing/ProcessIndex,MEC.CoroutineHandle>::ContainsKey(TKey)
inline bool Dictionary_2_ContainsKey_mD41C34E43A72893DA9509BA2C25ED1F65DED90E8 (Dictionary_2_t8C5B8A51DE825AB5FC3EDF04011A63CE33EBF2DB* __this, ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 ___key0, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t8C5B8A51DE825AB5FC3EDF04011A63CE33EBF2DB*, ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369, const RuntimeMethod*))Dictionary_2_ContainsKey_mD41C34E43A72893DA9509BA2C25ED1F65DED90E8_gshared)(__this, ___key0, method);
}
// System.Int32 MEC.Timing::KillCoroutinesOnInstance(MEC.CoroutineHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Timing_KillCoroutinesOnInstance_m2FBCFCA895C9A7EF1AB2FD59D13503CDA9D69844 (Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* __this, CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___handle0, const RuntimeMethod* method) ;
// TResult System.Func`3<System.Collections.Generic.IEnumerator`1<System.Single>,MEC.CoroutineHandle,System.Collections.Generic.IEnumerator`1<System.Single>>::Invoke(T1,T2)
inline RuntimeObject* Func_3_Invoke_m449954D575F90EC3F3985278FB908B853AA03D7D_inline (Func_3_tEDDCF49DDA3D0F9DCB147948F764E9514FC2EB04* __this, RuntimeObject* ___arg10, CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___arg21, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (Func_3_tEDDCF49DDA3D0F9DCB147948F764E9514FC2EB04*, RuntimeObject*, CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772, const RuntimeMethod*))Func_3_Invoke_m050A64479D76DDDE27033C49DBE096F3E5045E80_gshared_inline)(__this, ___arg10, ___arg21, method);
}
// System.Void UnityEngine.Debug::LogException(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogException_m82E44FEC6B03BC34AFC2CAF6583051570C60CB9E (Exception_t* ___exception0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Debug::LogError(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogError_m059825802BB6AF7EA9693FEBEEB0D85F59A3E38E (RuntimeObject* ___message0, const RuntimeMethod* method) ;
// System.Void MEC.Timing/<_StartWhenDone>d__275::<>m__Finally1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_StartWhenDoneU3Ed__275_U3CU3Em__Finally1_m9A933C8961DBAE12BCBB9FCAF18A9A5DF0D4717D (U3C_StartWhenDoneU3Ed__275_t27CB96517B4094CEC4B93433D970071A204798F8* __this, const RuntimeMethod* method) ;
// System.Void MEC.Timing/<_StartWhenDone>d__275::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_StartWhenDoneU3Ed__275_System_IDisposable_Dispose_mB8C546FB49C07B7D67ABC7DCC0507B784F4457A1 (U3C_StartWhenDoneU3Ed__275_t27CB96517B4094CEC4B93433D970071A204798F8* __this, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.Dictionary`2<MEC.CoroutineHandle,System.Collections.Generic.HashSet`1<MEC.CoroutineHandle>>::ContainsKey(TKey)
inline bool Dictionary_2_ContainsKey_m8466E7777FED2755A8F5EF371415CBCF59589873 (Dictionary_2_t586C869F4A815C42AD5BEFA81A6BB784048C02BE* __this, CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___key0, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t586C869F4A815C42AD5BEFA81A6BB784048C02BE*, CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772, const RuntimeMethod*))Dictionary_2_ContainsKey_mBE3ACA58B3CAB2D1077AD725E8775525876D4F54_gshared)(__this, ___key0, method);
}
// System.Void MEC.Timing::CloseWaitingProcess(MEC.CoroutineHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Timing_CloseWaitingProcess_m15BC8AD80A4856793BCC789091F397889DA35056 (Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* __this, CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___handle0, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.AsyncOperation::get_isDone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AsyncOperation_get_isDone_m68A0682777E2132FC033182E9F50303566AA354D (AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C* __this, const RuntimeMethod* method) ;
// System.Void System.Func`3<System.Collections.Generic.IEnumerator`1<System.Single>,MEC.CoroutineHandle,System.Collections.Generic.IEnumerator`1<System.Single>>::.ctor(System.Object,System.IntPtr)
inline void Func_3__ctor_mF85581DC03C4A720CCD77E9CCD0407A9B1130A14 (Func_3_tEDDCF49DDA3D0F9DCB147948F764E9514FC2EB04* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_3_tEDDCF49DDA3D0F9DCB147948F764E9514FC2EB04*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_3__ctor_m361C834A7E771E23547306659374BC4F75E01F42_gshared)(__this, ___object0, ___method1, method);
}
// System.Single MEC.Timing::WaitForSecondsOnInstance(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Timing_WaitForSecondsOnInstance_m739CE37954128787A9E525694F27B727342F52ED (Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* __this, float ___waitTime0, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___x0, Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___y1, const RuntimeMethod* method) ;
// System.Byte MEC.CoroutineHandle::get_Key()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint8_t CoroutineHandle_get_Key_mCB7FA3D0481F9A13B0EC1FA22CB6C9AFA95F830B (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, const RuntimeMethod* method) ;
// System.Void MEC.CoroutineHandle::.ctor(System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CoroutineHandle__ctor_mCF4D58FEC43F4D8E816A39148B5C949B78CEB02D (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, uint8_t ___ind0, const RuntimeMethod* method) ;
// System.Void MEC.CoroutineHandle::.ctor(MEC.CoroutineHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CoroutineHandle__ctor_m786A900364E72BB4A3D58C80623E072B2E327EB6 (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___other0, const RuntimeMethod* method) ;
// System.Boolean MEC.CoroutineHandle::Equals(MEC.CoroutineHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CoroutineHandle_Equals_m376A1B4289D9266EA669B42997BD425DC59A81E3 (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___other0, const RuntimeMethod* method) ;
// System.Boolean MEC.CoroutineHandle::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CoroutineHandle_Equals_mE9A2F6DD86A6E40A3AC3B2C0B5717FB3154C3E95 (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, RuntimeObject* ___other0, const RuntimeMethod* method) ;
// System.Int32 MEC.CoroutineHandle::GetHashCode()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t CoroutineHandle_GetHashCode_m146A36001E4646CB9F3F4548A7DD789FF89928EA_inline (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, const RuntimeMethod* method) ;
// System.String MEC.Timing::GetTag(MEC.CoroutineHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Timing_GetTag_m3FFBD6D52CBACFE6084DB6369448FD2773DC490F (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___handle0, const RuntimeMethod* method) ;
// System.Nullable`1<System.Int32> MEC.Timing::GetLayer(MEC.CoroutineHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28 Timing_GetLayer_mE7DD05B2F69426F64199C1D0D1DDA25E089C73EF (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___handle0, const RuntimeMethod* method) ;
// System.Boolean System.Nullable`1<System.Int32>::get_HasValue()
inline bool Nullable_1_get_HasValue_mCF2FD8B3055FA87FC9C504F2122B3B0FAEDE3EC9_inline (Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28* __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28*, const RuntimeMethod*))Nullable_1_get_HasValue_mCF2FD8B3055FA87FC9C504F2122B3B0FAEDE3EC9_gshared_inline)(__this, method);
}
// System.String MEC.Timing::GetDebugName(MEC.CoroutineHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Timing_GetDebugName_m07F6E0F01437B37B14A09E9F9D7A540B86DDCB35 (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___handle0, const RuntimeMethod* method) ;
// System.String System.Nullable`1<System.Int32>::ToString()
inline String_t* Nullable_1_ToString_m5FDC0CBE068DB2893454257CE6E29846D47B3038 (Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28* __this, const RuntimeMethod* method)
{
	return ((  String_t* (*) (Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28*, const RuntimeMethod*))Nullable_1_ToString_m5FDC0CBE068DB2893454257CE6E29846D47B3038_gshared)(__this, method);
}
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0 (String_t* ___str00, String_t* ___str11, String_t* ___str22, const RuntimeMethod* method) ;
// System.String System.String::Concat(System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0 (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___values0, const RuntimeMethod* method) ;
// System.String MEC.CoroutineHandle::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* CoroutineHandle_ToString_m46BBC999985604BAB9F8DEBDF875F93A986C5298 (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, const RuntimeMethod* method) ;
// System.String MEC.CoroutineHandle::get_Tag()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* CoroutineHandle_get_Tag_m98BDDB7DDC278A3F30EBA5E7F7376FDB6F0BA189 (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, const RuntimeMethod* method) ;
// System.Boolean MEC.Timing::SetTag(MEC.CoroutineHandle,System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Timing_SetTag_m96D6A4058545C0BBB5504EDF8758E3131F6E8A00 (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___handle0, String_t* ___newTag1, bool ___overwriteExisting2, const RuntimeMethod* method) ;
// System.Void MEC.CoroutineHandle::set_Tag(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CoroutineHandle_set_Tag_mDF98CC39D94E8F5E16464FDC13545A7C07A7BE43 (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, String_t* ___value0, const RuntimeMethod* method) ;
// System.Nullable`1<System.Int32> MEC.CoroutineHandle::get_Layer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28 CoroutineHandle_get_Layer_m4A8CE0F0E6F049A8C18A75064C20BE1060D52936 (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, const RuntimeMethod* method) ;
// System.Boolean MEC.Timing::RemoveLayer(MEC.CoroutineHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Timing_RemoveLayer_m4788A8DAF5636900A5A0A88F02BB1ABAE7CCE37F (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___handle0, const RuntimeMethod* method) ;
// T System.Nullable`1<System.Int32>::get_Value()
inline int32_t Nullable_1_get_Value_m0DF3B826A745419D26A168689AEB4BE6F8698ECA (Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28* __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28*, const RuntimeMethod*))Nullable_1_get_Value_m0DF3B826A745419D26A168689AEB4BE6F8698ECA_gshared)(__this, method);
}
// System.Boolean MEC.Timing::SetLayer(MEC.CoroutineHandle,System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Timing_SetLayer_m55CB305A12F67D268554C3FA8966C612FB64966D (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___handle0, int32_t ___newLayer1, bool ___overwriteExisting2, const RuntimeMethod* method) ;
// System.Void MEC.CoroutineHandle::set_Layer(System.Nullable`1<System.Int32>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CoroutineHandle_set_Layer_mA6B53D3EC1ED25970E3745D1908806C99DCACD9B (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28 ___value0, const RuntimeMethod* method) ;
// MEC.Segment MEC.Timing::GetSegment(MEC.CoroutineHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Timing_GetSegment_m8EBC2B3D9D10027B6576BD5A722F817A94B68315 (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___handle0, const RuntimeMethod* method) ;
// MEC.Segment MEC.CoroutineHandle::get_Segment()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t CoroutineHandle_get_Segment_m39312DC1EB22C830F2104324B99C3A146F5D38D0 (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, const RuntimeMethod* method) ;
// System.Boolean MEC.Timing::SetSegment(MEC.CoroutineHandle,MEC.Segment)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Timing_SetSegment_mE4271A8038182019ED1C10264932B8139946BC04 (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___handle0, int32_t ___newSegment1, const RuntimeMethod* method) ;
// System.Void MEC.CoroutineHandle::set_Segment(MEC.Segment)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CoroutineHandle_set_Segment_m6922E71F480119164D54F04D8184EA61EC3EA52D (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, int32_t ___value0, const RuntimeMethod* method) ;
// System.Boolean MEC.Timing::IsRunning(MEC.CoroutineHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Timing_IsRunning_mA3836D665CA50666A4A3A56F711EEABF2769A41A (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___handle0, const RuntimeMethod* method) ;
// System.Int32 MEC.Timing::KillCoroutines(MEC.CoroutineHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Timing_KillCoroutines_mFF74632A9C79E88FC999E971D02118CCEE9F113D (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___handle0, const RuntimeMethod* method) ;
// System.Void MEC.CoroutineHandle::set_IsRunning(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CoroutineHandle_set_IsRunning_mF8D1574108AEEB339D75232A75F4E0625D49D659 (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, bool ___value0, const RuntimeMethod* method) ;
// System.Boolean MEC.Timing::IsAliveAndPaused(MEC.CoroutineHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Timing_IsAliveAndPaused_m33387BF08F0E6871DF34D62EB6E9AC7B687863BF (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___handle0, const RuntimeMethod* method) ;
// System.Boolean MEC.CoroutineHandle::get_IsAliveAndPaused()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CoroutineHandle_get_IsAliveAndPaused_mC771E398960AA40366D3EE3BB8642478B3DDFABC (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, const RuntimeMethod* method) ;
// System.Int32 MEC.Timing::PauseCoroutines(MEC.CoroutineHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Timing_PauseCoroutines_mDC51884B61589946D922A013860F23A23A3AA29A (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___handle0, const RuntimeMethod* method) ;
// System.Int32 MEC.Timing::ResumeCoroutines(MEC.CoroutineHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Timing_ResumeCoroutines_m4472CFAFC1E76EE2ADB7921DC8AD2B0886BE1BE2 (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___handle0, const RuntimeMethod* method) ;
// System.Void MEC.CoroutineHandle::set_IsAliveAndPaused(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CoroutineHandle_set_IsAliveAndPaused_m8B313E39221D848E7F6974FE85288ADCA1CE28B2 (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, bool ___value0, const RuntimeMethod* method) ;
// System.Boolean MEC.CoroutineHandle::get_IsValid()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CoroutineHandle_get_IsValid_mA4DA81202B56F396DABF930A009CCF65F34FAD53 (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, const RuntimeMethod* method) ;
// MEC.Timing MEC.Timing::GetInstance(System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* Timing_GetInstance_m501B99433E8888DB426B38439FFB8FC45BE32411 (uint8_t ___ID0, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mD3DB0D72CE0250C84033DC2A90AEF9D59896E536 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___x0, Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___y1, const RuntimeMethod* method) ;
// System.Collections.Generic.IEnumerator`1<System.Single> MEC.CoroutineHandle::_OnDestroy(MEC.CoroutineHandle,System.Action)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* CoroutineHandle__OnDestroy_m137647C5EE847036CD98B31AA4F576A9261F82CE (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___watched0, Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___action1, const RuntimeMethod* method) ;
// MEC.CoroutineHandle MEC.Timing::RunCoroutineOnInstance(System.Collections.Generic.IEnumerator`1<System.Single>,MEC.Segment)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 Timing_RunCoroutineOnInstance_m2DF151338C742F53D0D8DD15C7FB624CBBD54FFE (Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* __this, RuntimeObject* ___coroutine0, int32_t ___segment1, const RuntimeMethod* method) ;
// MEC.CoroutineHandle MEC.CoroutineHandle::OnDestroy(System.Action,MEC.Segment)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 CoroutineHandle_OnDestroy_mC92C498C04D09E4648811A45E865659914D4A7AD (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___action0, int32_t ___segment1, const RuntimeMethod* method) ;
// System.Collections.Generic.IEnumerator`1<System.Single> MEC.CoroutineHandle::_OnDestroy(MEC.CoroutineHandle,System.Collections.Generic.IEnumerator`1<System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* CoroutineHandle__OnDestroy_mC0A14D883FC5554696AC37A00976393EC544B852 (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___watched0, RuntimeObject* ___action1, const RuntimeMethod* method) ;
// MEC.CoroutineHandle MEC.CoroutineHandle::OnDestroy(System.Collections.Generic.IEnumerator`1<System.Single>,MEC.Segment)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 CoroutineHandle_OnDestroy_m59FB02686E0DE9D99450DBFA50597951B4B3F795 (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, RuntimeObject* ___action0, int32_t ___segment1, const RuntimeMethod* method) ;
// System.Void MEC.CoroutineHandle/<_OnDestroy>d__32::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_OnDestroyU3Ed__32__ctor_m58B90BD1AF3010BBBC15328A4C0DAC4BD06C51A3 (U3C_OnDestroyU3Ed__32_tC237D236161175615D4FE20E0390919C6A0C1E1A* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void MEC.CoroutineHandle/<_OnDestroy>d__33::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_OnDestroyU3Ed__33__ctor_m4E8DEDC116D47F081B5144ABFE30D1BDF1CA67B0 (U3C_OnDestroyU3Ed__33_tBEBAF5C594D7B23973CC4FD41D86F37596AAA5F2* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// MEC.CoroutineHandle MEC.Timing::RunCoroutine(System.Collections.Generic.IEnumerator`1<System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 Timing_RunCoroutine_m305561CB26854D87B49610D24312D68C3E5F5EC0 (RuntimeObject* ___coroutine0, const RuntimeMethod* method) ;
// MEC.CoroutineHandle MEC.Timing::RunCoroutine(System.Collections.Generic.IEnumerator`1<System.Single>,UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 Timing_RunCoroutine_m2DFC1726A41097DCF9BA8D0477EED8CC1BF38227 (RuntimeObject* ___coroutine0, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___gameObj1, const RuntimeMethod* method) ;
// MEC.CoroutineHandle MEC.Timing::RunCoroutine(System.Collections.Generic.IEnumerator`1<System.Single>,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 Timing_RunCoroutine_mC2FA00211167549EE8A67379A2E5EA51C2B1AB7E (RuntimeObject* ___coroutine0, int32_t ___layer1, const RuntimeMethod* method) ;
// MEC.CoroutineHandle MEC.Timing::RunCoroutine(System.Collections.Generic.IEnumerator`1<System.Single>,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 Timing_RunCoroutine_m2E4FCC8B56FDBA931588D91A3EAE3BD4B930F295 (RuntimeObject* ___coroutine0, String_t* ___tag1, const RuntimeMethod* method) ;
// MEC.CoroutineHandle MEC.Timing::RunCoroutine(System.Collections.Generic.IEnumerator`1<System.Single>,UnityEngine.GameObject,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 Timing_RunCoroutine_m6D96D9EF50C4E426D04CCD05321C1EF167B0F291 (RuntimeObject* ___coroutine0, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___gameObj1, String_t* ___tag2, const RuntimeMethod* method) ;
// MEC.CoroutineHandle MEC.Timing::RunCoroutine(System.Collections.Generic.IEnumerator`1<System.Single>,System.Int32,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 Timing_RunCoroutine_m6E6FBC58F3524DB68EFFF7EE323D100D17B329D3 (RuntimeObject* ___coroutine0, int32_t ___layer1, String_t* ___tag2, const RuntimeMethod* method) ;
// MEC.CoroutineHandle MEC.Timing::RunCoroutine(System.Collections.Generic.IEnumerator`1<System.Single>,MEC.Segment)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 Timing_RunCoroutine_m0AAC38EFC113F9F0E3863713D0DFEBF89AD3AC02 (RuntimeObject* ___coroutine0, int32_t ___segment1, const RuntimeMethod* method) ;
// MEC.CoroutineHandle MEC.Timing::RunCoroutine(System.Collections.Generic.IEnumerator`1<System.Single>,MEC.Segment,UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 Timing_RunCoroutine_m9F766688EFD73238C991A00887473588C644B942 (RuntimeObject* ___coroutine0, int32_t ___segment1, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___gameObj2, const RuntimeMethod* method) ;
// MEC.CoroutineHandle MEC.Timing::RunCoroutine(System.Collections.Generic.IEnumerator`1<System.Single>,MEC.Segment,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 Timing_RunCoroutine_m3D86C97DCD708C04E2B4B95125C261C198F6F118 (RuntimeObject* ___coroutine0, int32_t ___segment1, int32_t ___layer2, const RuntimeMethod* method) ;
// MEC.CoroutineHandle MEC.Timing::RunCoroutine(System.Collections.Generic.IEnumerator`1<System.Single>,MEC.Segment,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 Timing_RunCoroutine_mD29B39A24A15D8D91F327F5BAB1DF1E8B150E693 (RuntimeObject* ___coroutine0, int32_t ___segment1, String_t* ___tag2, const RuntimeMethod* method) ;
// MEC.CoroutineHandle MEC.Timing::RunCoroutine(System.Collections.Generic.IEnumerator`1<System.Single>,MEC.Segment,UnityEngine.GameObject,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 Timing_RunCoroutine_mE568B2CBC2311DD24DCD39B7FC1781E0E84931FA (RuntimeObject* ___coroutine0, int32_t ___segment1, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___gameObj2, String_t* ___tag3, const RuntimeMethod* method) ;
// MEC.CoroutineHandle MEC.Timing::RunCoroutine(System.Collections.Generic.IEnumerator`1<System.Single>,MEC.Segment,System.Int32,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 Timing_RunCoroutine_m93D5FAC1E8FC571E40C5E315BA2E9912E8E2AB8F (RuntimeObject* ___coroutine0, int32_t ___segment1, int32_t ___layer2, String_t* ___tag3, const RuntimeMethod* method) ;
// MEC.CoroutineHandle MEC.Timing::RunCoroutineSingleton(System.Collections.Generic.IEnumerator`1<System.Single>,MEC.CoroutineHandle,MEC.SingletonBehavior)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 Timing_RunCoroutineSingleton_mE984BA10D2E2D367B4670A056B38BA7DD6895E9B (RuntimeObject* ___coroutine0, CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___handle1, int32_t ___behaviorOnCollision2, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Object::GetInstanceID()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Object_GetInstanceID_m554FF4073C9465F3835574CC084E68AAEEC6CC6A (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* __this, const RuntimeMethod* method) ;
// MEC.CoroutineHandle MEC.Timing::RunCoroutineSingleton(System.Collections.Generic.IEnumerator`1<System.Single>,System.Int32,MEC.SingletonBehavior)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 Timing_RunCoroutineSingleton_m86461102E88A1FF95E541A2AD2AEB8FF6AC3FA70 (RuntimeObject* ___coroutine0, int32_t ___layer1, int32_t ___behaviorOnCollision2, const RuntimeMethod* method) ;
// MEC.CoroutineHandle MEC.Timing::RunCoroutineSingleton(System.Collections.Generic.IEnumerator`1<System.Single>,System.String,MEC.SingletonBehavior)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 Timing_RunCoroutineSingleton_mE48BB72B57C3EAC7B635C2ABF37B9F84BCEC7CC8 (RuntimeObject* ___coroutine0, String_t* ___tag1, int32_t ___behaviorOnCollision2, const RuntimeMethod* method) ;
// MEC.CoroutineHandle MEC.Timing::RunCoroutineSingleton(System.Collections.Generic.IEnumerator`1<System.Single>,System.Int32,System.String,MEC.SingletonBehavior)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 Timing_RunCoroutineSingleton_m40F0A0B1B877BA62D7F4F85582142D64363C5347 (RuntimeObject* ___coroutine0, int32_t ___layer1, String_t* ___tag2, int32_t ___behaviorOnCollision3, const RuntimeMethod* method) ;
// MEC.CoroutineHandle MEC.Timing::RunCoroutineSingleton(System.Collections.Generic.IEnumerator`1<System.Single>,MEC.CoroutineHandle,MEC.Segment,MEC.SingletonBehavior)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 Timing_RunCoroutineSingleton_m1F53682D5A2464E16C3E9ED35811674552A63CBD (RuntimeObject* ___coroutine0, CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___handle1, int32_t ___segment2, int32_t ___behaviorOnCollision3, const RuntimeMethod* method) ;
// MEC.CoroutineHandle MEC.Timing::RunCoroutineSingleton(System.Collections.Generic.IEnumerator`1<System.Single>,MEC.Segment,System.Int32,MEC.SingletonBehavior)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 Timing_RunCoroutineSingleton_mCC729C918EA4FE33811DB7EAA33DC85939176CB3 (RuntimeObject* ___coroutine0, int32_t ___segment1, int32_t ___layer2, int32_t ___behaviorOnCollision3, const RuntimeMethod* method) ;
// MEC.CoroutineHandle MEC.Timing::RunCoroutineSingleton(System.Collections.Generic.IEnumerator`1<System.Single>,MEC.Segment,System.String,MEC.SingletonBehavior)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 Timing_RunCoroutineSingleton_mE7A0FEEF326DF483DBDB1172E5C84D4E8D799297 (RuntimeObject* ___coroutine0, int32_t ___segment1, String_t* ___tag2, int32_t ___behaviorOnCollision3, const RuntimeMethod* method) ;
// MEC.CoroutineHandle MEC.Timing::RunCoroutineSingleton(System.Collections.Generic.IEnumerator`1<System.Single>,MEC.Segment,System.Int32,System.String,MEC.SingletonBehavior)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 Timing_RunCoroutineSingleton_mA27E8D821C8C70B02EA97C9203FA1F2218C62A11 (RuntimeObject* ___coroutine0, int32_t ___segment1, int32_t ___layer2, String_t* ___tag3, int32_t ___behaviorOnCollision4, const RuntimeMethod* method) ;
// System.Single MEC.Timing::WaitUntilDone(System.Collections.Generic.IEnumerator`1<System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Timing_WaitUntilDone_m2737A996DF3BF053C9FE5E863D0E86EDC86CF6BD (RuntimeObject* ___newCoroutine0, const RuntimeMethod* method) ;
// System.Single MEC.Timing::WaitUntilDone(System.Collections.Generic.IEnumerator`1<System.Single>,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Timing_WaitUntilDone_mE9138099BCA19B24FDD72B0CF2DB652BEC604A68 (RuntimeObject* ___newCoroutine0, String_t* ___tag1, const RuntimeMethod* method) ;
// System.Single MEC.Timing::WaitUntilDone(System.Collections.Generic.IEnumerator`1<System.Single>,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Timing_WaitUntilDone_m402AEBABC3D70225AB598519246EA7934B4136DF (RuntimeObject* ___newCoroutine0, int32_t ___layer1, const RuntimeMethod* method) ;
// System.Single MEC.Timing::WaitUntilDone(System.Collections.Generic.IEnumerator`1<System.Single>,System.Int32,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Timing_WaitUntilDone_mCB62F3DAA912CEB2C28DCB3241C025766958B8CC (RuntimeObject* ___newCoroutine0, int32_t ___layer1, String_t* ___tag2, const RuntimeMethod* method) ;
// System.Single MEC.Timing::WaitUntilDone(System.Collections.Generic.IEnumerator`1<System.Single>,MEC.Segment)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Timing_WaitUntilDone_mD2C9B888066BB955B7F05A8EEDDC8E23350C717A (RuntimeObject* ___newCoroutine0, int32_t ___segment1, const RuntimeMethod* method) ;
// System.Single MEC.Timing::WaitUntilDone(System.Collections.Generic.IEnumerator`1<System.Single>,MEC.Segment,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Timing_WaitUntilDone_m1CE5D130C79F46912ABA99904C4D43BAA497FA8A (RuntimeObject* ___newCoroutine0, int32_t ___segment1, String_t* ___tag2, const RuntimeMethod* method) ;
// System.Single MEC.Timing::WaitUntilDone(System.Collections.Generic.IEnumerator`1<System.Single>,MEC.Segment,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Timing_WaitUntilDone_mC4C05E48616C85A0F5B73C0A1CF29F067670C13D (RuntimeObject* ___newCoroutine0, int32_t ___segment1, int32_t ___layer2, const RuntimeMethod* method) ;
// System.Single MEC.Timing::WaitUntilDone(System.Collections.Generic.IEnumerator`1<System.Single>,MEC.Segment,System.Int32,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Timing_WaitUntilDone_m78F49DF57ADB37D5F3C0A7BA22905599B5A57C91 (RuntimeObject* ___newCoroutine0, int32_t ___segment1, int32_t ___layer2, String_t* ___tag3, const RuntimeMethod* method) ;
// System.Int32 System.BitConverter::SingleToInt32Bits(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t BitConverter_SingleToInt32Bits_mA1902D40966CA4C89A8974B10E5680A06E88566B_inline (float ___value0, const RuntimeMethod* method) ;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2::Delay(System.Collections.Generic.IEnumerator`1<System.Single>,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MECExtensionMethods2_Delay_mC5AE0E342896648811D2F719CB9ECEEFE41A58C4 (RuntimeObject* ___coroutine0, float ___timeToDelay1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDelayU3Ed__0_tB8C42C4BE4F8BDBD9A8EFC17DAF05BA5C06045BC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CDelayU3Ed__0_tB8C42C4BE4F8BDBD9A8EFC17DAF05BA5C06045BC* L_0 = (U3CDelayU3Ed__0_tB8C42C4BE4F8BDBD9A8EFC17DAF05BA5C06045BC*)il2cpp_codegen_object_new(U3CDelayU3Ed__0_tB8C42C4BE4F8BDBD9A8EFC17DAF05BA5C06045BC_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CDelayU3Ed__0__ctor_m10C85E378645F967756C6BEA612ADC16AFCEF598(L_0, 0, NULL);
		U3CDelayU3Ed__0_tB8C42C4BE4F8BDBD9A8EFC17DAF05BA5C06045BC* L_1 = L_0;
		RuntimeObject* L_2 = ___coroutine0;
		NullCheck(L_1);
		L_1->___coroutine_3 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___coroutine_3), (void*)L_2);
		U3CDelayU3Ed__0_tB8C42C4BE4F8BDBD9A8EFC17DAF05BA5C06045BC* L_3 = L_1;
		float L_4 = ___timeToDelay1;
		NullCheck(L_3);
		L_3->___timeToDelay_2 = L_4;
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2::Delay(System.Collections.Generic.IEnumerator`1<System.Single>,System.Func`1<System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MECExtensionMethods2_Delay_mFA403E8FDAE26DFF4A7C7F9C6FF1046D135067FC (RuntimeObject* ___coroutine0, Func_1_t2BE7F58348C9CC544A8973B3A9E55541DE43C457* ___condition1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDelayU3Ed__1_tBD016C33189EAA123E44405BC6EDE65761067DFC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CDelayU3Ed__1_tBD016C33189EAA123E44405BC6EDE65761067DFC* L_0 = (U3CDelayU3Ed__1_tBD016C33189EAA123E44405BC6EDE65761067DFC*)il2cpp_codegen_object_new(U3CDelayU3Ed__1_tBD016C33189EAA123E44405BC6EDE65761067DFC_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CDelayU3Ed__1__ctor_m2D974F25CF6320C7643BC594E73F5954E6CB7772(L_0, 0, NULL);
		U3CDelayU3Ed__1_tBD016C33189EAA123E44405BC6EDE65761067DFC* L_1 = L_0;
		RuntimeObject* L_2 = ___coroutine0;
		NullCheck(L_1);
		L_1->___coroutine_3 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___coroutine_3), (void*)L_2);
		U3CDelayU3Ed__1_tBD016C33189EAA123E44405BC6EDE65761067DFC* L_3 = L_1;
		Func_1_t2BE7F58348C9CC544A8973B3A9E55541DE43C457* L_4 = ___condition1;
		NullCheck(L_3);
		L_3->___condition_2 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&L_3->___condition_2), (void*)L_4);
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2::DelayFrames(System.Collections.Generic.IEnumerator`1<System.Single>,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MECExtensionMethods2_DelayFrames_m41E0AD92E4221B1E32A3DFD1232C269E84282042 (RuntimeObject* ___coroutine0, int32_t ___framesToDelay1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDelayFramesU3Ed__3_t59DE39C1E9B371904A1FA321BF5737F51BAA58E4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CDelayFramesU3Ed__3_t59DE39C1E9B371904A1FA321BF5737F51BAA58E4* L_0 = (U3CDelayFramesU3Ed__3_t59DE39C1E9B371904A1FA321BF5737F51BAA58E4*)il2cpp_codegen_object_new(U3CDelayFramesU3Ed__3_t59DE39C1E9B371904A1FA321BF5737F51BAA58E4_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CDelayFramesU3Ed__3__ctor_mE82DFCFD4E3073EA3695FC59341AA77F17F68E26(L_0, 0, NULL);
		U3CDelayFramesU3Ed__3_t59DE39C1E9B371904A1FA321BF5737F51BAA58E4* L_1 = L_0;
		RuntimeObject* L_2 = ___coroutine0;
		NullCheck(L_1);
		L_1->___coroutine_3 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___coroutine_3), (void*)L_2);
		U3CDelayFramesU3Ed__3_t59DE39C1E9B371904A1FA321BF5737F51BAA58E4* L_3 = L_1;
		int32_t L_4 = ___framesToDelay1;
		NullCheck(L_3);
		L_3->___framesToDelay_2 = L_4;
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2::CancelWith(System.Collections.Generic.IEnumerator`1<System.Single>,UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MECExtensionMethods2_CancelWith_m6A2D5B57068C730CF96C580E4AA9D6EF325A7161 (RuntimeObject* ___coroutine0, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___gameObject1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCancelWithU3Ed__4_t8B8BAEC6F1E6A2547D9B597A4343E3863B73203C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CCancelWithU3Ed__4_t8B8BAEC6F1E6A2547D9B597A4343E3863B73203C* L_0 = (U3CCancelWithU3Ed__4_t8B8BAEC6F1E6A2547D9B597A4343E3863B73203C*)il2cpp_codegen_object_new(U3CCancelWithU3Ed__4_t8B8BAEC6F1E6A2547D9B597A4343E3863B73203C_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CCancelWithU3Ed__4__ctor_mBB2782280575A40C8D97441358D6DBA9DE0AA1D2(L_0, 0, NULL);
		U3CCancelWithU3Ed__4_t8B8BAEC6F1E6A2547D9B597A4343E3863B73203C* L_1 = L_0;
		RuntimeObject* L_2 = ___coroutine0;
		NullCheck(L_1);
		L_1->___coroutine_2 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___coroutine_2), (void*)L_2);
		U3CCancelWithU3Ed__4_t8B8BAEC6F1E6A2547D9B597A4343E3863B73203C* L_3 = L_1;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = ___gameObject1;
		NullCheck(L_3);
		L_3->___gameObject_3 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&L_3->___gameObject_3), (void*)L_4);
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2::CancelWith(System.Collections.Generic.IEnumerator`1<System.Single>,UnityEngine.GameObject,UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MECExtensionMethods2_CancelWith_mC165419692D3BAEB8570BB0B282EBBB675FD49DC (RuntimeObject* ___coroutine0, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___gameObject11, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___gameObject22, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCancelWithU3Ed__5_t77CB416A3F92FD3BD432622F2D4FA46C9D1155F5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CCancelWithU3Ed__5_t77CB416A3F92FD3BD432622F2D4FA46C9D1155F5* L_0 = (U3CCancelWithU3Ed__5_t77CB416A3F92FD3BD432622F2D4FA46C9D1155F5*)il2cpp_codegen_object_new(U3CCancelWithU3Ed__5_t77CB416A3F92FD3BD432622F2D4FA46C9D1155F5_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CCancelWithU3Ed__5__ctor_mE5CB9E3E5503412BCE13273FF4CEAF71146BF818(L_0, 0, NULL);
		U3CCancelWithU3Ed__5_t77CB416A3F92FD3BD432622F2D4FA46C9D1155F5* L_1 = L_0;
		RuntimeObject* L_2 = ___coroutine0;
		NullCheck(L_1);
		L_1->___coroutine_2 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___coroutine_2), (void*)L_2);
		U3CCancelWithU3Ed__5_t77CB416A3F92FD3BD432622F2D4FA46C9D1155F5* L_3 = L_1;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = ___gameObject11;
		NullCheck(L_3);
		L_3->___gameObject1_3 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&L_3->___gameObject1_3), (void*)L_4);
		U3CCancelWithU3Ed__5_t77CB416A3F92FD3BD432622F2D4FA46C9D1155F5* L_5 = L_3;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = ___gameObject22;
		NullCheck(L_5);
		L_5->___gameObject2_4 = L_6;
		Il2CppCodeGenWriteBarrier((void**)(&L_5->___gameObject2_4), (void*)L_6);
		return L_5;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2::CancelWith(System.Collections.Generic.IEnumerator`1<System.Single>,System.Func`1<System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MECExtensionMethods2_CancelWith_mFC3AD67B2E9804E7D2702D56579FA3E5BB85C1D9 (RuntimeObject* ___coroutine0, Func_1_t2BE7F58348C9CC544A8973B3A9E55541DE43C457* ___condition1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCancelWithU3Ed__7_t40CED40B36C8708B3F6BDFA2C421F36EE95CDAF7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CCancelWithU3Ed__7_t40CED40B36C8708B3F6BDFA2C421F36EE95CDAF7* L_0 = (U3CCancelWithU3Ed__7_t40CED40B36C8708B3F6BDFA2C421F36EE95CDAF7*)il2cpp_codegen_object_new(U3CCancelWithU3Ed__7_t40CED40B36C8708B3F6BDFA2C421F36EE95CDAF7_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CCancelWithU3Ed__7__ctor_m36061336ECC2EE68DAF34C20E44F945038A636FC(L_0, 0, NULL);
		U3CCancelWithU3Ed__7_t40CED40B36C8708B3F6BDFA2C421F36EE95CDAF7* L_1 = L_0;
		RuntimeObject* L_2 = ___coroutine0;
		NullCheck(L_1);
		L_1->___coroutine_3 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___coroutine_3), (void*)L_2);
		U3CCancelWithU3Ed__7_t40CED40B36C8708B3F6BDFA2C421F36EE95CDAF7* L_3 = L_1;
		Func_1_t2BE7F58348C9CC544A8973B3A9E55541DE43C457* L_4 = ___condition1;
		NullCheck(L_3);
		L_3->___condition_2 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&L_3->___condition_2), (void*)L_4);
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2::PauseWith(System.Collections.Generic.IEnumerator`1<System.Single>,UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MECExtensionMethods2_PauseWith_m9DB3E81864081841F0AB839A95406B08B0EBEB8F (RuntimeObject* ___coroutine0, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___gameObject1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPauseWithU3Ed__8_tF0B847125D7EE5CE9C57E7DC7FF416D0E5C794C0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CPauseWithU3Ed__8_tF0B847125D7EE5CE9C57E7DC7FF416D0E5C794C0* L_0 = (U3CPauseWithU3Ed__8_tF0B847125D7EE5CE9C57E7DC7FF416D0E5C794C0*)il2cpp_codegen_object_new(U3CPauseWithU3Ed__8_tF0B847125D7EE5CE9C57E7DC7FF416D0E5C794C0_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CPauseWithU3Ed__8__ctor_m21665018B1B93A435AC8685065568235841FCD01(L_0, 0, NULL);
		U3CPauseWithU3Ed__8_tF0B847125D7EE5CE9C57E7DC7FF416D0E5C794C0* L_1 = L_0;
		RuntimeObject* L_2 = ___coroutine0;
		NullCheck(L_1);
		L_1->___coroutine_3 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___coroutine_3), (void*)L_2);
		U3CPauseWithU3Ed__8_tF0B847125D7EE5CE9C57E7DC7FF416D0E5C794C0* L_3 = L_1;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = ___gameObject1;
		NullCheck(L_3);
		L_3->___gameObject_2 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&L_3->___gameObject_2), (void*)L_4);
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2::PauseWith(System.Collections.Generic.IEnumerator`1<System.Single>,UnityEngine.GameObject,UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MECExtensionMethods2_PauseWith_mD5772F1CD7261CEE6E45CE0D937F26F48E6C17B3 (RuntimeObject* ___coroutine0, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___gameObject11, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___gameObject22, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPauseWithU3Ed__9_t5B5FAC5525B8593A92FA9BF46781B2DA2B8DC5ED_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CPauseWithU3Ed__9_t5B5FAC5525B8593A92FA9BF46781B2DA2B8DC5ED* L_0 = (U3CPauseWithU3Ed__9_t5B5FAC5525B8593A92FA9BF46781B2DA2B8DC5ED*)il2cpp_codegen_object_new(U3CPauseWithU3Ed__9_t5B5FAC5525B8593A92FA9BF46781B2DA2B8DC5ED_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CPauseWithU3Ed__9__ctor_m174F914F1085EA2EBE33BF8FB35FE64AB510D5E5(L_0, 0, NULL);
		U3CPauseWithU3Ed__9_t5B5FAC5525B8593A92FA9BF46781B2DA2B8DC5ED* L_1 = L_0;
		RuntimeObject* L_2 = ___coroutine0;
		NullCheck(L_1);
		L_1->___coroutine_4 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___coroutine_4), (void*)L_2);
		U3CPauseWithU3Ed__9_t5B5FAC5525B8593A92FA9BF46781B2DA2B8DC5ED* L_3 = L_1;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = ___gameObject11;
		NullCheck(L_3);
		L_3->___gameObject1_2 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&L_3->___gameObject1_2), (void*)L_4);
		U3CPauseWithU3Ed__9_t5B5FAC5525B8593A92FA9BF46781B2DA2B8DC5ED* L_5 = L_3;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = ___gameObject22;
		NullCheck(L_5);
		L_5->___gameObject2_3 = L_6;
		Il2CppCodeGenWriteBarrier((void**)(&L_5->___gameObject2_3), (void*)L_6);
		return L_5;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2::PauseWith(System.Collections.Generic.IEnumerator`1<System.Single>,System.Func`1<System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MECExtensionMethods2_PauseWith_m748259D48AAE7F1CE62E3232E54D9FB3013B3F95 (RuntimeObject* ___coroutine0, Func_1_t2BE7F58348C9CC544A8973B3A9E55541DE43C457* ___condition1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPauseWithU3Ed__11_t5456BFBA17F259728246179E74467D248D6907EE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CPauseWithU3Ed__11_t5456BFBA17F259728246179E74467D248D6907EE* L_0 = (U3CPauseWithU3Ed__11_t5456BFBA17F259728246179E74467D248D6907EE*)il2cpp_codegen_object_new(U3CPauseWithU3Ed__11_t5456BFBA17F259728246179E74467D248D6907EE_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CPauseWithU3Ed__11__ctor_m7B8EC6973FC3CB3AD38FA624D09DC60D3BB15292(L_0, 0, NULL);
		U3CPauseWithU3Ed__11_t5456BFBA17F259728246179E74467D248D6907EE* L_1 = L_0;
		RuntimeObject* L_2 = ___coroutine0;
		NullCheck(L_1);
		L_1->___coroutine_3 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___coroutine_3), (void*)L_2);
		U3CPauseWithU3Ed__11_t5456BFBA17F259728246179E74467D248D6907EE* L_3 = L_1;
		Func_1_t2BE7F58348C9CC544A8973B3A9E55541DE43C457* L_4 = ___condition1;
		NullCheck(L_3);
		L_3->___condition_2 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&L_3->___condition_2), (void*)L_4);
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2::KillWith(System.Collections.Generic.IEnumerator`1<System.Single>,MEC.CoroutineHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MECExtensionMethods2_KillWith_m38373C3D6641C483F6E91EBE993E037C62B4D341 (RuntimeObject* ___coroutine0, CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___otherCoroutine1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CKillWithU3Ed__12_tC9D795656444FD23FDAD2843605F1E95509C8546_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CKillWithU3Ed__12_tC9D795656444FD23FDAD2843605F1E95509C8546* L_0 = (U3CKillWithU3Ed__12_tC9D795656444FD23FDAD2843605F1E95509C8546*)il2cpp_codegen_object_new(U3CKillWithU3Ed__12_tC9D795656444FD23FDAD2843605F1E95509C8546_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CKillWithU3Ed__12__ctor_mB27B4A9FE8CED4EA6EE6A0BED66CE4C2A7B78AC5(L_0, 0, NULL);
		U3CKillWithU3Ed__12_tC9D795656444FD23FDAD2843605F1E95509C8546* L_1 = L_0;
		RuntimeObject* L_2 = ___coroutine0;
		NullCheck(L_1);
		L_1->___coroutine_2 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___coroutine_2), (void*)L_2);
		U3CKillWithU3Ed__12_tC9D795656444FD23FDAD2843605F1E95509C8546* L_3 = L_1;
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_4 = ___otherCoroutine1;
		NullCheck(L_3);
		L_3->___otherCoroutine_3 = L_4;
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2::Append(System.Collections.Generic.IEnumerator`1<System.Single>,System.Collections.Generic.IEnumerator`1<System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MECExtensionMethods2_Append_mFAB856117A004E0A70C1931C3BC547688C58829F (RuntimeObject* ___coroutine0, RuntimeObject* ___nextCoroutine1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAppendU3Ed__13_tDA8C39AB4BF8F167235AE8A8F64E254A4BFF32E9_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CAppendU3Ed__13_tDA8C39AB4BF8F167235AE8A8F64E254A4BFF32E9* L_0 = (U3CAppendU3Ed__13_tDA8C39AB4BF8F167235AE8A8F64E254A4BFF32E9*)il2cpp_codegen_object_new(U3CAppendU3Ed__13_tDA8C39AB4BF8F167235AE8A8F64E254A4BFF32E9_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CAppendU3Ed__13__ctor_mE8C26EF341860BFCF2663634BC15276D43D402AD(L_0, 0, NULL);
		U3CAppendU3Ed__13_tDA8C39AB4BF8F167235AE8A8F64E254A4BFF32E9* L_1 = L_0;
		RuntimeObject* L_2 = ___coroutine0;
		NullCheck(L_1);
		L_1->___coroutine_2 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___coroutine_2), (void*)L_2);
		U3CAppendU3Ed__13_tDA8C39AB4BF8F167235AE8A8F64E254A4BFF32E9* L_3 = L_1;
		RuntimeObject* L_4 = ___nextCoroutine1;
		NullCheck(L_3);
		L_3->___nextCoroutine_3 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&L_3->___nextCoroutine_3), (void*)L_4);
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2::Append(System.Collections.Generic.IEnumerator`1<System.Single>,System.Action)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MECExtensionMethods2_Append_m2FAC85031AF0F57B3A40D6DDE9E11354FA0EFC8B (RuntimeObject* ___coroutine0, Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___onDone1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAppendU3Ed__14_t0E472D9D963516368FE446E17DB0A22607539C15_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CAppendU3Ed__14_t0E472D9D963516368FE446E17DB0A22607539C15* L_0 = (U3CAppendU3Ed__14_t0E472D9D963516368FE446E17DB0A22607539C15*)il2cpp_codegen_object_new(U3CAppendU3Ed__14_t0E472D9D963516368FE446E17DB0A22607539C15_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CAppendU3Ed__14__ctor_m8F179A0583E8FFB49E2D69516A59513115B9D42B(L_0, 0, NULL);
		U3CAppendU3Ed__14_t0E472D9D963516368FE446E17DB0A22607539C15* L_1 = L_0;
		RuntimeObject* L_2 = ___coroutine0;
		NullCheck(L_1);
		L_1->___coroutine_2 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___coroutine_2), (void*)L_2);
		U3CAppendU3Ed__14_t0E472D9D963516368FE446E17DB0A22607539C15* L_3 = L_1;
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_4 = ___onDone1;
		NullCheck(L_3);
		L_3->___onDone_3 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&L_3->___onDone_3), (void*)L_4);
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2::Prepend(System.Collections.Generic.IEnumerator`1<System.Single>,System.Collections.Generic.IEnumerator`1<System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MECExtensionMethods2_Prepend_m1CBE78291C604C18F44887C78FA49E7B2EACD586 (RuntimeObject* ___coroutine0, RuntimeObject* ___lastCoroutine1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrependU3Ed__15_t52DE12BFF91D5776D73148D2B29F7B452020EADB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CPrependU3Ed__15_t52DE12BFF91D5776D73148D2B29F7B452020EADB* L_0 = (U3CPrependU3Ed__15_t52DE12BFF91D5776D73148D2B29F7B452020EADB*)il2cpp_codegen_object_new(U3CPrependU3Ed__15_t52DE12BFF91D5776D73148D2B29F7B452020EADB_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CPrependU3Ed__15__ctor_mD88241B0D265A3F5FD24539D3EA596962A08F63F(L_0, 0, NULL);
		U3CPrependU3Ed__15_t52DE12BFF91D5776D73148D2B29F7B452020EADB* L_1 = L_0;
		RuntimeObject* L_2 = ___coroutine0;
		NullCheck(L_1);
		L_1->___coroutine_3 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___coroutine_3), (void*)L_2);
		U3CPrependU3Ed__15_t52DE12BFF91D5776D73148D2B29F7B452020EADB* L_3 = L_1;
		RuntimeObject* L_4 = ___lastCoroutine1;
		NullCheck(L_3);
		L_3->___lastCoroutine_2 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&L_3->___lastCoroutine_2), (void*)L_4);
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2::Prepend(System.Collections.Generic.IEnumerator`1<System.Single>,System.Action)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MECExtensionMethods2_Prepend_m380FBDF7FB899A937D4F42F0A07D83AF659A964D (RuntimeObject* ___coroutine0, Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___onStart1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrependU3Ed__16_t023603E04E5CBE20E04C0E48452C5CA1597C446E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CPrependU3Ed__16_t023603E04E5CBE20E04C0E48452C5CA1597C446E* L_0 = (U3CPrependU3Ed__16_t023603E04E5CBE20E04C0E48452C5CA1597C446E*)il2cpp_codegen_object_new(U3CPrependU3Ed__16_t023603E04E5CBE20E04C0E48452C5CA1597C446E_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CPrependU3Ed__16__ctor_m64A2A21EFC706D087AA24B51726031DD34D9B127(L_0, 0, NULL);
		U3CPrependU3Ed__16_t023603E04E5CBE20E04C0E48452C5CA1597C446E* L_1 = L_0;
		RuntimeObject* L_2 = ___coroutine0;
		NullCheck(L_1);
		L_1->___coroutine_3 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___coroutine_3), (void*)L_2);
		U3CPrependU3Ed__16_t023603E04E5CBE20E04C0E48452C5CA1597C446E* L_3 = L_1;
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_4 = ___onStart1;
		NullCheck(L_3);
		L_3->___onStart_2 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&L_3->___onStart_2), (void*)L_4);
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2::Superimpose(System.Collections.Generic.IEnumerator`1<System.Single>,System.Collections.Generic.IEnumerator`1<System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MECExtensionMethods2_Superimpose_m5697CB16C62F2C924BC4AC28EBCBD0B08F3AB9C2 (RuntimeObject* ___coroutineA0, RuntimeObject* ___coroutineB1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Superimpose(coroutineA, coroutineB, MEC.Timing.Instance);
		RuntimeObject* L_0 = ___coroutineA0;
		RuntimeObject* L_1 = ___coroutineB1;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_2;
		L_2 = Timing_get_Instance_mF42E49C49C3AFADB18C9AEA52FE27BB5A3362C11(NULL);
		RuntimeObject* L_3;
		L_3 = MECExtensionMethods2_Superimpose_mE7408CF7D1A39CED17C7F9D9307A008150E75B28(L_0, L_1, L_2, NULL);
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2::Superimpose(System.Collections.Generic.IEnumerator`1<System.Single>,System.Collections.Generic.IEnumerator`1<System.Single>,MEC.Timing)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MECExtensionMethods2_Superimpose_mE7408CF7D1A39CED17C7F9D9307A008150E75B28 (RuntimeObject* ___coroutineA0, RuntimeObject* ___coroutineB1, Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* ___instance2, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSuperimposeU3Ed__18_tE9CABB59D41E90F0F203FB1529715342294EF1B6_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CSuperimposeU3Ed__18_tE9CABB59D41E90F0F203FB1529715342294EF1B6* L_0 = (U3CSuperimposeU3Ed__18_tE9CABB59D41E90F0F203FB1529715342294EF1B6*)il2cpp_codegen_object_new(U3CSuperimposeU3Ed__18_tE9CABB59D41E90F0F203FB1529715342294EF1B6_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CSuperimposeU3Ed__18__ctor_m14A89B8EF2C764E26ADDDCCE0DA6A3AEC00AB3BA(L_0, 0, NULL);
		U3CSuperimposeU3Ed__18_tE9CABB59D41E90F0F203FB1529715342294EF1B6* L_1 = L_0;
		RuntimeObject* L_2 = ___coroutineA0;
		NullCheck(L_1);
		L_1->___coroutineA_2 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___coroutineA_2), (void*)L_2);
		U3CSuperimposeU3Ed__18_tE9CABB59D41E90F0F203FB1529715342294EF1B6* L_3 = L_1;
		RuntimeObject* L_4 = ___coroutineB1;
		NullCheck(L_3);
		L_3->___coroutineB_4 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&L_3->___coroutineB_4), (void*)L_4);
		U3CSuperimposeU3Ed__18_tE9CABB59D41E90F0F203FB1529715342294EF1B6* L_5 = L_3;
		Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_6 = ___instance2;
		NullCheck(L_5);
		L_5->___instance_3 = L_6;
		Il2CppCodeGenWriteBarrier((void**)(&L_5->___instance_3), (void*)L_6);
		return L_5;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2::Hijack(System.Collections.Generic.IEnumerator`1<System.Single>,System.Func`2<System.Single,System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MECExtensionMethods2_Hijack_m2E1150666DC2A86E45C60849BD42E9EEB38E5274 (RuntimeObject* ___coroutine0, Func_2_t2A7432CC4F64D0DF6D8629208B154CF139B39AF2* ___newReturn1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CHijackU3Ed__19_t57ED3438E560C43D619C0606A97C7FB3B547EBB1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CHijackU3Ed__19_t57ED3438E560C43D619C0606A97C7FB3B547EBB1* L_0 = (U3CHijackU3Ed__19_t57ED3438E560C43D619C0606A97C7FB3B547EBB1*)il2cpp_codegen_object_new(U3CHijackU3Ed__19_t57ED3438E560C43D619C0606A97C7FB3B547EBB1_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CHijackU3Ed__19__ctor_m00BFFC3B2E7C61F564D9A62A3508833FF491DEAB(L_0, 0, NULL);
		U3CHijackU3Ed__19_t57ED3438E560C43D619C0606A97C7FB3B547EBB1* L_1 = L_0;
		RuntimeObject* L_2 = ___coroutine0;
		NullCheck(L_1);
		L_1->___coroutine_3 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___coroutine_3), (void*)L_2);
		U3CHijackU3Ed__19_t57ED3438E560C43D619C0606A97C7FB3B547EBB1* L_3 = L_1;
		Func_2_t2A7432CC4F64D0DF6D8629208B154CF139B39AF2* L_4 = ___newReturn1;
		NullCheck(L_3);
		L_3->___newReturn_2 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&L_3->___newReturn_2), (void*)L_4);
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Single> MECExtensionMethods2::RerouteExceptions(System.Collections.Generic.IEnumerator`1<System.Single>,System.Action`1<System.Exception>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MECExtensionMethods2_RerouteExceptions_m70FB6BF7C5B0887E944FE90C5E4689FB9A82C7F8 (RuntimeObject* ___coroutine0, Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* ___exceptionHandler1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRerouteExceptionsU3Ed__20_t34352A3C593E8FF83A90E02879CEAD71D7417184_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CRerouteExceptionsU3Ed__20_t34352A3C593E8FF83A90E02879CEAD71D7417184* L_0 = (U3CRerouteExceptionsU3Ed__20_t34352A3C593E8FF83A90E02879CEAD71D7417184*)il2cpp_codegen_object_new(U3CRerouteExceptionsU3Ed__20_t34352A3C593E8FF83A90E02879CEAD71D7417184_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CRerouteExceptionsU3Ed__20__ctor_mA318DE4F8F2A5AB948440CB6B7F63C2F41E3670D(L_0, 0, NULL);
		U3CRerouteExceptionsU3Ed__20_t34352A3C593E8FF83A90E02879CEAD71D7417184* L_1 = L_0;
		RuntimeObject* L_2 = ___coroutine0;
		NullCheck(L_1);
		L_1->___coroutine_2 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___coroutine_2), (void*)L_2);
		U3CRerouteExceptionsU3Ed__20_t34352A3C593E8FF83A90E02879CEAD71D7417184* L_3 = L_1;
		Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_4 = ___exceptionHandler1;
		NullCheck(L_3);
		L_3->___exceptionHandler_3 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&L_3->___exceptionHandler_3), (void*)L_4);
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MECExtensionMethods2/<Delay>d__0::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDelayU3Ed__0__ctor_m10C85E378645F967756C6BEA612ADC16AFCEF598 (U3CDelayU3Ed__0_tB8C42C4BE4F8BDBD9A8EFC17DAF05BA5C06045BC* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void MECExtensionMethods2/<Delay>d__0::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDelayU3Ed__0_System_IDisposable_Dispose_m87AA263DEBB25C6D4814A894CCE9E849513DF255 (U3CDelayU3Ed__0_tB8C42C4BE4F8BDBD9A8EFC17DAF05BA5C06045BC* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean MECExtensionMethods2/<Delay>d__0::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CDelayU3Ed__0_MoveNext_m7CFDFC0CD5000967B5A0496D64897FC1819D3092 (U3CDelayU3Ed__0_tB8C42C4BE4F8BDBD9A8EFC17DAF05BA5C06045BC* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001b;
			}
			case 1:
			{
				goto IL_003c;
			}
			case 2:
			{
				goto IL_005f;
			}
		}
	}
	{
		return (bool)0;
	}

IL_001b:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// yield return MEC.Timing.WaitForSeconds(timeToDelay);
		float L_2 = __this->___timeToDelay_2;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		float L_3;
		L_3 = Timing_WaitForSeconds_m54FB367D2A4605DF51BDBBD01D02208B25B57E2D(L_2, NULL);
		__this->___U3CU3E2__current_1 = L_3;
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_003c:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_0066;
	}

IL_0045:
	{
		// yield return coroutine.Current;
		RuntimeObject* L_4 = __this->___coroutine_3;
		NullCheck(L_4);
		float L_5;
		L_5 = InterfaceFuncInvoker0< float >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var, L_4);
		__this->___U3CU3E2__current_1 = L_5;
		__this->___U3CU3E1__state_0 = 2;
		return (bool)1;
	}

IL_005f:
	{
		__this->___U3CU3E1__state_0 = (-1);
	}

IL_0066:
	{
		// while (coroutine.MoveNext())
		RuntimeObject* L_6 = __this->___coroutine_3;
		NullCheck(L_6);
		bool L_7;
		L_7 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_6);
		if (L_7)
		{
			goto IL_0045;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Single MECExtensionMethods2/<Delay>d__0::System.Collections.Generic.IEnumerator<System.Single>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float U3CDelayU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_mF1CD1DC969DF5A315033A8DDDF07FCE3CC5B76E8 (U3CDelayU3Ed__0_tB8C42C4BE4F8BDBD9A8EFC17DAF05BA5C06045BC* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void MECExtensionMethods2/<Delay>d__0::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDelayU3Ed__0_System_Collections_IEnumerator_Reset_m61429A4ED99BA4E915AF45EE14EF5E99CE52B618 (U3CDelayU3Ed__0_tB8C42C4BE4F8BDBD9A8EFC17DAF05BA5C06045BC* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CDelayU3Ed__0_System_Collections_IEnumerator_Reset_m61429A4ED99BA4E915AF45EE14EF5E99CE52B618_RuntimeMethod_var)));
	}
}
// System.Object MECExtensionMethods2/<Delay>d__0::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CDelayU3Ed__0_System_Collections_IEnumerator_get_Current_m9E25C36F90CEBBD391FF5F10465C2F1BF9E0EF7D (U3CDelayU3Ed__0_tB8C42C4BE4F8BDBD9A8EFC17DAF05BA5C06045BC* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->___U3CU3E2__current_1;
		float L_1 = L_0;
		RuntimeObject* L_2 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MECExtensionMethods2/<Delay>d__1::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDelayU3Ed__1__ctor_m2D974F25CF6320C7643BC594E73F5954E6CB7772 (U3CDelayU3Ed__1_tBD016C33189EAA123E44405BC6EDE65761067DFC* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void MECExtensionMethods2/<Delay>d__1::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDelayU3Ed__1_System_IDisposable_Dispose_m6FAA76788A9CCF37F9A3CD0895C09023BB773CB4 (U3CDelayU3Ed__1_tBD016C33189EAA123E44405BC6EDE65761067DFC* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean MECExtensionMethods2/<Delay>d__1::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CDelayU3Ed__1_MoveNext_m46A8F976EBA1DB6BC7A7880396B8A91015EAD22F (U3CDelayU3Ed__1_tBD016C33189EAA123E44405BC6EDE65761067DFC* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001b;
			}
			case 1:
			{
				goto IL_0038;
			}
			case 2:
			{
				goto IL_0068;
			}
		}
	}
	{
		return (bool)0;
	}

IL_001b:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_003f;
	}

IL_0024:
	{
		// yield return 0f;
		__this->___U3CU3E2__current_1 = (0.0f);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0038:
	{
		__this->___U3CU3E1__state_0 = (-1);
	}

IL_003f:
	{
		// while (!condition())
		Func_1_t2BE7F58348C9CC544A8973B3A9E55541DE43C457* L_2 = __this->___condition_2;
		NullCheck(L_2);
		bool L_3;
		L_3 = Func_1_Invoke_mBB7F37C468451AF57FAF31635C544D6B8C4373B2_inline(L_2, NULL);
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		goto IL_006f;
	}

IL_004e:
	{
		// yield return coroutine.Current;
		RuntimeObject* L_4 = __this->___coroutine_3;
		NullCheck(L_4);
		float L_5;
		L_5 = InterfaceFuncInvoker0< float >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var, L_4);
		__this->___U3CU3E2__current_1 = L_5;
		__this->___U3CU3E1__state_0 = 2;
		return (bool)1;
	}

IL_0068:
	{
		__this->___U3CU3E1__state_0 = (-1);
	}

IL_006f:
	{
		// while (coroutine.MoveNext())
		RuntimeObject* L_6 = __this->___coroutine_3;
		NullCheck(L_6);
		bool L_7;
		L_7 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_6);
		if (L_7)
		{
			goto IL_004e;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Single MECExtensionMethods2/<Delay>d__1::System.Collections.Generic.IEnumerator<System.Single>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float U3CDelayU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_m4AAEDA4E8B9F84CC69A8F375C5DFB8711867A0FD (U3CDelayU3Ed__1_tBD016C33189EAA123E44405BC6EDE65761067DFC* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void MECExtensionMethods2/<Delay>d__1::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDelayU3Ed__1_System_Collections_IEnumerator_Reset_m8DFD84218D4EF4A1AB8F0D5CB1F077BC24935E47 (U3CDelayU3Ed__1_tBD016C33189EAA123E44405BC6EDE65761067DFC* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CDelayU3Ed__1_System_Collections_IEnumerator_Reset_m8DFD84218D4EF4A1AB8F0D5CB1F077BC24935E47_RuntimeMethod_var)));
	}
}
// System.Object MECExtensionMethods2/<Delay>d__1::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CDelayU3Ed__1_System_Collections_IEnumerator_get_Current_m1E407D54B7CF4907686DB967314240BE32E679EC (U3CDelayU3Ed__1_tBD016C33189EAA123E44405BC6EDE65761067DFC* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->___U3CU3E2__current_1;
		float L_1 = L_0;
		RuntimeObject* L_2 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MECExtensionMethods2/<DelayFrames>d__3::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDelayFramesU3Ed__3__ctor_mE82DFCFD4E3073EA3695FC59341AA77F17F68E26 (U3CDelayFramesU3Ed__3_t59DE39C1E9B371904A1FA321BF5737F51BAA58E4* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void MECExtensionMethods2/<DelayFrames>d__3::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDelayFramesU3Ed__3_System_IDisposable_Dispose_m0DF6EF4A5BE7B5C7F31FC0D17B84D50DDCB6F413 (U3CDelayFramesU3Ed__3_t59DE39C1E9B371904A1FA321BF5737F51BAA58E4* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean MECExtensionMethods2/<DelayFrames>d__3::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CDelayFramesU3Ed__3_MoveNext_mCC644637B62C16F6478A7FE1B358687BF93D0FD0 (U3CDelayFramesU3Ed__3_t59DE39C1E9B371904A1FA321BF5737F51BAA58E4* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001b;
			}
			case 1:
			{
				goto IL_0038;
			}
			case 2:
			{
				goto IL_006f;
			}
		}
	}
	{
		return (bool)0;
	}

IL_001b:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_003f;
	}

IL_0024:
	{
		// yield return 0f;
		__this->___U3CU3E2__current_1 = (0.0f);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0038:
	{
		__this->___U3CU3E1__state_0 = (-1);
	}

IL_003f:
	{
		// while (framesToDelay-- > 0)
		int32_t L_2 = __this->___framesToDelay_2;
		V_1 = L_2;
		int32_t L_3 = V_1;
		__this->___framesToDelay_2 = ((int32_t)il2cpp_codegen_subtract(L_3, 1));
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) > ((int32_t)0)))
		{
			goto IL_0024;
		}
	}
	{
		goto IL_0076;
	}

IL_0055:
	{
		// yield return coroutine.Current;
		RuntimeObject* L_5 = __this->___coroutine_3;
		NullCheck(L_5);
		float L_6;
		L_6 = InterfaceFuncInvoker0< float >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var, L_5);
		__this->___U3CU3E2__current_1 = L_6;
		__this->___U3CU3E1__state_0 = 2;
		return (bool)1;
	}

IL_006f:
	{
		__this->___U3CU3E1__state_0 = (-1);
	}

IL_0076:
	{
		// while (coroutine.MoveNext())
		RuntimeObject* L_7 = __this->___coroutine_3;
		NullCheck(L_7);
		bool L_8;
		L_8 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_7);
		if (L_8)
		{
			goto IL_0055;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Single MECExtensionMethods2/<DelayFrames>d__3::System.Collections.Generic.IEnumerator<System.Single>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float U3CDelayFramesU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_m1A77FA7AB91EB59DF76BADAEC49CD454E993ED26 (U3CDelayFramesU3Ed__3_t59DE39C1E9B371904A1FA321BF5737F51BAA58E4* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void MECExtensionMethods2/<DelayFrames>d__3::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDelayFramesU3Ed__3_System_Collections_IEnumerator_Reset_mA243F12B3EAEC24744A1CAB6AE77DD69F264ED17 (U3CDelayFramesU3Ed__3_t59DE39C1E9B371904A1FA321BF5737F51BAA58E4* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CDelayFramesU3Ed__3_System_Collections_IEnumerator_Reset_mA243F12B3EAEC24744A1CAB6AE77DD69F264ED17_RuntimeMethod_var)));
	}
}
// System.Object MECExtensionMethods2/<DelayFrames>d__3::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CDelayFramesU3Ed__3_System_Collections_IEnumerator_get_Current_mEACBC315E02B8B6D9F49F670D6A1FE71B1816C53 (U3CDelayFramesU3Ed__3_t59DE39C1E9B371904A1FA321BF5737F51BAA58E4* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->___U3CU3E2__current_1;
		float L_1 = L_0;
		RuntimeObject* L_2 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MECExtensionMethods2/<CancelWith>d__4::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCancelWithU3Ed__4__ctor_mBB2782280575A40C8D97441358D6DBA9DE0AA1D2 (U3CCancelWithU3Ed__4_t8B8BAEC6F1E6A2547D9B597A4343E3863B73203C* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void MECExtensionMethods2/<CancelWith>d__4::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCancelWithU3Ed__4_System_IDisposable_Dispose_m800B3F3BFFA86D56E8A6F8E9174F3F80464C4453 (U3CCancelWithU3Ed__4_t8B8BAEC6F1E6A2547D9B597A4343E3863B73203C* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean MECExtensionMethods2/<CancelWith>d__4::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CCancelWithU3Ed__4_MoveNext_m153EC477C16464CA7D9C82C0AB1AE1439431DE7E (U3CCancelWithU3Ed__4_t8B8BAEC6F1E6A2547D9B597A4343E3863B73203C* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0033;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_003a;
	}

IL_0019:
	{
		// yield return coroutine.Current;
		RuntimeObject* L_3 = __this->___coroutine_2;
		NullCheck(L_3);
		float L_4;
		L_4 = InterfaceFuncInvoker0< float >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var, L_3);
		__this->___U3CU3E2__current_1 = L_4;
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0033:
	{
		__this->___U3CU3E1__state_0 = (-1);
	}

IL_003a:
	{
		// while (MEC.Timing.MainThread != System.Threading.Thread.CurrentThread ||
		//         (gameObject && gameObject.activeInHierarchy && coroutine.MoveNext()))
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		Thread_t0A773B9DE873D2DCAA7D229EAB36757B500E207F* L_5;
		L_5 = Timing_get_MainThread_mFEE85EB21F421371C788BFF8E2D61D0FF21FFEAF_inline(NULL);
		Thread_t0A773B9DE873D2DCAA7D229EAB36757B500E207F* L_6;
		L_6 = Thread_get_CurrentThread_m835AD1DF1C0D10BABE1A5427CC4B357C991B25AB(NULL);
		if ((!(((RuntimeObject*)(Thread_t0A773B9DE873D2DCAA7D229EAB36757B500E207F*)L_5) == ((RuntimeObject*)(Thread_t0A773B9DE873D2DCAA7D229EAB36757B500E207F*)L_6))))
		{
			goto IL_0019;
		}
	}
	{
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7 = __this->___gameObject_3;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_8;
		L_8 = Object_op_Implicit_m18E1885C296CC868AC918101523697CFE6413C79(L_7, NULL);
		if (!L_8)
		{
			goto IL_006d;
		}
	}
	{
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_9 = __this->___gameObject_3;
		NullCheck(L_9);
		bool L_10;
		L_10 = GameObject_get_activeInHierarchy_m49250F4F168DCC5388D5BE4F6A5681386907B109(L_9, NULL);
		if (!L_10)
		{
			goto IL_006d;
		}
	}
	{
		RuntimeObject* L_11 = __this->___coroutine_2;
		NullCheck(L_11);
		bool L_12;
		L_12 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_11);
		if (L_12)
		{
			goto IL_0019;
		}
	}

IL_006d:
	{
		// }
		return (bool)0;
	}
}
// System.Single MECExtensionMethods2/<CancelWith>d__4::System.Collections.Generic.IEnumerator<System.Single>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float U3CCancelWithU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_mA04E82712A68C74DA393141BBA1D61930DB21F1A (U3CCancelWithU3Ed__4_t8B8BAEC6F1E6A2547D9B597A4343E3863B73203C* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void MECExtensionMethods2/<CancelWith>d__4::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCancelWithU3Ed__4_System_Collections_IEnumerator_Reset_m655AEE33429EC98E86F2707FBF28446A62FCAFC6 (U3CCancelWithU3Ed__4_t8B8BAEC6F1E6A2547D9B597A4343E3863B73203C* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CCancelWithU3Ed__4_System_Collections_IEnumerator_Reset_m655AEE33429EC98E86F2707FBF28446A62FCAFC6_RuntimeMethod_var)));
	}
}
// System.Object MECExtensionMethods2/<CancelWith>d__4::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CCancelWithU3Ed__4_System_Collections_IEnumerator_get_Current_m53FB089958967FD8B98C92FABC5F8F87ACA49645 (U3CCancelWithU3Ed__4_t8B8BAEC6F1E6A2547D9B597A4343E3863B73203C* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->___U3CU3E2__current_1;
		float L_1 = L_0;
		RuntimeObject* L_2 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MECExtensionMethods2/<CancelWith>d__5::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCancelWithU3Ed__5__ctor_mE5CB9E3E5503412BCE13273FF4CEAF71146BF818 (U3CCancelWithU3Ed__5_t77CB416A3F92FD3BD432622F2D4FA46C9D1155F5* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void MECExtensionMethods2/<CancelWith>d__5::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCancelWithU3Ed__5_System_IDisposable_Dispose_mF52AFD52566F5DF08A86507EC62AA761FBAF65E3 (U3CCancelWithU3Ed__5_t77CB416A3F92FD3BD432622F2D4FA46C9D1155F5* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean MECExtensionMethods2/<CancelWith>d__5::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CCancelWithU3Ed__5_MoveNext_mB6F936DE7797F2046A971441CF4867E0A7ECD54F (U3CCancelWithU3Ed__5_t77CB416A3F92FD3BD432622F2D4FA46C9D1155F5* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0033;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_003a;
	}

IL_0019:
	{
		// yield return coroutine.Current;
		RuntimeObject* L_3 = __this->___coroutine_2;
		NullCheck(L_3);
		float L_4;
		L_4 = InterfaceFuncInvoker0< float >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var, L_3);
		__this->___U3CU3E2__current_1 = L_4;
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0033:
	{
		__this->___U3CU3E1__state_0 = (-1);
	}

IL_003a:
	{
		// while (MEC.Timing.MainThread != System.Threading.Thread.CurrentThread || (gameObject1 && gameObject1.activeInHierarchy &&
		//         gameObject2 && gameObject2.activeInHierarchy && coroutine.MoveNext()))
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		Thread_t0A773B9DE873D2DCAA7D229EAB36757B500E207F* L_5;
		L_5 = Timing_get_MainThread_mFEE85EB21F421371C788BFF8E2D61D0FF21FFEAF_inline(NULL);
		Thread_t0A773B9DE873D2DCAA7D229EAB36757B500E207F* L_6;
		L_6 = Thread_get_CurrentThread_m835AD1DF1C0D10BABE1A5427CC4B357C991B25AB(NULL);
		if ((!(((RuntimeObject*)(Thread_t0A773B9DE873D2DCAA7D229EAB36757B500E207F*)L_5) == ((RuntimeObject*)(Thread_t0A773B9DE873D2DCAA7D229EAB36757B500E207F*)L_6))))
		{
			goto IL_0019;
		}
	}
	{
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7 = __this->___gameObject1_3;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_8;
		L_8 = Object_op_Implicit_m18E1885C296CC868AC918101523697CFE6413C79(L_7, NULL);
		if (!L_8)
		{
			goto IL_0087;
		}
	}
	{
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_9 = __this->___gameObject1_3;
		NullCheck(L_9);
		bool L_10;
		L_10 = GameObject_get_activeInHierarchy_m49250F4F168DCC5388D5BE4F6A5681386907B109(L_9, NULL);
		if (!L_10)
		{
			goto IL_0087;
		}
	}
	{
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_11 = __this->___gameObject2_4;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_12;
		L_12 = Object_op_Implicit_m18E1885C296CC868AC918101523697CFE6413C79(L_11, NULL);
		if (!L_12)
		{
			goto IL_0087;
		}
	}
	{
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_13 = __this->___gameObject2_4;
		NullCheck(L_13);
		bool L_14;
		L_14 = GameObject_get_activeInHierarchy_m49250F4F168DCC5388D5BE4F6A5681386907B109(L_13, NULL);
		if (!L_14)
		{
			goto IL_0087;
		}
	}
	{
		RuntimeObject* L_15 = __this->___coroutine_2;
		NullCheck(L_15);
		bool L_16;
		L_16 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_15);
		if (L_16)
		{
			goto IL_0019;
		}
	}

IL_0087:
	{
		// }
		return (bool)0;
	}
}
// System.Single MECExtensionMethods2/<CancelWith>d__5::System.Collections.Generic.IEnumerator<System.Single>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float U3CCancelWithU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_mB60BA5C7E2D8EE993FA84B299E92135E8AE2730A (U3CCancelWithU3Ed__5_t77CB416A3F92FD3BD432622F2D4FA46C9D1155F5* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void MECExtensionMethods2/<CancelWith>d__5::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCancelWithU3Ed__5_System_Collections_IEnumerator_Reset_m99E7F97F6C0BD792FD3F300A8A2D3A13A0F6DC4A (U3CCancelWithU3Ed__5_t77CB416A3F92FD3BD432622F2D4FA46C9D1155F5* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CCancelWithU3Ed__5_System_Collections_IEnumerator_Reset_m99E7F97F6C0BD792FD3F300A8A2D3A13A0F6DC4A_RuntimeMethod_var)));
	}
}
// System.Object MECExtensionMethods2/<CancelWith>d__5::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CCancelWithU3Ed__5_System_Collections_IEnumerator_get_Current_m0FE1B0762222FD4BCEB787F6B4AE2872E5015A71 (U3CCancelWithU3Ed__5_t77CB416A3F92FD3BD432622F2D4FA46C9D1155F5* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->___U3CU3E2__current_1;
		float L_1 = L_0;
		RuntimeObject* L_2 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MECExtensionMethods2/<CancelWith>d__7::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCancelWithU3Ed__7__ctor_m36061336ECC2EE68DAF34C20E44F945038A636FC (U3CCancelWithU3Ed__7_t40CED40B36C8708B3F6BDFA2C421F36EE95CDAF7* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void MECExtensionMethods2/<CancelWith>d__7::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCancelWithU3Ed__7_System_IDisposable_Dispose_m55E6E0A28F5CCDE287DBB7C1C87FF7D3961E21BE (U3CCancelWithU3Ed__7_t40CED40B36C8708B3F6BDFA2C421F36EE95CDAF7* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean MECExtensionMethods2/<CancelWith>d__7::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CCancelWithU3Ed__7_MoveNext_mD39899AAD07115CE3A539BE348DA6D869C9D91B5 (U3CCancelWithU3Ed__7_t40CED40B36C8708B3F6BDFA2C421F36EE95CDAF7* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_003b;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// if (condition == null) yield break;
		Func_1_t2BE7F58348C9CC544A8973B3A9E55541DE43C457* L_3 = __this->___condition_2;
		if (L_3)
		{
			goto IL_0042;
		}
	}
	{
		// if (condition == null) yield break;
		return (bool)0;
	}

IL_0021:
	{
		// yield return coroutine.Current;
		RuntimeObject* L_4 = __this->___coroutine_3;
		NullCheck(L_4);
		float L_5;
		L_5 = InterfaceFuncInvoker0< float >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var, L_4);
		__this->___U3CU3E2__current_1 = L_5;
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_003b:
	{
		__this->___U3CU3E1__state_0 = (-1);
	}

IL_0042:
	{
		// while (MEC.Timing.MainThread != System.Threading.Thread.CurrentThread || (condition() && coroutine.MoveNext()))
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		Thread_t0A773B9DE873D2DCAA7D229EAB36757B500E207F* L_6;
		L_6 = Timing_get_MainThread_mFEE85EB21F421371C788BFF8E2D61D0FF21FFEAF_inline(NULL);
		Thread_t0A773B9DE873D2DCAA7D229EAB36757B500E207F* L_7;
		L_7 = Thread_get_CurrentThread_m835AD1DF1C0D10BABE1A5427CC4B357C991B25AB(NULL);
		if ((!(((RuntimeObject*)(Thread_t0A773B9DE873D2DCAA7D229EAB36757B500E207F*)L_6) == ((RuntimeObject*)(Thread_t0A773B9DE873D2DCAA7D229EAB36757B500E207F*)L_7))))
		{
			goto IL_0021;
		}
	}
	{
		Func_1_t2BE7F58348C9CC544A8973B3A9E55541DE43C457* L_8 = __this->___condition_2;
		NullCheck(L_8);
		bool L_9;
		L_9 = Func_1_Invoke_mBB7F37C468451AF57FAF31635C544D6B8C4373B2_inline(L_8, NULL);
		if (!L_9)
		{
			goto IL_0068;
		}
	}
	{
		RuntimeObject* L_10 = __this->___coroutine_3;
		NullCheck(L_10);
		bool L_11;
		L_11 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_10);
		if (L_11)
		{
			goto IL_0021;
		}
	}

IL_0068:
	{
		// }
		return (bool)0;
	}
}
// System.Single MECExtensionMethods2/<CancelWith>d__7::System.Collections.Generic.IEnumerator<System.Single>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float U3CCancelWithU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_mC1112176588FF9D0AB69AC30DE418711D9F87DE3 (U3CCancelWithU3Ed__7_t40CED40B36C8708B3F6BDFA2C421F36EE95CDAF7* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void MECExtensionMethods2/<CancelWith>d__7::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCancelWithU3Ed__7_System_Collections_IEnumerator_Reset_mBF3A27178CCE6D84607BE54AF608AEADA8776863 (U3CCancelWithU3Ed__7_t40CED40B36C8708B3F6BDFA2C421F36EE95CDAF7* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CCancelWithU3Ed__7_System_Collections_IEnumerator_Reset_mBF3A27178CCE6D84607BE54AF608AEADA8776863_RuntimeMethod_var)));
	}
}
// System.Object MECExtensionMethods2/<CancelWith>d__7::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CCancelWithU3Ed__7_System_Collections_IEnumerator_get_Current_m024445C9BAEC654A8D589D3A2CB45DB8F3921C86 (U3CCancelWithU3Ed__7_t40CED40B36C8708B3F6BDFA2C421F36EE95CDAF7* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->___U3CU3E2__current_1;
		float L_1 = L_0;
		RuntimeObject* L_2 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MECExtensionMethods2/<PauseWith>d__8::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CPauseWithU3Ed__8__ctor_m21665018B1B93A435AC8685065568235841FCD01 (U3CPauseWithU3Ed__8_tF0B847125D7EE5CE9C57E7DC7FF416D0E5C794C0* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void MECExtensionMethods2/<PauseWith>d__8::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CPauseWithU3Ed__8_System_IDisposable_Dispose_m4DD937C76A79D0B889F7B348F0C136A853EDE41B (U3CPauseWithU3Ed__8_tF0B847125D7EE5CE9C57E7DC7FF416D0E5C794C0* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean MECExtensionMethods2/<PauseWith>d__8::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CPauseWithU3Ed__8_MoveNext_m2838C8703CE44AC436098A5DE168CDC33A0AD139 (U3CPauseWithU3Ed__8_tF0B847125D7EE5CE9C57E7DC7FF416D0E5C794C0* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001b;
			}
			case 1:
			{
				goto IL_0058;
			}
			case 2:
			{
				goto IL_0077;
			}
		}
	}
	{
		return (bool)0;
	}

IL_001b:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_007e;
	}

IL_0024:
	{
		// if (gameObject.activeInHierarchy)
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___gameObject_2;
		NullCheck(L_2);
		bool L_3;
		L_3 = GameObject_get_activeInHierarchy_m49250F4F168DCC5388D5BE4F6A5681386907B109(L_2, NULL);
		if (!L_3)
		{
			goto IL_0063;
		}
	}
	{
		// if (coroutine.MoveNext())
		RuntimeObject* L_4 = __this->___coroutine_3;
		NullCheck(L_4);
		bool L_5;
		L_5 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_4);
		if (!L_5)
		{
			goto IL_0061;
		}
	}
	{
		// yield return coroutine.Current;
		RuntimeObject* L_6 = __this->___coroutine_3;
		NullCheck(L_6);
		float L_7;
		L_7 = InterfaceFuncInvoker0< float >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var, L_6);
		__this->___U3CU3E2__current_1 = L_7;
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0058:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_007e;
	}

IL_0061:
	{
		// yield break;
		return (bool)0;
	}

IL_0063:
	{
		// yield return MEC.Timing.WaitForOneFrame;
		__this->___U3CU3E2__current_1 = (-std::numeric_limits<float>::infinity());
		__this->___U3CU3E1__state_0 = 2;
		return (bool)1;
	}

IL_0077:
	{
		__this->___U3CU3E1__state_0 = (-1);
	}

IL_007e:
	{
		// while (MEC.Timing.MainThread == System.Threading.Thread.CurrentThread && gameObject)
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		Thread_t0A773B9DE873D2DCAA7D229EAB36757B500E207F* L_8;
		L_8 = Timing_get_MainThread_mFEE85EB21F421371C788BFF8E2D61D0FF21FFEAF_inline(NULL);
		Thread_t0A773B9DE873D2DCAA7D229EAB36757B500E207F* L_9;
		L_9 = Thread_get_CurrentThread_m835AD1DF1C0D10BABE1A5427CC4B357C991B25AB(NULL);
		if ((!(((RuntimeObject*)(Thread_t0A773B9DE873D2DCAA7D229EAB36757B500E207F*)L_8) == ((RuntimeObject*)(Thread_t0A773B9DE873D2DCAA7D229EAB36757B500E207F*)L_9))))
		{
			goto IL_0097;
		}
	}
	{
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_10 = __this->___gameObject_2;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_11;
		L_11 = Object_op_Implicit_m18E1885C296CC868AC918101523697CFE6413C79(L_10, NULL);
		if (L_11)
		{
			goto IL_0024;
		}
	}

IL_0097:
	{
		// }
		return (bool)0;
	}
}
// System.Single MECExtensionMethods2/<PauseWith>d__8::System.Collections.Generic.IEnumerator<System.Single>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float U3CPauseWithU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_m96A061BC2EC190330D3FBB5C141A3F947BBDCEAC (U3CPauseWithU3Ed__8_tF0B847125D7EE5CE9C57E7DC7FF416D0E5C794C0* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void MECExtensionMethods2/<PauseWith>d__8::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CPauseWithU3Ed__8_System_Collections_IEnumerator_Reset_m18E92390DD8C84073811F72E56BF5A338395F69A (U3CPauseWithU3Ed__8_tF0B847125D7EE5CE9C57E7DC7FF416D0E5C794C0* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CPauseWithU3Ed__8_System_Collections_IEnumerator_Reset_m18E92390DD8C84073811F72E56BF5A338395F69A_RuntimeMethod_var)));
	}
}
// System.Object MECExtensionMethods2/<PauseWith>d__8::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CPauseWithU3Ed__8_System_Collections_IEnumerator_get_Current_m63F519D2BA8A0E2C9B69B3D115628DA22D07B3DC (U3CPauseWithU3Ed__8_tF0B847125D7EE5CE9C57E7DC7FF416D0E5C794C0* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->___U3CU3E2__current_1;
		float L_1 = L_0;
		RuntimeObject* L_2 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MECExtensionMethods2/<PauseWith>d__9::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CPauseWithU3Ed__9__ctor_m174F914F1085EA2EBE33BF8FB35FE64AB510D5E5 (U3CPauseWithU3Ed__9_t5B5FAC5525B8593A92FA9BF46781B2DA2B8DC5ED* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void MECExtensionMethods2/<PauseWith>d__9::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CPauseWithU3Ed__9_System_IDisposable_Dispose_mEAA3F8581842967A97C2260F1B7294FF7EE0D584 (U3CPauseWithU3Ed__9_t5B5FAC5525B8593A92FA9BF46781B2DA2B8DC5ED* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean MECExtensionMethods2/<PauseWith>d__9::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CPauseWithU3Ed__9_MoveNext_mC901F94FD3CE0399E6E4CAD8DFA2B3DC970C4E7F (U3CPauseWithU3Ed__9_t5B5FAC5525B8593A92FA9BF46781B2DA2B8DC5ED* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001b;
			}
			case 1:
			{
				goto IL_0065;
			}
			case 2:
			{
				goto IL_0084;
			}
		}
	}
	{
		return (bool)0;
	}

IL_001b:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_008b;
	}

IL_0024:
	{
		// if (gameObject1.activeInHierarchy && gameObject2.activeInHierarchy)
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___gameObject1_2;
		NullCheck(L_2);
		bool L_3;
		L_3 = GameObject_get_activeInHierarchy_m49250F4F168DCC5388D5BE4F6A5681386907B109(L_2, NULL);
		if (!L_3)
		{
			goto IL_0070;
		}
	}
	{
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___gameObject2_3;
		NullCheck(L_4);
		bool L_5;
		L_5 = GameObject_get_activeInHierarchy_m49250F4F168DCC5388D5BE4F6A5681386907B109(L_4, NULL);
		if (!L_5)
		{
			goto IL_0070;
		}
	}
	{
		// if (coroutine.MoveNext())
		RuntimeObject* L_6 = __this->___coroutine_4;
		NullCheck(L_6);
		bool L_7;
		L_7 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_6);
		if (!L_7)
		{
			goto IL_006e;
		}
	}
	{
		// yield return coroutine.Current;
		RuntimeObject* L_8 = __this->___coroutine_4;
		NullCheck(L_8);
		float L_9;
		L_9 = InterfaceFuncInvoker0< float >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var, L_8);
		__this->___U3CU3E2__current_1 = L_9;
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0065:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_008b;
	}

IL_006e:
	{
		// yield break;
		return (bool)0;
	}

IL_0070:
	{
		// yield return MEC.Timing.WaitForOneFrame;
		__this->___U3CU3E2__current_1 = (-std::numeric_limits<float>::infinity());
		__this->___U3CU3E1__state_0 = 2;
		return (bool)1;
	}

IL_0084:
	{
		__this->___U3CU3E1__state_0 = (-1);
	}

IL_008b:
	{
		// while (MEC.Timing.MainThread == System.Threading.Thread.CurrentThread && gameObject1 && gameObject2)
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		Thread_t0A773B9DE873D2DCAA7D229EAB36757B500E207F* L_10;
		L_10 = Timing_get_MainThread_mFEE85EB21F421371C788BFF8E2D61D0FF21FFEAF_inline(NULL);
		Thread_t0A773B9DE873D2DCAA7D229EAB36757B500E207F* L_11;
		L_11 = Thread_get_CurrentThread_m835AD1DF1C0D10BABE1A5427CC4B357C991B25AB(NULL);
		if ((!(((RuntimeObject*)(Thread_t0A773B9DE873D2DCAA7D229EAB36757B500E207F*)L_10) == ((RuntimeObject*)(Thread_t0A773B9DE873D2DCAA7D229EAB36757B500E207F*)L_11))))
		{
			goto IL_00b4;
		}
	}
	{
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_12 = __this->___gameObject1_2;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_13;
		L_13 = Object_op_Implicit_m18E1885C296CC868AC918101523697CFE6413C79(L_12, NULL);
		if (!L_13)
		{
			goto IL_00b4;
		}
	}
	{
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_14 = __this->___gameObject2_3;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_15;
		L_15 = Object_op_Implicit_m18E1885C296CC868AC918101523697CFE6413C79(L_14, NULL);
		if (L_15)
		{
			goto IL_0024;
		}
	}

IL_00b4:
	{
		// }
		return (bool)0;
	}
}
// System.Single MECExtensionMethods2/<PauseWith>d__9::System.Collections.Generic.IEnumerator<System.Single>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float U3CPauseWithU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_m94B291CFBE79E2C5B949B7A1D3D21C07A7AE115A (U3CPauseWithU3Ed__9_t5B5FAC5525B8593A92FA9BF46781B2DA2B8DC5ED* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void MECExtensionMethods2/<PauseWith>d__9::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CPauseWithU3Ed__9_System_Collections_IEnumerator_Reset_mB369AC85BC8DF2634270D64EB43CFAC81243011A (U3CPauseWithU3Ed__9_t5B5FAC5525B8593A92FA9BF46781B2DA2B8DC5ED* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CPauseWithU3Ed__9_System_Collections_IEnumerator_Reset_mB369AC85BC8DF2634270D64EB43CFAC81243011A_RuntimeMethod_var)));
	}
}
// System.Object MECExtensionMethods2/<PauseWith>d__9::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CPauseWithU3Ed__9_System_Collections_IEnumerator_get_Current_mEE2489B7598F29D68657863A9E9D6F412275DB31 (U3CPauseWithU3Ed__9_t5B5FAC5525B8593A92FA9BF46781B2DA2B8DC5ED* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->___U3CU3E2__current_1;
		float L_1 = L_0;
		RuntimeObject* L_2 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MECExtensionMethods2/<PauseWith>d__11::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CPauseWithU3Ed__11__ctor_m7B8EC6973FC3CB3AD38FA624D09DC60D3BB15292 (U3CPauseWithU3Ed__11_t5456BFBA17F259728246179E74467D248D6907EE* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void MECExtensionMethods2/<PauseWith>d__11::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CPauseWithU3Ed__11_System_IDisposable_Dispose_m84A2469F43DBB69B78A0B52F42473E3265DBC67D (U3CPauseWithU3Ed__11_t5456BFBA17F259728246179E74467D248D6907EE* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean MECExtensionMethods2/<PauseWith>d__11::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CPauseWithU3Ed__11_MoveNext_mB7575EBCB1113DBF958A60D9C7A8AD45DE8C963F (U3CPauseWithU3Ed__11_t5456BFBA17F259728246179E74467D248D6907EE* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_003b;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// if (condition == null) yield break;
		Func_1_t2BE7F58348C9CC544A8973B3A9E55541DE43C457* L_3 = __this->___condition_2;
		if (L_3)
		{
			goto IL_0042;
		}
	}
	{
		// if (condition == null) yield break;
		return (bool)0;
	}

IL_0021:
	{
		// yield return coroutine.Current;
		RuntimeObject* L_4 = __this->___coroutine_3;
		NullCheck(L_4);
		float L_5;
		L_5 = InterfaceFuncInvoker0< float >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var, L_4);
		__this->___U3CU3E2__current_1 = L_5;
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_003b:
	{
		__this->___U3CU3E1__state_0 = (-1);
	}

IL_0042:
	{
		// while (MEC.Timing.MainThread != System.Threading.Thread.CurrentThread || (condition() && coroutine.MoveNext()))
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		Thread_t0A773B9DE873D2DCAA7D229EAB36757B500E207F* L_6;
		L_6 = Timing_get_MainThread_mFEE85EB21F421371C788BFF8E2D61D0FF21FFEAF_inline(NULL);
		Thread_t0A773B9DE873D2DCAA7D229EAB36757B500E207F* L_7;
		L_7 = Thread_get_CurrentThread_m835AD1DF1C0D10BABE1A5427CC4B357C991B25AB(NULL);
		if ((!(((RuntimeObject*)(Thread_t0A773B9DE873D2DCAA7D229EAB36757B500E207F*)L_6) == ((RuntimeObject*)(Thread_t0A773B9DE873D2DCAA7D229EAB36757B500E207F*)L_7))))
		{
			goto IL_0021;
		}
	}
	{
		Func_1_t2BE7F58348C9CC544A8973B3A9E55541DE43C457* L_8 = __this->___condition_2;
		NullCheck(L_8);
		bool L_9;
		L_9 = Func_1_Invoke_mBB7F37C468451AF57FAF31635C544D6B8C4373B2_inline(L_8, NULL);
		if (!L_9)
		{
			goto IL_0068;
		}
	}
	{
		RuntimeObject* L_10 = __this->___coroutine_3;
		NullCheck(L_10);
		bool L_11;
		L_11 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_10);
		if (L_11)
		{
			goto IL_0021;
		}
	}

IL_0068:
	{
		// }
		return (bool)0;
	}
}
// System.Single MECExtensionMethods2/<PauseWith>d__11::System.Collections.Generic.IEnumerator<System.Single>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float U3CPauseWithU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_m4ABAC467E28A074CE08373D19FCAC546948D7D86 (U3CPauseWithU3Ed__11_t5456BFBA17F259728246179E74467D248D6907EE* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void MECExtensionMethods2/<PauseWith>d__11::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CPauseWithU3Ed__11_System_Collections_IEnumerator_Reset_m184D4FE68946399934736D8E8A275A2CE252DC5C (U3CPauseWithU3Ed__11_t5456BFBA17F259728246179E74467D248D6907EE* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CPauseWithU3Ed__11_System_Collections_IEnumerator_Reset_m184D4FE68946399934736D8E8A275A2CE252DC5C_RuntimeMethod_var)));
	}
}
// System.Object MECExtensionMethods2/<PauseWith>d__11::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CPauseWithU3Ed__11_System_Collections_IEnumerator_get_Current_mE17AD51FA3D96DA3A21447254AF62A7318725F9C (U3CPauseWithU3Ed__11_t5456BFBA17F259728246179E74467D248D6907EE* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->___U3CU3E2__current_1;
		float L_1 = L_0;
		RuntimeObject* L_2 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MECExtensionMethods2/<KillWith>d__12::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CKillWithU3Ed__12__ctor_mB27B4A9FE8CED4EA6EE6A0BED66CE4C2A7B78AC5 (U3CKillWithU3Ed__12_tC9D795656444FD23FDAD2843605F1E95509C8546* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void MECExtensionMethods2/<KillWith>d__12::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CKillWithU3Ed__12_System_IDisposable_Dispose_mC3182F7C8DC90932347752BBA24FF00AB5A093A2 (U3CKillWithU3Ed__12_tC9D795656444FD23FDAD2843605F1E95509C8546* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean MECExtensionMethods2/<KillWith>d__12::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CKillWithU3Ed__12_MoveNext_m449B8EF6525A3DF5F0DF3DBBACBEA2EB55BBAC22 (U3CKillWithU3Ed__12_tC9D795656444FD23FDAD2843605F1E95509C8546* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0033;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_003a;
	}

IL_0019:
	{
		// yield return coroutine.Current;
		RuntimeObject* L_3 = __this->___coroutine_2;
		NullCheck(L_3);
		float L_4;
		L_4 = InterfaceFuncInvoker0< float >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var, L_3);
		__this->___U3CU3E2__current_1 = L_4;
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0033:
	{
		__this->___U3CU3E1__state_0 = (-1);
	}

IL_003a:
	{
		// while (otherCoroutine.IsRunning && coroutine.MoveNext())
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* L_5 = (&__this->___otherCoroutine_3);
		bool L_6;
		L_6 = CoroutineHandle_get_IsRunning_m8E91075AE685F04BC4569D0E455CD4B10AE9B5AF(L_5, NULL);
		if (!L_6)
		{
			goto IL_0054;
		}
	}
	{
		RuntimeObject* L_7 = __this->___coroutine_2;
		NullCheck(L_7);
		bool L_8;
		L_8 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_7);
		if (L_8)
		{
			goto IL_0019;
		}
	}

IL_0054:
	{
		// }
		return (bool)0;
	}
}
// System.Single MECExtensionMethods2/<KillWith>d__12::System.Collections.Generic.IEnumerator<System.Single>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float U3CKillWithU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_m5DAFB23C05ACB01BD8BDF048B1D4E8DE86272CCF (U3CKillWithU3Ed__12_tC9D795656444FD23FDAD2843605F1E95509C8546* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void MECExtensionMethods2/<KillWith>d__12::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CKillWithU3Ed__12_System_Collections_IEnumerator_Reset_mE25758EF248A8B30D3D39785EAC1F99D5B34ECA6 (U3CKillWithU3Ed__12_tC9D795656444FD23FDAD2843605F1E95509C8546* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CKillWithU3Ed__12_System_Collections_IEnumerator_Reset_mE25758EF248A8B30D3D39785EAC1F99D5B34ECA6_RuntimeMethod_var)));
	}
}
// System.Object MECExtensionMethods2/<KillWith>d__12::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CKillWithU3Ed__12_System_Collections_IEnumerator_get_Current_m32D65AC56600D710E3FB4DC6354814F7E3DDAC5A (U3CKillWithU3Ed__12_tC9D795656444FD23FDAD2843605F1E95509C8546* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->___U3CU3E2__current_1;
		float L_1 = L_0;
		RuntimeObject* L_2 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MECExtensionMethods2/<Append>d__13::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CAppendU3Ed__13__ctor_mE8C26EF341860BFCF2663634BC15276D43D402AD (U3CAppendU3Ed__13_tDA8C39AB4BF8F167235AE8A8F64E254A4BFF32E9* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void MECExtensionMethods2/<Append>d__13::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CAppendU3Ed__13_System_IDisposable_Dispose_m604AAECF028CDFC16C5DA067A319F11E1F556174 (U3CAppendU3Ed__13_tDA8C39AB4BF8F167235AE8A8F64E254A4BFF32E9* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean MECExtensionMethods2/<Append>d__13::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CAppendU3Ed__13_MoveNext_mA5558656D00635E29C0E75BADD7537DE280BEB59 (U3CAppendU3Ed__13_tDA8C39AB4BF8F167235AE8A8F64E254A4BFF32E9* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001b;
			}
			case 1:
			{
				goto IL_003e;
			}
			case 2:
			{
				goto IL_0076;
			}
		}
	}
	{
		return (bool)0;
	}

IL_001b:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_0045;
	}

IL_0024:
	{
		// yield return coroutine.Current;
		RuntimeObject* L_2 = __this->___coroutine_2;
		NullCheck(L_2);
		float L_3;
		L_3 = InterfaceFuncInvoker0< float >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var, L_2);
		__this->___U3CU3E2__current_1 = L_3;
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_003e:
	{
		__this->___U3CU3E1__state_0 = (-1);
	}

IL_0045:
	{
		// while (coroutine.MoveNext())
		RuntimeObject* L_4 = __this->___coroutine_2;
		NullCheck(L_4);
		bool L_5;
		L_5 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_4);
		if (L_5)
		{
			goto IL_0024;
		}
	}
	{
		// if (nextCoroutine == null) yield break;
		RuntimeObject* L_6 = __this->___nextCoroutine_3;
		if (L_6)
		{
			goto IL_007d;
		}
	}
	{
		// if (nextCoroutine == null) yield break;
		return (bool)0;
	}

IL_005c:
	{
		// yield return nextCoroutine.Current;
		RuntimeObject* L_7 = __this->___nextCoroutine_3;
		NullCheck(L_7);
		float L_8;
		L_8 = InterfaceFuncInvoker0< float >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var, L_7);
		__this->___U3CU3E2__current_1 = L_8;
		__this->___U3CU3E1__state_0 = 2;
		return (bool)1;
	}

IL_0076:
	{
		__this->___U3CU3E1__state_0 = (-1);
	}

IL_007d:
	{
		// while (nextCoroutine.MoveNext())
		RuntimeObject* L_9 = __this->___nextCoroutine_3;
		NullCheck(L_9);
		bool L_10;
		L_10 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_9);
		if (L_10)
		{
			goto IL_005c;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Single MECExtensionMethods2/<Append>d__13::System.Collections.Generic.IEnumerator<System.Single>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float U3CAppendU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_mF1194B0463CD98D042DF239E1C1666C8AE197A3E (U3CAppendU3Ed__13_tDA8C39AB4BF8F167235AE8A8F64E254A4BFF32E9* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void MECExtensionMethods2/<Append>d__13::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CAppendU3Ed__13_System_Collections_IEnumerator_Reset_m53165567602358B22AFB3317C8DE1C1A1E649824 (U3CAppendU3Ed__13_tDA8C39AB4BF8F167235AE8A8F64E254A4BFF32E9* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CAppendU3Ed__13_System_Collections_IEnumerator_Reset_m53165567602358B22AFB3317C8DE1C1A1E649824_RuntimeMethod_var)));
	}
}
// System.Object MECExtensionMethods2/<Append>d__13::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CAppendU3Ed__13_System_Collections_IEnumerator_get_Current_mE10C3112EDE62661EC71290F2CB4FB68CA61E491 (U3CAppendU3Ed__13_tDA8C39AB4BF8F167235AE8A8F64E254A4BFF32E9* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->___U3CU3E2__current_1;
		float L_1 = L_0;
		RuntimeObject* L_2 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MECExtensionMethods2/<Append>d__14::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CAppendU3Ed__14__ctor_m8F179A0583E8FFB49E2D69516A59513115B9D42B (U3CAppendU3Ed__14_t0E472D9D963516368FE446E17DB0A22607539C15* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void MECExtensionMethods2/<Append>d__14::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CAppendU3Ed__14_System_IDisposable_Dispose_mF7D184BB7F6C6F72AACCE3466B8E5BC099C21D35 (U3CAppendU3Ed__14_t0E472D9D963516368FE446E17DB0A22607539C15* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean MECExtensionMethods2/<Append>d__14::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CAppendU3Ed__14_MoveNext_mE41125C953CA615ADA7C3B3DEEB1C095586ECCDF (U3CAppendU3Ed__14_t0E472D9D963516368FE446E17DB0A22607539C15* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0033;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_003a;
	}

IL_0019:
	{
		// yield return coroutine.Current;
		RuntimeObject* L_3 = __this->___coroutine_2;
		NullCheck(L_3);
		float L_4;
		L_4 = InterfaceFuncInvoker0< float >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var, L_3);
		__this->___U3CU3E2__current_1 = L_4;
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0033:
	{
		__this->___U3CU3E1__state_0 = (-1);
	}

IL_003a:
	{
		// while (coroutine.MoveNext())
		RuntimeObject* L_5 = __this->___coroutine_2;
		NullCheck(L_5);
		bool L_6;
		L_6 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_5);
		if (L_6)
		{
			goto IL_0019;
		}
	}
	{
		// if (onDone != null)
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_7 = __this->___onDone_3;
		if (!L_7)
		{
			goto IL_005a;
		}
	}
	{
		// onDone();
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_8 = __this->___onDone_3;
		NullCheck(L_8);
		Action_Invoke_m7126A54DACA72B845424072887B5F3A51FC3808E_inline(L_8, NULL);
	}

IL_005a:
	{
		// }
		return (bool)0;
	}
}
// System.Single MECExtensionMethods2/<Append>d__14::System.Collections.Generic.IEnumerator<System.Single>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float U3CAppendU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_m61BFCE0A3F1F055562CDDE74654A132A8905F273 (U3CAppendU3Ed__14_t0E472D9D963516368FE446E17DB0A22607539C15* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void MECExtensionMethods2/<Append>d__14::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CAppendU3Ed__14_System_Collections_IEnumerator_Reset_m44792D85988AC0A44FE6BDA0EC19DAEE1F181582 (U3CAppendU3Ed__14_t0E472D9D963516368FE446E17DB0A22607539C15* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CAppendU3Ed__14_System_Collections_IEnumerator_Reset_m44792D85988AC0A44FE6BDA0EC19DAEE1F181582_RuntimeMethod_var)));
	}
}
// System.Object MECExtensionMethods2/<Append>d__14::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CAppendU3Ed__14_System_Collections_IEnumerator_get_Current_m45859FD1FEDF60B29763709E60A1B40A05B7ABBA (U3CAppendU3Ed__14_t0E472D9D963516368FE446E17DB0A22607539C15* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->___U3CU3E2__current_1;
		float L_1 = L_0;
		RuntimeObject* L_2 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MECExtensionMethods2/<Prepend>d__15::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CPrependU3Ed__15__ctor_mD88241B0D265A3F5FD24539D3EA596962A08F63F (U3CPrependU3Ed__15_t52DE12BFF91D5776D73148D2B29F7B452020EADB* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void MECExtensionMethods2/<Prepend>d__15::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CPrependU3Ed__15_System_IDisposable_Dispose_m95638772BB938EEC3FAC7518B4E10F3C84655DBC (U3CPrependU3Ed__15_t52DE12BFF91D5776D73148D2B29F7B452020EADB* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean MECExtensionMethods2/<Prepend>d__15::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CPrependU3Ed__15_MoveNext_m76993C3664991FC37B4700FC69200DC8CB5DADD6 (U3CPrependU3Ed__15_t52DE12BFF91D5776D73148D2B29F7B452020EADB* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001b;
			}
			case 1:
			{
				goto IL_0046;
			}
			case 2:
			{
				goto IL_0076;
			}
		}
	}
	{
		return (bool)0;
	}

IL_001b:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// if (lastCoroutine != null)
		RuntimeObject* L_2 = __this->___lastCoroutine_2;
		if (!L_2)
		{
			goto IL_007d;
		}
	}
	{
		goto IL_004d;
	}

IL_002c:
	{
		// yield return lastCoroutine.Current;
		RuntimeObject* L_3 = __this->___lastCoroutine_2;
		NullCheck(L_3);
		float L_4;
		L_4 = InterfaceFuncInvoker0< float >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var, L_3);
		__this->___U3CU3E2__current_1 = L_4;
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0046:
	{
		__this->___U3CU3E1__state_0 = (-1);
	}

IL_004d:
	{
		// while (lastCoroutine.MoveNext())
		RuntimeObject* L_5 = __this->___lastCoroutine_2;
		NullCheck(L_5);
		bool L_6;
		L_6 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_5);
		if (L_6)
		{
			goto IL_002c;
		}
	}
	{
		goto IL_007d;
	}

IL_005c:
	{
		// yield return coroutine.Current;
		RuntimeObject* L_7 = __this->___coroutine_3;
		NullCheck(L_7);
		float L_8;
		L_8 = InterfaceFuncInvoker0< float >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var, L_7);
		__this->___U3CU3E2__current_1 = L_8;
		__this->___U3CU3E1__state_0 = 2;
		return (bool)1;
	}

IL_0076:
	{
		__this->___U3CU3E1__state_0 = (-1);
	}

IL_007d:
	{
		// while (coroutine.MoveNext())
		RuntimeObject* L_9 = __this->___coroutine_3;
		NullCheck(L_9);
		bool L_10;
		L_10 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_9);
		if (L_10)
		{
			goto IL_005c;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Single MECExtensionMethods2/<Prepend>d__15::System.Collections.Generic.IEnumerator<System.Single>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float U3CPrependU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_m3285A7531288B2EAC4C5338D6256EEDE03D5AA7A (U3CPrependU3Ed__15_t52DE12BFF91D5776D73148D2B29F7B452020EADB* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void MECExtensionMethods2/<Prepend>d__15::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CPrependU3Ed__15_System_Collections_IEnumerator_Reset_mA49D8E8865EB9AB231E0FB3CCE31AAB6DB372C65 (U3CPrependU3Ed__15_t52DE12BFF91D5776D73148D2B29F7B452020EADB* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CPrependU3Ed__15_System_Collections_IEnumerator_Reset_mA49D8E8865EB9AB231E0FB3CCE31AAB6DB372C65_RuntimeMethod_var)));
	}
}
// System.Object MECExtensionMethods2/<Prepend>d__15::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CPrependU3Ed__15_System_Collections_IEnumerator_get_Current_m8F3435A6B9B9B674C90AE6CC8EE7F8B41C425755 (U3CPrependU3Ed__15_t52DE12BFF91D5776D73148D2B29F7B452020EADB* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->___U3CU3E2__current_1;
		float L_1 = L_0;
		RuntimeObject* L_2 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MECExtensionMethods2/<Prepend>d__16::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CPrependU3Ed__16__ctor_m64A2A21EFC706D087AA24B51726031DD34D9B127 (U3CPrependU3Ed__16_t023603E04E5CBE20E04C0E48452C5CA1597C446E* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void MECExtensionMethods2/<Prepend>d__16::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CPrependU3Ed__16_System_IDisposable_Dispose_m6EC44E9CEE82AC7E3E1D0B5C0A567B2BDEEDCD49 (U3CPrependU3Ed__16_t023603E04E5CBE20E04C0E48452C5CA1597C446E* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean MECExtensionMethods2/<Prepend>d__16::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CPrependU3Ed__16_MoveNext_mBCE2AFC58E6944EE91DC248703B5526026844236 (U3CPrependU3Ed__16_t023603E04E5CBE20E04C0E48452C5CA1597C446E* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0046;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// if (onStart != null)
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_3 = __this->___onStart_2;
		if (!L_3)
		{
			goto IL_004d;
		}
	}
	{
		// onStart();
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_4 = __this->___onStart_2;
		NullCheck(L_4);
		Action_Invoke_m7126A54DACA72B845424072887B5F3A51FC3808E_inline(L_4, NULL);
		goto IL_004d;
	}

IL_002c:
	{
		// yield return coroutine.Current;
		RuntimeObject* L_5 = __this->___coroutine_3;
		NullCheck(L_5);
		float L_6;
		L_6 = InterfaceFuncInvoker0< float >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var, L_5);
		__this->___U3CU3E2__current_1 = L_6;
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0046:
	{
		__this->___U3CU3E1__state_0 = (-1);
	}

IL_004d:
	{
		// while (coroutine.MoveNext())
		RuntimeObject* L_7 = __this->___coroutine_3;
		NullCheck(L_7);
		bool L_8;
		L_8 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_7);
		if (L_8)
		{
			goto IL_002c;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Single MECExtensionMethods2/<Prepend>d__16::System.Collections.Generic.IEnumerator<System.Single>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float U3CPrependU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_mAA7B4B0D492890B7CADFE1526DA43E60BCD6BAFF (U3CPrependU3Ed__16_t023603E04E5CBE20E04C0E48452C5CA1597C446E* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void MECExtensionMethods2/<Prepend>d__16::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CPrependU3Ed__16_System_Collections_IEnumerator_Reset_m35208B6A2CEB689B4E8B1BDE56332A6139A0A1EF (U3CPrependU3Ed__16_t023603E04E5CBE20E04C0E48452C5CA1597C446E* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CPrependU3Ed__16_System_Collections_IEnumerator_Reset_m35208B6A2CEB689B4E8B1BDE56332A6139A0A1EF_RuntimeMethod_var)));
	}
}
// System.Object MECExtensionMethods2/<Prepend>d__16::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CPrependU3Ed__16_System_Collections_IEnumerator_get_Current_m6337839A5F45894149DDB5920B6C4D517AB7123A (U3CPrependU3Ed__16_t023603E04E5CBE20E04C0E48452C5CA1597C446E* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->___U3CU3E2__current_1;
		float L_1 = L_0;
		RuntimeObject* L_2 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MECExtensionMethods2/<Superimpose>d__18::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSuperimposeU3Ed__18__ctor_m14A89B8EF2C764E26ADDDCCE0DA6A3AEC00AB3BA (U3CSuperimposeU3Ed__18_tE9CABB59D41E90F0F203FB1529715342294EF1B6* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void MECExtensionMethods2/<Superimpose>d__18::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSuperimposeU3Ed__18_System_IDisposable_Dispose_m7C247A2D71E6DB4A74560E0195A9AF072232BA2F (U3CSuperimposeU3Ed__18_tE9CABB59D41E90F0F203FB1529715342294EF1B6* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean MECExtensionMethods2/<Superimpose>d__18::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CSuperimposeU3Ed__18_MoveNext_m5A231606D5E2DBE3EF28AB908A2BB094BDC3EC6D (U3CSuperimposeU3Ed__18_tE9CABB59D41E90F0F203FB1529715342294EF1B6* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	U3CSuperimposeU3Ed__18_tE9CABB59D41E90F0F203FB1529715342294EF1B6* G_B21_0 = NULL;
	U3CSuperimposeU3Ed__18_tE9CABB59D41E90F0F203FB1529715342294EF1B6* G_B20_0 = NULL;
	float G_B22_0 = 0.0f;
	U3CSuperimposeU3Ed__18_tE9CABB59D41E90F0F203FB1529715342294EF1B6* G_B22_1 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0023;
			}
			case 1:
			{
				goto IL_00df;
			}
			case 2:
			{
				goto IL_013a;
			}
			case 3:
			{
				goto IL_016d;
			}
			case 4:
			{
				goto IL_0198;
			}
		}
	}
	{
		return (bool)0;
	}

IL_0023:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_019f;
	}

IL_002f:
	{
		// if (coroutineA != null && !(instance.localTime < coroutineA.Current) && !coroutineA.MoveNext())
		RuntimeObject* L_2 = __this->___coroutineA_2;
		if (!L_2)
		{
			goto IL_0063;
		}
	}
	{
		Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_3 = __this->___instance_3;
		NullCheck(L_3);
		float L_4 = L_3->___localTime_16;
		RuntimeObject* L_5 = __this->___coroutineA_2;
		NullCheck(L_5);
		float L_6;
		L_6 = InterfaceFuncInvoker0< float >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var, L_5);
		if ((((float)L_4) < ((float)L_6)))
		{
			goto IL_0063;
		}
	}
	{
		RuntimeObject* L_7 = __this->___coroutineA_2;
		NullCheck(L_7);
		bool L_8;
		L_8 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_7);
		if (L_8)
		{
			goto IL_0063;
		}
	}
	{
		// coroutineA = null;
		__this->___coroutineA_2 = (RuntimeObject*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___coroutineA_2), (void*)(RuntimeObject*)NULL);
	}

IL_0063:
	{
		// if (coroutineB != null && !(instance.localTime < coroutineB.Current) && !coroutineB.MoveNext())
		RuntimeObject* L_9 = __this->___coroutineB_4;
		if (!L_9)
		{
			goto IL_0097;
		}
	}
	{
		Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_10 = __this->___instance_3;
		NullCheck(L_10);
		float L_11 = L_10->___localTime_16;
		RuntimeObject* L_12 = __this->___coroutineB_4;
		NullCheck(L_12);
		float L_13;
		L_13 = InterfaceFuncInvoker0< float >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var, L_12);
		if ((((float)L_11) < ((float)L_13)))
		{
			goto IL_0097;
		}
	}
	{
		RuntimeObject* L_14 = __this->___coroutineB_4;
		NullCheck(L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_14);
		if (L_15)
		{
			goto IL_0097;
		}
	}
	{
		// coroutineB = null;
		__this->___coroutineB_4 = (RuntimeObject*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___coroutineB_4), (void*)(RuntimeObject*)NULL);
	}

IL_0097:
	{
		// if ((coroutineA != null && float.IsNaN(coroutineA.Current)) || (coroutineB != null && float.IsNaN(coroutineB.Current)))
		RuntimeObject* L_16 = __this->___coroutineA_2;
		if (!L_16)
		{
			goto IL_00b1;
		}
	}
	{
		RuntimeObject* L_17 = __this->___coroutineA_2;
		NullCheck(L_17);
		float L_18;
		L_18 = InterfaceFuncInvoker0< float >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var, L_17);
		bool L_19;
		L_19 = Single_IsNaN_m684B090AA2F895FD91821CA8684CBC11D784E4DD_inline(L_18, NULL);
		if (L_19)
		{
			goto IL_00cb;
		}
	}

IL_00b1:
	{
		RuntimeObject* L_20 = __this->___coroutineB_4;
		if (!L_20)
		{
			goto IL_00eb;
		}
	}
	{
		RuntimeObject* L_21 = __this->___coroutineB_4;
		NullCheck(L_21);
		float L_22;
		L_22 = InterfaceFuncInvoker0< float >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var, L_21);
		bool L_23;
		L_23 = Single_IsNaN_m684B090AA2F895FD91821CA8684CBC11D784E4DD_inline(L_22, NULL);
		if (!L_23)
		{
			goto IL_00eb;
		}
	}

IL_00cb:
	{
		// yield return float.NaN;
		__this->___U3CU3E2__current_1 = (std::numeric_limits<float>::quiet_NaN());
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_00df:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_019f;
	}

IL_00eb:
	{
		// else if (coroutineA != null && coroutineB != null)
		RuntimeObject* L_24 = __this->___coroutineA_2;
		if (!L_24)
		{
			goto IL_0143;
		}
	}
	{
		RuntimeObject* L_25 = __this->___coroutineB_4;
		if (!L_25)
		{
			goto IL_0143;
		}
	}
	{
		// yield return coroutineA.Current < coroutineB.Current ? coroutineA.Current : coroutineB.Current;
		RuntimeObject* L_26 = __this->___coroutineA_2;
		NullCheck(L_26);
		float L_27;
		L_27 = InterfaceFuncInvoker0< float >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var, L_26);
		RuntimeObject* L_28 = __this->___coroutineB_4;
		NullCheck(L_28);
		float L_29;
		L_29 = InterfaceFuncInvoker0< float >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var, L_28);
		G_B20_0 = __this;
		if ((((float)L_27) < ((float)L_29)))
		{
			G_B21_0 = __this;
			goto IL_0121;
		}
	}
	{
		RuntimeObject* L_30 = __this->___coroutineB_4;
		NullCheck(L_30);
		float L_31;
		L_31 = InterfaceFuncInvoker0< float >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var, L_30);
		G_B22_0 = L_31;
		G_B22_1 = G_B20_0;
		goto IL_012c;
	}

IL_0121:
	{
		RuntimeObject* L_32 = __this->___coroutineA_2;
		NullCheck(L_32);
		float L_33;
		L_33 = InterfaceFuncInvoker0< float >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var, L_32);
		G_B22_0 = L_33;
		G_B22_1 = G_B21_0;
	}

IL_012c:
	{
		NullCheck(G_B22_1);
		G_B22_1->___U3CU3E2__current_1 = G_B22_0;
		__this->___U3CU3E1__state_0 = 2;
		return (bool)1;
	}

IL_013a:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_019f;
	}

IL_0143:
	{
		// else if (coroutineA == null && coroutineB != null)
		RuntimeObject* L_34 = __this->___coroutineA_2;
		if (L_34)
		{
			goto IL_0176;
		}
	}
	{
		RuntimeObject* L_35 = __this->___coroutineB_4;
		if (!L_35)
		{
			goto IL_0176;
		}
	}
	{
		// yield return coroutineB.Current;
		RuntimeObject* L_36 = __this->___coroutineB_4;
		NullCheck(L_36);
		float L_37;
		L_37 = InterfaceFuncInvoker0< float >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var, L_36);
		__this->___U3CU3E2__current_1 = L_37;
		__this->___U3CU3E1__state_0 = 3;
		return (bool)1;
	}

IL_016d:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_019f;
	}

IL_0176:
	{
		// else if (coroutineA != null)
		RuntimeObject* L_38 = __this->___coroutineA_2;
		if (!L_38)
		{
			goto IL_019f;
		}
	}
	{
		// yield return coroutineA.Current;
		RuntimeObject* L_39 = __this->___coroutineA_2;
		NullCheck(L_39);
		float L_40;
		L_40 = InterfaceFuncInvoker0< float >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var, L_39);
		__this->___U3CU3E2__current_1 = L_40;
		__this->___U3CU3E1__state_0 = 4;
		return (bool)1;
	}

IL_0198:
	{
		__this->___U3CU3E1__state_0 = (-1);
	}

IL_019f:
	{
		// while (coroutineA != null || coroutineB != null)
		RuntimeObject* L_41 = __this->___coroutineA_2;
		if (L_41)
		{
			goto IL_002f;
		}
	}
	{
		RuntimeObject* L_42 = __this->___coroutineB_4;
		if (L_42)
		{
			goto IL_002f;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Single MECExtensionMethods2/<Superimpose>d__18::System.Collections.Generic.IEnumerator<System.Single>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float U3CSuperimposeU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_m6DA8072EBFFBC2B865D0DE2B06823D0A8B7E61CD (U3CSuperimposeU3Ed__18_tE9CABB59D41E90F0F203FB1529715342294EF1B6* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void MECExtensionMethods2/<Superimpose>d__18::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSuperimposeU3Ed__18_System_Collections_IEnumerator_Reset_m0D4D07DF6253C85A2E560CE041AC14FD1BCA3D2B (U3CSuperimposeU3Ed__18_tE9CABB59D41E90F0F203FB1529715342294EF1B6* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CSuperimposeU3Ed__18_System_Collections_IEnumerator_Reset_m0D4D07DF6253C85A2E560CE041AC14FD1BCA3D2B_RuntimeMethod_var)));
	}
}
// System.Object MECExtensionMethods2/<Superimpose>d__18::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CSuperimposeU3Ed__18_System_Collections_IEnumerator_get_Current_mB0F9F9F3BC27012AC4B797749311783BB3636C39 (U3CSuperimposeU3Ed__18_tE9CABB59D41E90F0F203FB1529715342294EF1B6* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->___U3CU3E2__current_1;
		float L_1 = L_0;
		RuntimeObject* L_2 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MECExtensionMethods2/<Hijack>d__19::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CHijackU3Ed__19__ctor_m00BFFC3B2E7C61F564D9A62A3508833FF491DEAB (U3CHijackU3Ed__19_t57ED3438E560C43D619C0606A97C7FB3B547EBB1* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void MECExtensionMethods2/<Hijack>d__19::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CHijackU3Ed__19_System_IDisposable_Dispose_mA3139C10D734A7F53C24CAB8760168D9B0426770 (U3CHijackU3Ed__19_t57ED3438E560C43D619C0606A97C7FB3B547EBB1* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean MECExtensionMethods2/<Hijack>d__19::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CHijackU3Ed__19_MoveNext_m8453BBA79F6FEDBA3996EEAA8B7D1B62F8438BF4 (U3CHijackU3Ed__19_t57ED3438E560C43D619C0606A97C7FB3B547EBB1* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0046;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// if (newReturn == null) yield break;
		Func_2_t2A7432CC4F64D0DF6D8629208B154CF139B39AF2* L_3 = __this->___newReturn_2;
		if (L_3)
		{
			goto IL_004d;
		}
	}
	{
		// if (newReturn == null) yield break;
		return (bool)0;
	}

IL_0021:
	{
		// yield return newReturn(coroutine.Current);
		Func_2_t2A7432CC4F64D0DF6D8629208B154CF139B39AF2* L_4 = __this->___newReturn_2;
		RuntimeObject* L_5 = __this->___coroutine_3;
		NullCheck(L_5);
		float L_6;
		L_6 = InterfaceFuncInvoker0< float >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var, L_5);
		NullCheck(L_4);
		float L_7;
		L_7 = Func_2_Invoke_m5728ECFB038CFC6FEF889DC2D566EEF49D0E24B9_inline(L_4, L_6, NULL);
		__this->___U3CU3E2__current_1 = L_7;
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0046:
	{
		__this->___U3CU3E1__state_0 = (-1);
	}

IL_004d:
	{
		// while (coroutine.MoveNext())
		RuntimeObject* L_8 = __this->___coroutine_3;
		NullCheck(L_8);
		bool L_9;
		L_9 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_8);
		if (L_9)
		{
			goto IL_0021;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Single MECExtensionMethods2/<Hijack>d__19::System.Collections.Generic.IEnumerator<System.Single>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float U3CHijackU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_m2E8E4B0D9E066BE214DDE4B7E3AD7724FEE7C30E (U3CHijackU3Ed__19_t57ED3438E560C43D619C0606A97C7FB3B547EBB1* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void MECExtensionMethods2/<Hijack>d__19::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CHijackU3Ed__19_System_Collections_IEnumerator_Reset_m009F853065024DFB852BCBF17965E120B8A93247 (U3CHijackU3Ed__19_t57ED3438E560C43D619C0606A97C7FB3B547EBB1* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CHijackU3Ed__19_System_Collections_IEnumerator_Reset_m009F853065024DFB852BCBF17965E120B8A93247_RuntimeMethod_var)));
	}
}
// System.Object MECExtensionMethods2/<Hijack>d__19::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CHijackU3Ed__19_System_Collections_IEnumerator_get_Current_mED11EB3D4C3873D8A437CA9749321B1DEA817C6F (U3CHijackU3Ed__19_t57ED3438E560C43D619C0606A97C7FB3B547EBB1* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->___U3CU3E2__current_1;
		float L_1 = L_0;
		RuntimeObject* L_2 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MECExtensionMethods2/<RerouteExceptions>d__20::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CRerouteExceptionsU3Ed__20__ctor_mA318DE4F8F2A5AB948440CB6B7F63C2F41E3670D (U3CRerouteExceptionsU3Ed__20_t34352A3C593E8FF83A90E02879CEAD71D7417184* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void MECExtensionMethods2/<RerouteExceptions>d__20::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CRerouteExceptionsU3Ed__20_System_IDisposable_Dispose_m27C2C2F4E88C6A337E93A1D11ED625702658157D (U3CRerouteExceptionsU3Ed__20_t34352A3C593E8FF83A90E02879CEAD71D7417184* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean MECExtensionMethods2/<RerouteExceptions>d__20::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CRerouteExceptionsU3Ed__20_MoveNext_m59D18041650F055AA1540611E365AD4683A77B4C (U3CRerouteExceptionsU3Ed__20_t34352A3C593E8FF83A90E02879CEAD71D7417184* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	Exception_t* V_2 = NULL;
	il2cpp::utils::ExceptionSupportStack<RuntimeObject*, 1> __active_exceptions;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_1 = L_0;
		int32_t L_1 = V_1;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_1;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_005e;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		__this->___U3CU3E1__state_0 = (-1);
	}

IL_0017:
	{
	}
	try
	{// begin try (depth: 1)
		{
			// if (!coroutine.MoveNext())
			RuntimeObject* L_3 = __this->___coroutine_2;
			NullCheck(L_3);
			bool L_4;
			L_4 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_3);
			if (L_4)
			{
				goto IL_0029_1;
			}
		}
		{
			// yield break;
			V_0 = (bool)0;
			goto IL_0067;
		}

IL_0029_1:
		{
			// }
			goto IL_0044;
		}
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
		{
			IL2CPP_PUSH_ACTIVE_EXCEPTION(e.ex);
			goto CATCH_002b;
		}
		throw e;
	}

CATCH_002b:
	{// begin catch(System.Exception)
		{
			// catch (System.Exception ex)
			V_2 = ((Exception_t*)IL2CPP_GET_ACTIVE_EXCEPTION(Exception_t*));
			// if (exceptionHandler != null)
			Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_5 = __this->___exceptionHandler_3;
			if (!L_5)
			{
				goto IL_0040;
			}
		}
		{
			// exceptionHandler(ex);
			Action_1_tAFBD759E01ADE1CCF9C2015D5EFB3E69A9F26F04* L_6 = __this->___exceptionHandler_3;
			Exception_t* L_7 = V_2;
			NullCheck(L_6);
			Action_1_Invoke_m43B5C4C0F292CE3E07CB03B46D8F960ACF7D6A58_inline(L_6, L_7, NULL);
		}

IL_0040:
		{
			// yield break;
			V_0 = (bool)0;
			IL2CPP_POP_ACTIVE_EXCEPTION();
			goto IL_0067;
		}
	}// end catch (depth: 1)

IL_0044:
	{
		// yield return coroutine.Current;
		RuntimeObject* L_8 = __this->___coroutine_2;
		NullCheck(L_8);
		float L_9;
		L_9 = InterfaceFuncInvoker0< float >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var, L_8);
		__this->___U3CU3E2__current_1 = L_9;
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_005e:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// while(true)
		goto IL_0017;
	}

IL_0067:
	{
		bool L_10 = V_0;
		return L_10;
	}
}
// System.Single MECExtensionMethods2/<RerouteExceptions>d__20::System.Collections.Generic.IEnumerator<System.Single>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float U3CRerouteExceptionsU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_mF31F126286361897289520756AE43DDA8FB47D74 (U3CRerouteExceptionsU3Ed__20_t34352A3C593E8FF83A90E02879CEAD71D7417184* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void MECExtensionMethods2/<RerouteExceptions>d__20::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CRerouteExceptionsU3Ed__20_System_Collections_IEnumerator_Reset_m13856FEAEC1D9666C40A681C2C796634ABDE89D3 (U3CRerouteExceptionsU3Ed__20_t34352A3C593E8FF83A90E02879CEAD71D7417184* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CRerouteExceptionsU3Ed__20_System_Collections_IEnumerator_Reset_m13856FEAEC1D9666C40A681C2C796634ABDE89D3_RuntimeMethod_var)));
	}
}
// System.Object MECExtensionMethods2/<RerouteExceptions>d__20::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CRerouteExceptionsU3Ed__20_System_Collections_IEnumerator_get_Current_m0AA775D17EDB581B19D78CA4501601AEF16E792D (U3CRerouteExceptionsU3Ed__20_t34352A3C593E8FF83A90E02879CEAD71D7417184* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->___U3CU3E2__current_1;
		float L_1 = L_0;
		RuntimeObject* L_2 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean MEC.Timing/ProcessIndex::Equals(MEC.Timing/ProcessIndex)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ProcessIndex_Equals_m0606720D3D1A042B4FEE328E47520C1F54FAAAFC (ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369* __this, ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 ___other0, const RuntimeMethod* method) 
{
	{
		// return seg == other.seg && i == other.i;
		int32_t L_0 = __this->___seg_0;
		ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 L_1 = ___other0;
		int32_t L_2 = L_1.___seg_0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_2))))
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_3 = __this->___i_1;
		ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 L_4 = ___other0;
		int32_t L_5 = L_4.___i_1;
		return (bool)((((int32_t)L_3) == ((int32_t)L_5))? 1 : 0);
	}

IL_001d:
	{
		return (bool)0;
	}
}
IL2CPP_EXTERN_C  bool ProcessIndex_Equals_m0606720D3D1A042B4FEE328E47520C1F54FAAAFC_AdjustorThunk (RuntimeObject* __this, ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 ___other0, const RuntimeMethod* method)
{
	ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369*>(__this + _offset);
	bool _returnValue;
	_returnValue = ProcessIndex_Equals_m0606720D3D1A042B4FEE328E47520C1F54FAAAFC(_thisAdjusted, ___other0, method);
	return _returnValue;
}
// System.Boolean MEC.Timing/ProcessIndex::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ProcessIndex_Equals_mE98E3DCDA417344D6838C798774A6B41F375D201 (ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369* __this, RuntimeObject* ___other0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (other is ProcessIndex)
		RuntimeObject* L_0 = ___other0;
		if (!((RuntimeObject*)IsInstSealed((RuntimeObject*)L_0, ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369_il2cpp_TypeInfo_var)))
		{
			goto IL_0015;
		}
	}
	{
		// return Equals((ProcessIndex)other);
		RuntimeObject* L_1 = ___other0;
		bool L_2;
		L_2 = ProcessIndex_Equals_m0606720D3D1A042B4FEE328E47520C1F54FAAAFC(__this, ((*(ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369*)((ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369*)(ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369*)UnBox(L_1, ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369_il2cpp_TypeInfo_var)))), NULL);
		return L_2;
	}

IL_0015:
	{
		// return false;
		return (bool)0;
	}
}
IL2CPP_EXTERN_C  bool ProcessIndex_Equals_mE98E3DCDA417344D6838C798774A6B41F375D201_AdjustorThunk (RuntimeObject* __this, RuntimeObject* ___other0, const RuntimeMethod* method)
{
	ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369*>(__this + _offset);
	bool _returnValue;
	_returnValue = ProcessIndex_Equals_mE98E3DCDA417344D6838C798774A6B41F375D201(_thisAdjusted, ___other0, method);
	return _returnValue;
}
// System.Boolean MEC.Timing/ProcessIndex::op_Equality(MEC.Timing/ProcessIndex,MEC.Timing/ProcessIndex)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ProcessIndex_op_Equality_m8EADCD4276A3D029C835A3DF695CBEE42D508EEF (ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 ___a0, ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 ___b1, const RuntimeMethod* method) 
{
	{
		// return a.seg == b.seg && a.i == b.i;
		ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 L_0 = ___a0;
		int32_t L_1 = L_0.___seg_0;
		ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 L_2 = ___b1;
		int32_t L_3 = L_2.___seg_0;
		if ((!(((uint32_t)L_1) == ((uint32_t)L_3))))
		{
			goto IL_001d;
		}
	}
	{
		ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 L_4 = ___a0;
		int32_t L_5 = L_4.___i_1;
		ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 L_6 = ___b1;
		int32_t L_7 = L_6.___i_1;
		return (bool)((((int32_t)L_5) == ((int32_t)L_7))? 1 : 0);
	}

IL_001d:
	{
		return (bool)0;
	}
}
// System.Boolean MEC.Timing/ProcessIndex::op_Inequality(MEC.Timing/ProcessIndex,MEC.Timing/ProcessIndex)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ProcessIndex_op_Inequality_m6D46D2ABE45D32003A54FDDFB5402B18B3CC1E08 (ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 ___a0, ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 ___b1, const RuntimeMethod* method) 
{
	{
		// return a.seg != b.seg || a.i != b.i;
		ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 L_0 = ___a0;
		int32_t L_1 = L_0.___seg_0;
		ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 L_2 = ___b1;
		int32_t L_3 = L_2.___seg_0;
		if ((!(((uint32_t)L_1) == ((uint32_t)L_3))))
		{
			goto IL_0020;
		}
	}
	{
		ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 L_4 = ___a0;
		int32_t L_5 = L_4.___i_1;
		ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 L_6 = ___b1;
		int32_t L_7 = L_6.___i_1;
		return (bool)((((int32_t)((((int32_t)L_5) == ((int32_t)L_7))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_0020:
	{
		return (bool)1;
	}
}
// System.Int32 MEC.Timing/ProcessIndex::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ProcessIndex_GetHashCode_m4AAF9C4E8445B9A0FBF925F8E51032FBA39250D7 (ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369* __this, const RuntimeMethod* method) 
{
	{
		// return (((int)seg - 4) * (int.MaxValue / 7)) + i;
		int32_t L_0 = __this->___seg_0;
		int32_t L_1 = __this->___i_1;
		return ((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_multiply(((int32_t)il2cpp_codegen_subtract((int32_t)L_0, 4)), ((int32_t)306783378))), L_1));
	}
}
IL2CPP_EXTERN_C  int32_t ProcessIndex_GetHashCode_m4AAF9C4E8445B9A0FBF925F8E51032FBA39250D7_AdjustorThunk (RuntimeObject* __this, const RuntimeMethod* method)
{
	ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369*>(__this + _offset);
	int32_t _returnValue;
	_returnValue = ProcessIndex_GetHashCode_m4AAF9C4E8445B9A0FBF925F8E51032FBA39250D7(_thisAdjusted, method);
	return _returnValue;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MEC.Timing/<_EOFPumpWatcher>d__132::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_EOFPumpWatcherU3Ed__132__ctor_m503187620AB45547A945263D4A04628213171275 (U3C_EOFPumpWatcherU3Ed__132_t083D79457AC8E327332F8E9614E1F0625985BBED* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void MEC.Timing/<_EOFPumpWatcher>d__132::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_EOFPumpWatcherU3Ed__132_System_IDisposable_Dispose_m5DD7DD17C1B9DA86B88B12B99E7D347F9C657541 (U3C_EOFPumpWatcherU3Ed__132_t083D79457AC8E327332F8E9614E1F0625985BBED* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean MEC.Timing/<_EOFPumpWatcher>d__132::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3C_EOFPumpWatcherU3Ed__132_MoveNext_m9CE78A9FC6789EBF826C5FAFB97E72A630B61AC8 (U3C_EOFPumpWatcherU3Ed__132_t083D79457AC8E327332F8E9614E1F0625985BBED* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* V_1 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_1 = __this->___U3CU3E4__this_2;
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0050;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_0057;
	}

IL_0020:
	{
		// if (!_EOFPumpRan)
		Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = L_4->____EOFPumpRan_64;
		if (L_5)
		{
			goto IL_0035;
		}
	}
	{
		// base.StartCoroutine(_EOFPump());
		Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_6 = V_1;
		Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_7 = V_1;
		NullCheck(L_7);
		RuntimeObject* L_8;
		L_8 = Timing__EOFPump_m11B1F13D2395603365EE502A583B9397737F39F3(L_7, NULL);
		NullCheck(L_6);
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_9;
		L_9 = MonoBehaviour_StartCoroutine_m4CAFF732AA28CD3BDC5363B44A863575530EC812(L_6, L_8, NULL);
	}

IL_0035:
	{
		// _EOFPumpRan = false;
		Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_10 = V_1;
		NullCheck(L_10);
		L_10->____EOFPumpRan_64 = (bool)0;
		// yield return WaitForOneFrame;
		__this->___U3CU3E2__current_1 = (-std::numeric_limits<float>::infinity());
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0050:
	{
		__this->___U3CU3E1__state_0 = (-1);
	}

IL_0057:
	{
		// while (_nextEndOfFrameProcessSlot > 0)
		Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_11 = V_1;
		NullCheck(L_11);
		int32_t L_12 = L_11->____nextEndOfFrameProcessSlot_41;
		if ((((int32_t)L_12) > ((int32_t)0)))
		{
			goto IL_0020;
		}
	}
	{
		// _EOFPumpRan = false;
		Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_13 = V_1;
		NullCheck(L_13);
		L_13->____EOFPumpRan_64 = (bool)0;
		// }
		return (bool)0;
	}
}
// System.Single MEC.Timing/<_EOFPumpWatcher>d__132::System.Collections.Generic.IEnumerator<System.Single>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float U3C_EOFPumpWatcherU3Ed__132_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_m1D97FBD3443BEE8AE77852BE30E900FB3739ECFB (U3C_EOFPumpWatcherU3Ed__132_t083D79457AC8E327332F8E9614E1F0625985BBED* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void MEC.Timing/<_EOFPumpWatcher>d__132::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_EOFPumpWatcherU3Ed__132_System_Collections_IEnumerator_Reset_m15E6D5092EF7E27BAC2B1CA1106976EAC0F5F0BD (U3C_EOFPumpWatcherU3Ed__132_t083D79457AC8E327332F8E9614E1F0625985BBED* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3C_EOFPumpWatcherU3Ed__132_System_Collections_IEnumerator_Reset_m15E6D5092EF7E27BAC2B1CA1106976EAC0F5F0BD_RuntimeMethod_var)));
	}
}
// System.Object MEC.Timing/<_EOFPumpWatcher>d__132::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3C_EOFPumpWatcherU3Ed__132_System_Collections_IEnumerator_get_Current_m33954C135746380CE70AAFA3921B2BB9A5FC8CA1 (U3C_EOFPumpWatcherU3Ed__132_t083D79457AC8E327332F8E9614E1F0625985BBED* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->___U3CU3E2__current_1;
		float L_1 = L_0;
		RuntimeObject* L_2 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MEC.Timing/<_EOFPump>d__133::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_EOFPumpU3Ed__133__ctor_m554A35797A4E8BC6D6521E141A7A9702303D6CF0 (U3C_EOFPumpU3Ed__133_tE02D8D1E07F743410384C7B9F487FC45070B6C01* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void MEC.Timing/<_EOFPump>d__133::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_EOFPumpU3Ed__133_System_IDisposable_Dispose_mA3688C3A81A334FFA1E282D210EEA365169E2524 (U3C_EOFPumpU3Ed__133_tE02D8D1E07F743410384C7B9F487FC45070B6C01* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean MEC.Timing/<_EOFPump>d__133::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3C_EOFPumpU3Ed__133_MoveNext_mB99E54B478769B89A44F7EA1537B72B7BFBD7F09 (U3C_EOFPumpU3Ed__133_tE02D8D1E07F743410384C7B9F487FC45070B6C01* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_mD41C34E43A72893DA9509BA2C25ED1F65DED90E8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_mEE876E1A07892EE036FBCB85F5C96E283EADBEB7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* V_1 = NULL;
	ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 V_2;
	memset((&V_2), 0, sizeof(V_2));
	ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 V_3;
	memset((&V_3), 0, sizeof(V_3));
	CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 V_4;
	memset((&V_4), 0, sizeof(V_4));
	il2cpp::utils::ExceptionSupportStack<RuntimeObject*, 1> __active_exceptions;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_1 = __this->___U3CU3E4__this_2;
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0037;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_01f4;
	}

IL_0023:
	{
		// yield return EofWaitObject;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663* L_4 = ((Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_StaticFields*)il2cpp_codegen_static_fields_for(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var))->___EofWaitObject_66;
		__this->___U3CU3E2__current_1 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_4);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0037:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// if (OnPreExecute != null)
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_5 = ((Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_StaticFields*)il2cpp_codegen_static_fields_for(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var))->___OnPreExecute_20;
		if (!L_5)
		{
			goto IL_004f;
		}
	}
	{
		// OnPreExecute();
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_6 = ((Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_StaticFields*)il2cpp_codegen_static_fields_for(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var))->___OnPreExecute_20;
		NullCheck(L_6);
		Action_Invoke_m7126A54DACA72B845424072887B5F3A51FC3808E_inline(L_6, NULL);
	}

IL_004f:
	{
		// ProcessIndex coindex = new ProcessIndex { seg = Segment.EndOfFrame };
		il2cpp_codegen_initobj((&V_3), sizeof(ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369));
		(&V_3)->___seg_0 = 7;
		ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 L_7 = V_3;
		V_2 = L_7;
		// _EOFPumpRan = true;
		Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_8 = V_1;
		NullCheck(L_8);
		L_8->____EOFPumpRan_64 = (bool)1;
		// if (UpdateTimeValues(coindex.seg))
		Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_9 = V_1;
		ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 L_10 = V_2;
		int32_t L_11 = L_10.___seg_0;
		NullCheck(L_9);
		bool L_12;
		L_12 = Timing_UpdateTimeValues_m99DABFAC068CCA50B69C9C9F60405097A453EF72(L_9, L_11, NULL);
		if (!L_12)
		{
			goto IL_0082;
		}
	}
	{
		// _lastEndOfFrameProcessSlot = _nextEndOfFrameProcessSlot;
		Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_13 = V_1;
		Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_14 = V_1;
		NullCheck(L_14);
		int32_t L_15 = L_14->____nextEndOfFrameProcessSlot_41;
		NullCheck(L_13);
		L_13->____lastEndOfFrameProcessSlot_48 = L_15;
	}

IL_0082:
	{
		// for (coindex.i = 0; coindex.i < _lastEndOfFrameProcessSlot; coindex.i++)
		(&V_2)->___i_1 = 0;
		goto IL_01e3;
	}

IL_008f:
	{
	}
	try
	{// begin try (depth: 1)
		{
			// if (!EndOfFramePaused[coindex.i] && !EndOfFrameHeld[coindex.i] && EndOfFrameProcesses[coindex.i] != null && !(localTime < EndOfFrameProcesses[coindex.i].Current))
			Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_16 = V_1;
			NullCheck(L_16);
			BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4* L_17 = L_16->___EndOfFramePaused_91;
			ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 L_18 = V_2;
			int32_t L_19 = L_18.___i_1;
			NullCheck(L_17);
			int32_t L_20 = L_19;
			uint8_t L_21 = (uint8_t)(L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
			if (L_21)
			{
				goto IL_01bc_1;
			}
		}
		{
			Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_22 = V_1;
			NullCheck(L_22);
			BooleanU5BU5D_tD317D27C31DB892BE79FAE3AEBC0B3FFB73DE9B4* L_23 = L_22->___EndOfFrameHeld_100;
			ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 L_24 = V_2;
			int32_t L_25 = L_24.___i_1;
			NullCheck(L_23);
			int32_t L_26 = L_25;
			uint8_t L_27 = (uint8_t)(L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
			if (L_27)
			{
				goto IL_01bc_1;
			}
		}
		{
			Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_28 = V_1;
			NullCheck(L_28);
			IEnumerator_1U5BU5D_tA2EF2FFDBA098C53558E3177C143C4DCFB0E5B88* L_29 = L_28->___EndOfFrameProcesses_82;
			ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 L_30 = V_2;
			int32_t L_31 = L_30.___i_1;
			NullCheck(L_29);
			int32_t L_32 = L_31;
			RuntimeObject* L_33 = (L_29)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
			if (!L_33)
			{
				goto IL_01bc_1;
			}
		}
		{
			Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_34 = V_1;
			NullCheck(L_34);
			float L_35 = L_34->___localTime_16;
			Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_36 = V_1;
			NullCheck(L_36);
			IEnumerator_1U5BU5D_tA2EF2FFDBA098C53558E3177C143C4DCFB0E5B88* L_37 = L_36->___EndOfFrameProcesses_82;
			ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 L_38 = V_2;
			int32_t L_39 = L_38.___i_1;
			NullCheck(L_37);
			int32_t L_40 = L_39;
			RuntimeObject* L_41 = (L_37)->GetAt(static_cast<il2cpp_array_size_t>(L_40));
			NullCheck(L_41);
			float L_42;
			L_42 = InterfaceFuncInvoker0< float >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var, L_41);
			if ((((float)L_35) < ((float)L_42)))
			{
				goto IL_01bc_1;
			}
		}
		{
			// currentCoroutine = _indexToHandle[coindex];
			Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_43 = V_1;
			Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_44 = V_1;
			NullCheck(L_44);
			Dictionary_2_t8C5B8A51DE825AB5FC3EDF04011A63CE33EBF2DB* L_45 = L_44->____indexToHandle_70;
			ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 L_46 = V_2;
			NullCheck(L_45);
			CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_47;
			L_47 = Dictionary_2_get_Item_mEE876E1A07892EE036FBCB85F5C96E283EADBEB7(L_45, L_46, Dictionary_2_get_Item_mEE876E1A07892EE036FBCB85F5C96E283EADBEB7_RuntimeMethod_var);
			NullCheck(L_43);
			Timing_set_currentCoroutine_m3DBBE23CAA1674B5622501475023ECCF1BFAD433_inline(L_43, L_47, NULL);
			// if (ProfilerDebugAmount != DebugInfoType.None && _indexToHandle.ContainsKey(coindex))
			Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_48 = V_1;
			NullCheck(L_48);
			int32_t L_49 = L_48->___ProfilerDebugAmount_5;
			if (!L_49)
			{
				goto IL_010a_1;
			}
		}
		{
			Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_50 = V_1;
			NullCheck(L_50);
			Dictionary_2_t8C5B8A51DE825AB5FC3EDF04011A63CE33EBF2DB* L_51 = L_50->____indexToHandle_70;
			ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 L_52 = V_2;
			NullCheck(L_51);
			bool L_53;
			L_53 = Dictionary_2_ContainsKey_mD41C34E43A72893DA9509BA2C25ED1F65DED90E8(L_51, L_52, Dictionary_2_ContainsKey_mD41C34E43A72893DA9509BA2C25ED1F65DED90E8_RuntimeMethod_var);
		}

IL_010a_1:
		{
			// if (!EndOfFrameProcesses[coindex.i].MoveNext())
			Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_54 = V_1;
			NullCheck(L_54);
			IEnumerator_1U5BU5D_tA2EF2FFDBA098C53558E3177C143C4DCFB0E5B88* L_55 = L_54->___EndOfFrameProcesses_82;
			ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 L_56 = V_2;
			int32_t L_57 = L_56.___i_1;
			NullCheck(L_55);
			int32_t L_58 = L_57;
			RuntimeObject* L_59 = (L_55)->GetAt(static_cast<il2cpp_array_size_t>(L_58));
			NullCheck(L_59);
			bool L_60;
			L_60 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_59);
			if (L_60)
			{
				goto IL_0144_1;
			}
		}
		{
			// if (_indexToHandle.ContainsKey(coindex))
			Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_61 = V_1;
			NullCheck(L_61);
			Dictionary_2_t8C5B8A51DE825AB5FC3EDF04011A63CE33EBF2DB* L_62 = L_61->____indexToHandle_70;
			ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 L_63 = V_2;
			NullCheck(L_62);
			bool L_64;
			L_64 = Dictionary_2_ContainsKey_mD41C34E43A72893DA9509BA2C25ED1F65DED90E8(L_62, L_63, Dictionary_2_ContainsKey_mD41C34E43A72893DA9509BA2C25ED1F65DED90E8_RuntimeMethod_var);
			if (!L_64)
			{
				goto IL_01b5_1;
			}
		}
		{
			// KillCoroutinesOnInstance(_indexToHandle[coindex]);
			Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_65 = V_1;
			Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_66 = V_1;
			NullCheck(L_66);
			Dictionary_2_t8C5B8A51DE825AB5FC3EDF04011A63CE33EBF2DB* L_67 = L_66->____indexToHandle_70;
			ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 L_68 = V_2;
			NullCheck(L_67);
			CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_69;
			L_69 = Dictionary_2_get_Item_mEE876E1A07892EE036FBCB85F5C96E283EADBEB7(L_67, L_68, Dictionary_2_get_Item_mEE876E1A07892EE036FBCB85F5C96E283EADBEB7_RuntimeMethod_var);
			NullCheck(L_65);
			int32_t L_70;
			L_70 = Timing_KillCoroutinesOnInstance_m2FBCFCA895C9A7EF1AB2FD59D13503CDA9D69844(L_65, L_69, NULL);
			goto IL_01b5_1;
		}

IL_0144_1:
		{
			// else if (EndOfFrameProcesses[coindex.i] != null && float.IsNaN(EndOfFrameProcesses[coindex.i].Current))
			Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_71 = V_1;
			NullCheck(L_71);
			IEnumerator_1U5BU5D_tA2EF2FFDBA098C53558E3177C143C4DCFB0E5B88* L_72 = L_71->___EndOfFrameProcesses_82;
			ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 L_73 = V_2;
			int32_t L_74 = L_73.___i_1;
			NullCheck(L_72);
			int32_t L_75 = L_74;
			RuntimeObject* L_76 = (L_72)->GetAt(static_cast<il2cpp_array_size_t>(L_75));
			if (!L_76)
			{
				goto IL_01b5_1;
			}
		}
		{
			Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_77 = V_1;
			NullCheck(L_77);
			IEnumerator_1U5BU5D_tA2EF2FFDBA098C53558E3177C143C4DCFB0E5B88* L_78 = L_77->___EndOfFrameProcesses_82;
			ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 L_79 = V_2;
			int32_t L_80 = L_79.___i_1;
			NullCheck(L_78);
			int32_t L_81 = L_80;
			RuntimeObject* L_82 = (L_78)->GetAt(static_cast<il2cpp_array_size_t>(L_81));
			NullCheck(L_82);
			float L_83;
			L_83 = InterfaceFuncInvoker0< float >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var, L_82);
			bool L_84;
			L_84 = Single_IsNaN_m684B090AA2F895FD91821CA8684CBC11D784E4DD_inline(L_83, NULL);
			if (!L_84)
			{
				goto IL_01b5_1;
			}
		}
		{
			// if (ReplacementFunction != null)
			il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
			Func_3_tEDDCF49DDA3D0F9DCB147948F764E9514FC2EB04* L_85 = ((Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_StaticFields*)il2cpp_codegen_static_fields_for(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var))->___ReplacementFunction_19;
			if (!L_85)
			{
				goto IL_01a9_1;
			}
		}
		{
			// EndOfFrameProcesses[coindex.i] = ReplacementFunction(EndOfFrameProcesses[coindex.i], _indexToHandle[coindex]);
			Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_86 = V_1;
			NullCheck(L_86);
			IEnumerator_1U5BU5D_tA2EF2FFDBA098C53558E3177C143C4DCFB0E5B88* L_87 = L_86->___EndOfFrameProcesses_82;
			ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 L_88 = V_2;
			int32_t L_89 = L_88.___i_1;
			il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
			Func_3_tEDDCF49DDA3D0F9DCB147948F764E9514FC2EB04* L_90 = ((Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_StaticFields*)il2cpp_codegen_static_fields_for(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var))->___ReplacementFunction_19;
			Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_91 = V_1;
			NullCheck(L_91);
			IEnumerator_1U5BU5D_tA2EF2FFDBA098C53558E3177C143C4DCFB0E5B88* L_92 = L_91->___EndOfFrameProcesses_82;
			ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 L_93 = V_2;
			int32_t L_94 = L_93.___i_1;
			NullCheck(L_92);
			int32_t L_95 = L_94;
			RuntimeObject* L_96 = (L_92)->GetAt(static_cast<il2cpp_array_size_t>(L_95));
			Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_97 = V_1;
			NullCheck(L_97);
			Dictionary_2_t8C5B8A51DE825AB5FC3EDF04011A63CE33EBF2DB* L_98 = L_97->____indexToHandle_70;
			ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 L_99 = V_2;
			NullCheck(L_98);
			CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_100;
			L_100 = Dictionary_2_get_Item_mEE876E1A07892EE036FBCB85F5C96E283EADBEB7(L_98, L_99, Dictionary_2_get_Item_mEE876E1A07892EE036FBCB85F5C96E283EADBEB7_RuntimeMethod_var);
			NullCheck(L_90);
			RuntimeObject* L_101;
			L_101 = Func_3_Invoke_m449954D575F90EC3F3985278FB908B853AA03D7D_inline(L_90, L_96, L_100, NULL);
			NullCheck(L_87);
			ArrayElementTypeCheck (L_87, L_101);
			(L_87)->SetAt(static_cast<il2cpp_array_size_t>(L_89), (RuntimeObject*)L_101);
			// ReplacementFunction = null;
			((Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_StaticFields*)il2cpp_codegen_static_fields_for(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var))->___ReplacementFunction_19 = (Func_3_tEDDCF49DDA3D0F9DCB147948F764E9514FC2EB04*)NULL;
			Il2CppCodeGenWriteBarrier((void**)(&((Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_StaticFields*)il2cpp_codegen_static_fields_for(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var))->___ReplacementFunction_19), (void*)(Func_3_tEDDCF49DDA3D0F9DCB147948F764E9514FC2EB04*)NULL);
		}

IL_01a9_1:
		{
			// coindex.i--;
			int32_t* L_102 = (&(&V_2)->___i_1);
			int32_t* L_103 = L_102;
			int32_t L_104 = *((int32_t*)L_103);
			*((int32_t*)L_103) = (int32_t)((int32_t)il2cpp_codegen_subtract(L_104, 1));
		}

IL_01b5_1:
		{
			// if (ProfilerDebugAmount != DebugInfoType.None)
			Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_105 = V_1;
			NullCheck(L_105);
			int32_t L_106 = L_105->___ProfilerDebugAmount_5;
		}

IL_01bc_1:
		{
			// }
			goto IL_01d7;
		}
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
		{
			IL2CPP_PUSH_ACTIVE_EXCEPTION(e.ex);
			goto CATCH_01be;
		}
		throw e;
	}

CATCH_01be:
	{// begin catch(System.Exception)
		{
			// Debug.LogException(ex);
			Exception_t* L_107 = ((Exception_t*)IL2CPP_GET_ACTIVE_EXCEPTION(Exception_t*));
			il2cpp_codegen_runtime_class_init_inline(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var)));
			Debug_LogException_m82E44FEC6B03BC34AFC2CAF6583051570C60CB9E(L_107, NULL);
			// if (ex is MissingReferenceException)
			if (!((MissingReferenceException_t35FD5FA7551453057398F69DC338ADBC09B7581B*)IsInstClass((RuntimeObject*)L_107, ((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&MissingReferenceException_t35FD5FA7551453057398F69DC338ADBC09B7581B_il2cpp_TypeInfo_var)))))
			{
				goto IL_01d5;
			}
		}
		{
			// Debug.LogError("This exception can probably be fixed by adding \"CancelWith(gameObject)\" when you run the coroutine.\n"
			//     + "Example: Timing.RunCoroutine(_foo().CancelWith(gameObject), Segment.EndOfFrame);");
			il2cpp_codegen_runtime_class_init_inline(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var)));
			Debug_LogError_m059825802BB6AF7EA9693FEBEEB0D85F59A3E38E(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral37407E1CCD62BFCCE0980D9087E09AE63E8534D2)), NULL);
		}

IL_01d5:
		{
			// }
			IL2CPP_POP_ACTIVE_EXCEPTION();
			goto IL_01d7;
		}
	}// end catch (depth: 1)

IL_01d7:
	{
		// for (coindex.i = 0; coindex.i < _lastEndOfFrameProcessSlot; coindex.i++)
		int32_t* L_108 = (&(&V_2)->___i_1);
		int32_t* L_109 = L_108;
		int32_t L_110 = *((int32_t*)L_109);
		*((int32_t*)L_109) = (int32_t)((int32_t)il2cpp_codegen_add(L_110, 1));
	}

IL_01e3:
	{
		// for (coindex.i = 0; coindex.i < _lastEndOfFrameProcessSlot; coindex.i++)
		ProcessIndex_t86667E9CDAE03ABEE2457984988152D75E408369 L_111 = V_2;
		int32_t L_112 = L_111.___i_1;
		Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_113 = V_1;
		NullCheck(L_113);
		int32_t L_114 = L_113->____lastEndOfFrameProcessSlot_48;
		if ((((int32_t)L_112) < ((int32_t)L_114)))
		{
			goto IL_008f;
		}
	}

IL_01f4:
	{
		// while (_nextEndOfFrameProcessSlot > 0)
		Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_115 = V_1;
		NullCheck(L_115);
		int32_t L_116 = L_115->____nextEndOfFrameProcessSlot_41;
		if ((((int32_t)L_116) > ((int32_t)0)))
		{
			goto IL_0023;
		}
	}
	{
		// currentCoroutine = default(CoroutineHandle);
		Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_117 = V_1;
		il2cpp_codegen_initobj((&V_4), sizeof(CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772));
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_118 = V_4;
		NullCheck(L_117);
		Timing_set_currentCoroutine_m3DBBE23CAA1674B5622501475023ECCF1BFAD433_inline(L_117, L_118, NULL);
		// }
		return (bool)0;
	}
}
// System.Object MEC.Timing/<_EOFPump>d__133::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3C_EOFPumpU3Ed__133_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3DCB2032ECDFC4E71C2353EBB50B0E45524125AB (U3C_EOFPumpU3Ed__133_tE02D8D1E07F743410384C7B9F487FC45070B6C01* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void MEC.Timing/<_EOFPump>d__133::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_EOFPumpU3Ed__133_System_Collections_IEnumerator_Reset_mA584345D587C9DDB8394E095ACC4311435B8CFCC (U3C_EOFPumpU3Ed__133_tE02D8D1E07F743410384C7B9F487FC45070B6C01* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3C_EOFPumpU3Ed__133_System_Collections_IEnumerator_Reset_mA584345D587C9DDB8394E095ACC4311435B8CFCC_RuntimeMethod_var)));
	}
}
// System.Object MEC.Timing/<_EOFPump>d__133::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3C_EOFPumpU3Ed__133_System_Collections_IEnumerator_get_Current_m3D40A46B62B52A7E14E4619998781B0541DACBCE (U3C_EOFPumpU3Ed__133_tE02D8D1E07F743410384C7B9F487FC45070B6C01* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MEC.Timing/<_StartWhenDone>d__275::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_StartWhenDoneU3Ed__275__ctor_m3CBE998E01F318F0D0122D7DC7134345EF73FDC3 (U3C_StartWhenDoneU3Ed__275_t27CB96517B4094CEC4B93433D970071A204798F8* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void MEC.Timing/<_StartWhenDone>d__275::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_StartWhenDoneU3Ed__275_System_IDisposable_Dispose_mB8C546FB49C07B7D67ABC7DCC0507B784F4457A1 (U3C_StartWhenDoneU3Ed__275_t27CB96517B4094CEC4B93433D970071A204798F8* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)-3))))
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)((int32_t)il2cpp_codegen_subtract(L_2, 1))) <= ((uint32_t)1))))
		{
			goto IL_001c;
		}
	}

IL_0012:
	{
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0015:
			{// begin finally (depth: 1)
				U3C_StartWhenDoneU3Ed__275_U3CU3Em__Finally1_m9A933C8961DBAE12BCBB9FCAF18A9A5DF0D4717D(__this, NULL);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			goto IL_001c;
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_001c:
	{
		return;
	}
}
// System.Boolean MEC.Timing/<_StartWhenDone>d__275::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3C_StartWhenDoneU3Ed__275_MoveNext_m2A711A7923BDBC0ABC7B619093848EC6464F0546 (U3C_StartWhenDoneU3Ed__275_t27CB96517B4094CEC4B93433D970071A204798F8* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_m8466E7777FED2755A8F5EF371415CBCF59589873_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* V_2 = NULL;
	{
		auto __finallyBlock = il2cpp::utils::Fault([&]
		{

FAULT_00c4:
			{// begin fault (depth: 1)
				U3C_StartWhenDoneU3Ed__275_System_IDisposable_Dispose_mB8C546FB49C07B7D67ABC7DCC0507B784F4457A1(__this, NULL);
				return;
			}// end fault
		});
		try
		{// begin try (depth: 1)
			{
				int32_t L_0 = __this->___U3CU3E1__state_0;
				V_1 = L_0;
				Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_1 = __this->___U3CU3E4__this_2;
				V_2 = L_1;
				int32_t L_2 = V_1;
				switch (L_2)
				{
					case 0:
					{
						goto IL_0027_1;
					}
					case 1:
					{
						goto IL_007f_1;
					}
					case 2:
					{
						goto IL_00a5_1;
					}
				}
			}
			{
				V_0 = (bool)0;
				goto IL_00cb;
			}

IL_0027_1:
			{
				__this->___U3CU3E1__state_0 = (-1);
				// if (!_waitingTriggers.ContainsKey(handle)) yield break;
				Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_3 = V_2;
				NullCheck(L_3);
				Dictionary_2_t586C869F4A815C42AD5BEFA81A6BB784048C02BE* L_4 = L_3->____waitingTriggers_67;
				CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_5 = __this->___handle_3;
				NullCheck(L_4);
				bool L_6;
				L_6 = Dictionary_2_ContainsKey_m8466E7777FED2755A8F5EF371415CBCF59589873(L_4, L_5, Dictionary_2_ContainsKey_m8466E7777FED2755A8F5EF371415CBCF59589873_RuntimeMethod_var);
				if (L_6)
				{
					goto IL_0048_1;
				}
			}
			{
				// if (!_waitingTriggers.ContainsKey(handle)) yield break;
				V_0 = (bool)0;
				goto IL_00cb;
			}

IL_0048_1:
			{
				__this->___U3CU3E1__state_0 = ((int32_t)-3);
				// if (proc.Current > localTime)
				RuntimeObject* L_7 = __this->___proc_4;
				NullCheck(L_7);
				float L_8;
				L_8 = InterfaceFuncInvoker0< float >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var, L_7);
				Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_9 = V_2;
				NullCheck(L_9);
				float L_10 = L_9->___localTime_16;
				if ((!(((float)L_8) > ((float)L_10))))
				{
					goto IL_00ad_1;
				}
			}
			{
				// yield return proc.Current;
				RuntimeObject* L_11 = __this->___proc_4;
				NullCheck(L_11);
				float L_12;
				L_12 = InterfaceFuncInvoker0< float >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var, L_11);
				__this->___U3CU3E2__current_1 = L_12;
				__this->___U3CU3E1__state_0 = 1;
				V_0 = (bool)1;
				goto IL_00cb;
			}

IL_007f_1:
			{
				__this->___U3CU3E1__state_0 = ((int32_t)-3);
				goto IL_00ad_1;
			}

IL_0089_1:
			{
				// yield return proc.Current;
				RuntimeObject* L_13 = __this->___proc_4;
				NullCheck(L_13);
				float L_14;
				L_14 = InterfaceFuncInvoker0< float >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var, L_13);
				__this->___U3CU3E2__current_1 = L_14;
				__this->___U3CU3E1__state_0 = 2;
				V_0 = (bool)1;
				goto IL_00cb;
			}

IL_00a5_1:
			{
				__this->___U3CU3E1__state_0 = ((int32_t)-3);
			}

IL_00ad_1:
			{
				// while (proc.MoveNext())
				RuntimeObject* L_15 = __this->___proc_4;
				NullCheck(L_15);
				bool L_16;
				L_16 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_15);
				if (L_16)
				{
					goto IL_0089_1;
				}
			}
			{
				// }
				U3C_StartWhenDoneU3Ed__275_U3CU3Em__Finally1_m9A933C8961DBAE12BCBB9FCAF18A9A5DF0D4717D(__this, NULL);
				// }
				V_0 = (bool)0;
				goto IL_00cb;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_00cb:
	{
		bool L_17 = V_0;
		return L_17;
	}
}
// System.Void MEC.Timing/<_StartWhenDone>d__275::<>m__Finally1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_StartWhenDoneU3Ed__275_U3CU3Em__Finally1_m9A933C8961DBAE12BCBB9FCAF18A9A5DF0D4717D (U3C_StartWhenDoneU3Ed__275_t27CB96517B4094CEC4B93433D970071A204798F8* __this, const RuntimeMethod* method) 
{
	{
		__this->___U3CU3E1__state_0 = (-1);
		Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_0 = __this->___U3CU3E4__this_2;
		// CloseWaitingProcess(handle);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_1 = __this->___handle_3;
		NullCheck(L_0);
		Timing_CloseWaitingProcess_m15BC8AD80A4856793BCC789091F397889DA35056(L_0, L_1, NULL);
		// }
		return;
	}
}
// System.Single MEC.Timing/<_StartWhenDone>d__275::System.Collections.Generic.IEnumerator<System.Single>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float U3C_StartWhenDoneU3Ed__275_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_mD86F629C66D93002C1B65788E0C3A48FC796B05F (U3C_StartWhenDoneU3Ed__275_t27CB96517B4094CEC4B93433D970071A204798F8* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void MEC.Timing/<_StartWhenDone>d__275::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_StartWhenDoneU3Ed__275_System_Collections_IEnumerator_Reset_m0825ACABC6908F6EC451DA4F80879DFC4F1026B1 (U3C_StartWhenDoneU3Ed__275_t27CB96517B4094CEC4B93433D970071A204798F8* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3C_StartWhenDoneU3Ed__275_System_Collections_IEnumerator_Reset_m0825ACABC6908F6EC451DA4F80879DFC4F1026B1_RuntimeMethod_var)));
	}
}
// System.Object MEC.Timing/<_StartWhenDone>d__275::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3C_StartWhenDoneU3Ed__275_System_Collections_IEnumerator_get_Current_m85A9FA517F98CE3FD201C9FD319EE30B56C5C3A6 (U3C_StartWhenDoneU3Ed__275_t27CB96517B4094CEC4B93433D970071A204798F8* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->___U3CU3E2__current_1;
		float L_1 = L_0;
		RuntimeObject* L_2 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MEC.Timing/<_StartWhenDone>d__280::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_StartWhenDoneU3Ed__280__ctor_m6C0D9F177DD22EED6325FB57CC52DA1FD8867C76 (U3C_StartWhenDoneU3Ed__280_t97BCE744B518341C8288ADDDBFAB856E7277142A* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void MEC.Timing/<_StartWhenDone>d__280::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_StartWhenDoneU3Ed__280_System_IDisposable_Dispose_m4EB2C1B62EE10EEBD965661808F6B5AC82C3F11C (U3C_StartWhenDoneU3Ed__280_t97BCE744B518341C8288ADDDBFAB856E7277142A* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean MEC.Timing/<_StartWhenDone>d__280::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3C_StartWhenDoneU3Ed__280_MoveNext_m6BEDB02C0A21C82F4BB0C553245AF5E222576934 (U3C_StartWhenDoneU3Ed__280_t97BCE744B518341C8288ADDDBFAB856E7277142A* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_3_tEDDCF49DDA3D0F9DCB147948F764E9514FC2EB04_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_ReturnTmpRefForRepFunc_m1FEA4D102C9DD648C661C9A6503443AF9015CDAC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001b;
			}
			case 1:
			{
				goto IL_0038;
			}
			case 2:
			{
				goto IL_007c;
			}
		}
	}
	{
		return (bool)0;
	}

IL_001b:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_003f;
	}

IL_0024:
	{
		// yield return WaitForOneFrame;
		__this->___U3CU3E2__current_1 = (-std::numeric_limits<float>::infinity());
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0038:
	{
		__this->___U3CU3E1__state_0 = (-1);
	}

IL_003f:
	{
		// while (!operation.isDone)
		AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C* L_2 = __this->___operation_2;
		NullCheck(L_2);
		bool L_3;
		L_3 = AsyncOperation_get_isDone_m68A0682777E2132FC033182E9F50303566AA354D(L_2, NULL);
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		// _tmpRef = pausedProc;
		RuntimeObject* L_4 = __this->___pausedProc_3;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		((Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_StaticFields*)il2cpp_codegen_static_fields_for(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var))->____tmpRef_24 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&((Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_StaticFields*)il2cpp_codegen_static_fields_for(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var))->____tmpRef_24), (void*)L_4);
		// ReplacementFunction = ReturnTmpRefForRepFunc;
		Func_3_tEDDCF49DDA3D0F9DCB147948F764E9514FC2EB04* L_5 = (Func_3_tEDDCF49DDA3D0F9DCB147948F764E9514FC2EB04*)il2cpp_codegen_object_new(Func_3_tEDDCF49DDA3D0F9DCB147948F764E9514FC2EB04_il2cpp_TypeInfo_var);
		NullCheck(L_5);
		Func_3__ctor_mF85581DC03C4A720CCD77E9CCD0407A9B1130A14(L_5, NULL, (intptr_t)((void*)Timing_ReturnTmpRefForRepFunc_m1FEA4D102C9DD648C661C9A6503443AF9015CDAC_RuntimeMethod_var), NULL);
		((Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_StaticFields*)il2cpp_codegen_static_fields_for(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var))->___ReplacementFunction_19 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&((Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_StaticFields*)il2cpp_codegen_static_fields_for(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var))->___ReplacementFunction_19), (void*)L_5);
		// yield return float.NaN;
		__this->___U3CU3E2__current_1 = (std::numeric_limits<float>::quiet_NaN());
		__this->___U3CU3E1__state_0 = 2;
		return (bool)1;
	}

IL_007c:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// }
		return (bool)0;
	}
}
// System.Single MEC.Timing/<_StartWhenDone>d__280::System.Collections.Generic.IEnumerator<System.Single>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float U3C_StartWhenDoneU3Ed__280_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_mFCC3614CC493DBC18FB8CE9D6D2E12F67EAE8DA8 (U3C_StartWhenDoneU3Ed__280_t97BCE744B518341C8288ADDDBFAB856E7277142A* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void MEC.Timing/<_StartWhenDone>d__280::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_StartWhenDoneU3Ed__280_System_Collections_IEnumerator_Reset_mE155E978CC6C00E9C68D941549CAD5BF05E8570D (U3C_StartWhenDoneU3Ed__280_t97BCE744B518341C8288ADDDBFAB856E7277142A* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3C_StartWhenDoneU3Ed__280_System_Collections_IEnumerator_Reset_mE155E978CC6C00E9C68D941549CAD5BF05E8570D_RuntimeMethod_var)));
	}
}
// System.Object MEC.Timing/<_StartWhenDone>d__280::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3C_StartWhenDoneU3Ed__280_System_Collections_IEnumerator_get_Current_m7948F0767FBE9E6F0DB316D4F6B6D0EB1000A723 (U3C_StartWhenDoneU3Ed__280_t97BCE744B518341C8288ADDDBFAB856E7277142A* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->___U3CU3E2__current_1;
		float L_1 = L_0;
		RuntimeObject* L_2 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MEC.Timing/<_StartWhenDone>d__282::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_StartWhenDoneU3Ed__282__ctor_m323AD40CE7AD7071A5D019F96E01189B3A7C1FE4 (U3C_StartWhenDoneU3Ed__282_tE9D05D0D91E0AA510B531696B3373824F8299163* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void MEC.Timing/<_StartWhenDone>d__282::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_StartWhenDoneU3Ed__282_System_IDisposable_Dispose_m799ACB0647019CAF5FF403DDCBB60168ADE2BB89 (U3C_StartWhenDoneU3Ed__282_tE9D05D0D91E0AA510B531696B3373824F8299163* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean MEC.Timing/<_StartWhenDone>d__282::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3C_StartWhenDoneU3Ed__282_MoveNext_m1AF48F9925B6C2D3A7AAFE493D51686F2C6E99A0 (U3C_StartWhenDoneU3Ed__282_tE9D05D0D91E0AA510B531696B3373824F8299163* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_3_tEDDCF49DDA3D0F9DCB147948F764E9514FC2EB04_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_ReturnTmpRefForRepFunc_m1FEA4D102C9DD648C661C9A6503443AF9015CDAC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001b;
			}
			case 1:
			{
				goto IL_0038;
			}
			case 2:
			{
				goto IL_007c;
			}
		}
	}
	{
		return (bool)0;
	}

IL_001b:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_003f;
	}

IL_0024:
	{
		// yield return WaitForOneFrame;
		__this->___U3CU3E2__current_1 = (-std::numeric_limits<float>::infinity());
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0038:
	{
		__this->___U3CU3E1__state_0 = (-1);
	}

IL_003f:
	{
		// while (operation.keepWaiting)
		CustomYieldInstruction_t6B81A50D5D210C1ACAAE247FB53B65CDFFEB7617* L_2 = __this->___operation_2;
		NullCheck(L_2);
		bool L_3;
		L_3 = VirtualFuncInvoker0< bool >::Invoke(7 /* System.Boolean UnityEngine.CustomYieldInstruction::get_keepWaiting() */, L_2);
		if (L_3)
		{
			goto IL_0024;
		}
	}
	{
		// _tmpRef = pausedProc;
		RuntimeObject* L_4 = __this->___pausedProc_3;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		((Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_StaticFields*)il2cpp_codegen_static_fields_for(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var))->____tmpRef_24 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&((Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_StaticFields*)il2cpp_codegen_static_fields_for(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var))->____tmpRef_24), (void*)L_4);
		// ReplacementFunction = ReturnTmpRefForRepFunc;
		Func_3_tEDDCF49DDA3D0F9DCB147948F764E9514FC2EB04* L_5 = (Func_3_tEDDCF49DDA3D0F9DCB147948F764E9514FC2EB04*)il2cpp_codegen_object_new(Func_3_tEDDCF49DDA3D0F9DCB147948F764E9514FC2EB04_il2cpp_TypeInfo_var);
		NullCheck(L_5);
		Func_3__ctor_mF85581DC03C4A720CCD77E9CCD0407A9B1130A14(L_5, NULL, (intptr_t)((void*)Timing_ReturnTmpRefForRepFunc_m1FEA4D102C9DD648C661C9A6503443AF9015CDAC_RuntimeMethod_var), NULL);
		((Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_StaticFields*)il2cpp_codegen_static_fields_for(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var))->___ReplacementFunction_19 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&((Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_StaticFields*)il2cpp_codegen_static_fields_for(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var))->___ReplacementFunction_19), (void*)L_5);
		// yield return float.NaN;
		__this->___U3CU3E2__current_1 = (std::numeric_limits<float>::quiet_NaN());
		__this->___U3CU3E1__state_0 = 2;
		return (bool)1;
	}

IL_007c:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// }
		return (bool)0;
	}
}
// System.Single MEC.Timing/<_StartWhenDone>d__282::System.Collections.Generic.IEnumerator<System.Single>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float U3C_StartWhenDoneU3Ed__282_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_mE06C069EF03481D692042813AF71D2D7113E8901 (U3C_StartWhenDoneU3Ed__282_tE9D05D0D91E0AA510B531696B3373824F8299163* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void MEC.Timing/<_StartWhenDone>d__282::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_StartWhenDoneU3Ed__282_System_Collections_IEnumerator_Reset_m44F075C9EFBD9182513D3490235D7DC582C44447 (U3C_StartWhenDoneU3Ed__282_tE9D05D0D91E0AA510B531696B3373824F8299163* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3C_StartWhenDoneU3Ed__282_System_Collections_IEnumerator_Reset_m44F075C9EFBD9182513D3490235D7DC582C44447_RuntimeMethod_var)));
	}
}
// System.Object MEC.Timing/<_StartWhenDone>d__282::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3C_StartWhenDoneU3Ed__282_System_Collections_IEnumerator_get_Current_m12C16C99A17CE40081FC1DC16D334407C110A21D (U3C_StartWhenDoneU3Ed__282_tE9D05D0D91E0AA510B531696B3373824F8299163* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->___U3CU3E2__current_1;
		float L_1 = L_0;
		RuntimeObject* L_2 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MEC.Timing/<_StartWhenDone>d__287::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_StartWhenDoneU3Ed__287__ctor_mBDCF0CCD737AF1305C0AE4070EFD6F656DFA26AC (U3C_StartWhenDoneU3Ed__287_tAE4B8324916A8A4250BF3343E47E25811AF2D40F* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void MEC.Timing/<_StartWhenDone>d__287::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_StartWhenDoneU3Ed__287_System_IDisposable_Dispose_mE13A3DD773919A03BEFFD235C677B0D47247BD4E (U3C_StartWhenDoneU3Ed__287_tAE4B8324916A8A4250BF3343E47E25811AF2D40F* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean MEC.Timing/<_StartWhenDone>d__287::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3C_StartWhenDoneU3Ed__287_MoveNext_m3F33BF22DFD1B0C0EE96592F973D181366B845D6 (U3C_StartWhenDoneU3Ed__287_tAE4B8324916A8A4250BF3343E47E25811AF2D40F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_3_tEDDCF49DDA3D0F9DCB147948F764E9514FC2EB04_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_ReturnTmpRefForRepFunc_m1FEA4D102C9DD648C661C9A6503443AF9015CDAC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001b;
			}
			case 1:
			{
				goto IL_0038;
			}
			case 2:
			{
				goto IL_0082;
			}
		}
	}
	{
		return (bool)0;
	}

IL_001b:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_003f;
	}

IL_0024:
	{
		// yield return WaitForOneFrame;
		__this->___U3CU3E2__current_1 = (-std::numeric_limits<float>::infinity());
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0038:
	{
		__this->___U3CU3E1__state_0 = (-1);
	}

IL_003f:
	{
		// while (evaluatorFunc() == continueOn)
		Func_1_t2BE7F58348C9CC544A8973B3A9E55541DE43C457* L_2 = __this->___evaluatorFunc_2;
		NullCheck(L_2);
		bool L_3;
		L_3 = Func_1_Invoke_mBB7F37C468451AF57FAF31635C544D6B8C4373B2_inline(L_2, NULL);
		bool L_4 = __this->___continueOn_3;
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0024;
		}
	}
	{
		// _tmpRef = pausedProc;
		RuntimeObject* L_5 = __this->___pausedProc_4;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		((Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_StaticFields*)il2cpp_codegen_static_fields_for(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var))->____tmpRef_24 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&((Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_StaticFields*)il2cpp_codegen_static_fields_for(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var))->____tmpRef_24), (void*)L_5);
		// ReplacementFunction = ReturnTmpRefForRepFunc;
		Func_3_tEDDCF49DDA3D0F9DCB147948F764E9514FC2EB04* L_6 = (Func_3_tEDDCF49DDA3D0F9DCB147948F764E9514FC2EB04*)il2cpp_codegen_object_new(Func_3_tEDDCF49DDA3D0F9DCB147948F764E9514FC2EB04_il2cpp_TypeInfo_var);
		NullCheck(L_6);
		Func_3__ctor_mF85581DC03C4A720CCD77E9CCD0407A9B1130A14(L_6, NULL, (intptr_t)((void*)Timing_ReturnTmpRefForRepFunc_m1FEA4D102C9DD648C661C9A6503443AF9015CDAC_RuntimeMethod_var), NULL);
		((Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_StaticFields*)il2cpp_codegen_static_fields_for(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var))->___ReplacementFunction_19 = L_6;
		Il2CppCodeGenWriteBarrier((void**)(&((Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_StaticFields*)il2cpp_codegen_static_fields_for(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var))->___ReplacementFunction_19), (void*)L_6);
		// yield return float.NaN;
		__this->___U3CU3E2__current_1 = (std::numeric_limits<float>::quiet_NaN());
		__this->___U3CU3E1__state_0 = 2;
		return (bool)1;
	}

IL_0082:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// }
		return (bool)0;
	}
}
// System.Single MEC.Timing/<_StartWhenDone>d__287::System.Collections.Generic.IEnumerator<System.Single>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float U3C_StartWhenDoneU3Ed__287_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_m2F2A16EF3342036689D6FE52C84E4A2108EAEDA1 (U3C_StartWhenDoneU3Ed__287_tAE4B8324916A8A4250BF3343E47E25811AF2D40F* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void MEC.Timing/<_StartWhenDone>d__287::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_StartWhenDoneU3Ed__287_System_Collections_IEnumerator_Reset_m4D9AC2A778EF63FFBA8B1AD1B65B59A7A7E59A19 (U3C_StartWhenDoneU3Ed__287_tAE4B8324916A8A4250BF3343E47E25811AF2D40F* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3C_StartWhenDoneU3Ed__287_System_Collections_IEnumerator_Reset_m4D9AC2A778EF63FFBA8B1AD1B65B59A7A7E59A19_RuntimeMethod_var)));
	}
}
// System.Object MEC.Timing/<_StartWhenDone>d__287::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3C_StartWhenDoneU3Ed__287_System_Collections_IEnumerator_get_Current_mF3E9DE0DCD097D3C30E61DA05BABA6329575050B (U3C_StartWhenDoneU3Ed__287_tAE4B8324916A8A4250BF3343E47E25811AF2D40F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->___U3CU3E2__current_1;
		float L_1 = L_0;
		RuntimeObject* L_2 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MEC.Timing/<_InjectDelay>d__288::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_InjectDelayU3Ed__288__ctor_mD51610FAA3B928048CDDBBB4B93A8A3BDD6DBF51 (U3C_InjectDelayU3Ed__288_t68BAE838FFF0D6A5D77DD945629395CB307A7C47* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void MEC.Timing/<_InjectDelay>d__288::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_InjectDelayU3Ed__288_System_IDisposable_Dispose_m1D3CEC7BB9B806B1F5C17743360061D66321AD1B (U3C_InjectDelayU3Ed__288_t68BAE838FFF0D6A5D77DD945629395CB307A7C47* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean MEC.Timing/<_InjectDelay>d__288::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3C_InjectDelayU3Ed__288_MoveNext_m51B0BED287403A8585D7EEA7433DBF7517861FFD (U3C_InjectDelayU3Ed__288_t68BAE838FFF0D6A5D77DD945629395CB307A7C47* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_3_tEDDCF49DDA3D0F9DCB147948F764E9514FC2EB04_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_ReturnTmpRefForRepFunc_m1FEA4D102C9DD648C661C9A6503443AF9015CDAC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* V_1 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_1 = __this->___U3CU3E4__this_2;
		V_1 = L_1;
		int32_t L_2 = V_0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_0022;
			}
			case 1:
			{
				goto IL_0044;
			}
			case 2:
			{
				goto IL_007b;
			}
		}
	}
	{
		return (bool)0;
	}

IL_0022:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// yield return WaitForSecondsOnInstance(waitTime);
		Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_3 = V_1;
		float L_4 = __this->___waitTime_3;
		NullCheck(L_3);
		float L_5;
		L_5 = Timing_WaitForSecondsOnInstance_m739CE37954128787A9E525694F27B727342F52ED(L_3, L_4, NULL);
		__this->___U3CU3E2__current_1 = L_5;
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0044:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// _tmpRef = proc;
		RuntimeObject* L_6 = __this->___proc_4;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		((Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_StaticFields*)il2cpp_codegen_static_fields_for(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var))->____tmpRef_24 = L_6;
		Il2CppCodeGenWriteBarrier((void**)(&((Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_StaticFields*)il2cpp_codegen_static_fields_for(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var))->____tmpRef_24), (void*)L_6);
		// ReplacementFunction = ReturnTmpRefForRepFunc;
		Func_3_tEDDCF49DDA3D0F9DCB147948F764E9514FC2EB04* L_7 = (Func_3_tEDDCF49DDA3D0F9DCB147948F764E9514FC2EB04*)il2cpp_codegen_object_new(Func_3_tEDDCF49DDA3D0F9DCB147948F764E9514FC2EB04_il2cpp_TypeInfo_var);
		NullCheck(L_7);
		Func_3__ctor_mF85581DC03C4A720CCD77E9CCD0407A9B1130A14(L_7, NULL, (intptr_t)((void*)Timing_ReturnTmpRefForRepFunc_m1FEA4D102C9DD648C661C9A6503443AF9015CDAC_RuntimeMethod_var), NULL);
		((Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_StaticFields*)il2cpp_codegen_static_fields_for(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var))->___ReplacementFunction_19 = L_7;
		Il2CppCodeGenWriteBarrier((void**)(&((Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_StaticFields*)il2cpp_codegen_static_fields_for(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var))->___ReplacementFunction_19), (void*)L_7);
		// yield return float.NaN;
		__this->___U3CU3E2__current_1 = (std::numeric_limits<float>::quiet_NaN());
		__this->___U3CU3E1__state_0 = 2;
		return (bool)1;
	}

IL_007b:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// }
		return (bool)0;
	}
}
// System.Single MEC.Timing/<_InjectDelay>d__288::System.Collections.Generic.IEnumerator<System.Single>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float U3C_InjectDelayU3Ed__288_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_m9EF765C734779B525F70B40C03361892173EAE41 (U3C_InjectDelayU3Ed__288_t68BAE838FFF0D6A5D77DD945629395CB307A7C47* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void MEC.Timing/<_InjectDelay>d__288::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_InjectDelayU3Ed__288_System_Collections_IEnumerator_Reset_mB7DD920810F96F2A2299354FB8CA75032B9860B1 (U3C_InjectDelayU3Ed__288_t68BAE838FFF0D6A5D77DD945629395CB307A7C47* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3C_InjectDelayU3Ed__288_System_Collections_IEnumerator_Reset_mB7DD920810F96F2A2299354FB8CA75032B9860B1_RuntimeMethod_var)));
	}
}
// System.Object MEC.Timing/<_InjectDelay>d__288::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3C_InjectDelayU3Ed__288_System_Collections_IEnumerator_get_Current_m4CB95E0589E3364D0C4B64066E5FC2529E6A0496 (U3C_InjectDelayU3Ed__288_t68BAE838FFF0D6A5D77DD945629395CB307A7C47* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->___U3CU3E2__current_1;
		float L_1 = L_0;
		RuntimeObject* L_2 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MEC.Timing/<_DelayedCall>d__313::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_DelayedCallU3Ed__313__ctor_m37D3DF24EC83A2A3FF4830AB29435ECB6B5CDF89 (U3C_DelayedCallU3Ed__313_tBF4CBB957951173CC872155112E8791D838F1FBF* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void MEC.Timing/<_DelayedCall>d__313::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_DelayedCallU3Ed__313_System_IDisposable_Dispose_m1F57F26C605422505A10CF010689532970E73CEF (U3C_DelayedCallU3Ed__313_tBF4CBB957951173CC872155112E8791D838F1FBF* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean MEC.Timing/<_DelayedCall>d__313::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3C_DelayedCallU3Ed__313_MoveNext_m91EB800644BC0EF6A702EBB08E97FA78158CAF67 (U3C_DelayedCallU3Ed__313_tBF4CBB957951173CC872155112E8791D838F1FBF* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* V_1 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_1 = __this->___U3CU3E4__this_2;
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0039;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// yield return WaitForSecondsOnInstance(delay);
		Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_4 = V_1;
		float L_5 = __this->___delay_3;
		NullCheck(L_4);
		float L_6;
		L_6 = Timing_WaitForSecondsOnInstance_m739CE37954128787A9E525694F27B727342F52ED(L_4, L_5, NULL);
		__this->___U3CU3E2__current_1 = L_6;
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0039:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// if (ReferenceEquals(cancelWith, null) || cancelWith != null)
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7 = __this->___cancelWith_4;
		if (!L_7)
		{
			goto IL_0056;
		}
	}
	{
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_8 = __this->___cancelWith_4;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_9;
		L_9 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_8, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_9)
		{
			goto IL_0061;
		}
	}

IL_0056:
	{
		// action();
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_10 = __this->___action_5;
		NullCheck(L_10);
		Action_Invoke_m7126A54DACA72B845424072887B5F3A51FC3808E_inline(L_10, NULL);
	}

IL_0061:
	{
		// }
		return (bool)0;
	}
}
// System.Single MEC.Timing/<_DelayedCall>d__313::System.Collections.Generic.IEnumerator<System.Single>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float U3C_DelayedCallU3Ed__313_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_mD908618131793BCAF6866B7036D48C41976D2F89 (U3C_DelayedCallU3Ed__313_tBF4CBB957951173CC872155112E8791D838F1FBF* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void MEC.Timing/<_DelayedCall>d__313::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_DelayedCallU3Ed__313_System_Collections_IEnumerator_Reset_m9223F0C17129D85FAE544A3A67C6E4F831049243 (U3C_DelayedCallU3Ed__313_tBF4CBB957951173CC872155112E8791D838F1FBF* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3C_DelayedCallU3Ed__313_System_Collections_IEnumerator_Reset_m9223F0C17129D85FAE544A3A67C6E4F831049243_RuntimeMethod_var)));
	}
}
// System.Object MEC.Timing/<_DelayedCall>d__313::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3C_DelayedCallU3Ed__313_System_Collections_IEnumerator_get_Current_mCA6D5EAD47F91D7FFD55910686BA01B53B969745 (U3C_DelayedCallU3Ed__313_tBF4CBB957951173CC872155112E8791D838F1FBF* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->___U3CU3E2__current_1;
		float L_1 = L_0;
		RuntimeObject* L_2 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MEC.Timing/<_WatchCall>d__334::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_WatchCallU3Ed__334__ctor_m72A499F30E92C0979A6DFD3D52091346A0673EA7 (U3C_WatchCallU3Ed__334_t7772F9C12997D6A2EF678E1FBD5FF42FC86C41AA* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void MEC.Timing/<_WatchCall>d__334::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_WatchCallU3Ed__334_System_IDisposable_Dispose_m55F53BBBF23B635E2A710C51CC9B88B36FDFD823 (U3C_WatchCallU3Ed__334_t7772F9C12997D6A2EF678E1FBD5FF42FC86C41AA* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean MEC.Timing/<_WatchCall>d__334::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3C_WatchCallU3Ed__334_MoveNext_m4675FB0A78D01BBD2087F526F045669B2539D8DF (U3C_WatchCallU3Ed__334_t7772F9C12997D6A2EF678E1FBD5FF42FC86C41AA* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* V_1 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_1 = __this->___U3CU3E4__this_2;
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0039;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// yield return WaitForSecondsOnInstance(timeframe);
		Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_4 = V_1;
		float L_5 = __this->___timeframe_3;
		NullCheck(L_4);
		float L_6;
		L_6 = Timing_WaitForSecondsOnInstance_m739CE37954128787A9E525694F27B727342F52ED(L_4, L_5, NULL);
		__this->___U3CU3E2__current_1 = L_6;
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0039:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// KillCoroutinesOnInstance(handle);
		Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_7 = V_1;
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_8 = __this->___handle_4;
		NullCheck(L_7);
		int32_t L_9;
		L_9 = Timing_KillCoroutinesOnInstance_m2FBCFCA895C9A7EF1AB2FD59D13503CDA9D69844(L_7, L_8, NULL);
		// if (onDone != null && (ReferenceEquals(gObject, null) || gObject != null))
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_10 = __this->___onDone_5;
		if (!L_10)
		{
			goto IL_0076;
		}
	}
	{
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_11 = __this->___gObject_6;
		if (!L_11)
		{
			goto IL_006b;
		}
	}
	{
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_12 = __this->___gObject_6;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_13;
		L_13 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_12, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_13)
		{
			goto IL_0076;
		}
	}

IL_006b:
	{
		// onDone();
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_14 = __this->___onDone_5;
		NullCheck(L_14);
		Action_Invoke_m7126A54DACA72B845424072887B5F3A51FC3808E_inline(L_14, NULL);
	}

IL_0076:
	{
		// }
		return (bool)0;
	}
}
// System.Single MEC.Timing/<_WatchCall>d__334::System.Collections.Generic.IEnumerator<System.Single>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float U3C_WatchCallU3Ed__334_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_m73C0231BDE0B014A1E5932E5EEDC9BFC4F6CD9C7 (U3C_WatchCallU3Ed__334_t7772F9C12997D6A2EF678E1FBD5FF42FC86C41AA* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void MEC.Timing/<_WatchCall>d__334::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_WatchCallU3Ed__334_System_Collections_IEnumerator_Reset_m6E5B1313F721247D6775473FDC048DAABDDB1620 (U3C_WatchCallU3Ed__334_t7772F9C12997D6A2EF678E1FBD5FF42FC86C41AA* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3C_WatchCallU3Ed__334_System_Collections_IEnumerator_Reset_m6E5B1313F721247D6775473FDC048DAABDDB1620_RuntimeMethod_var)));
	}
}
// System.Object MEC.Timing/<_WatchCall>d__334::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3C_WatchCallU3Ed__334_System_Collections_IEnumerator_get_Current_mC25AF8382294043CE6433811D28E0A15FCED7EFC (U3C_WatchCallU3Ed__334_t7772F9C12997D6A2EF678E1FBD5FF42FC86C41AA* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->___U3CU3E2__current_1;
		float L_1 = L_0;
		RuntimeObject* L_2 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MEC.Timing/<_CallContinuously>d__335::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_CallContinuouslyU3Ed__335__ctor_m771BB432DE4A81C3BA9C909C6F881C62A9AECD2B (U3C_CallContinuouslyU3Ed__335_t7F2527CCC86565E516FFF4EEEE2B4966BEA16368* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void MEC.Timing/<_CallContinuously>d__335::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_CallContinuouslyU3Ed__335_System_IDisposable_Dispose_mEC4A0D0A8554E41988B241F42180BFDB395E4A7E (U3C_CallContinuouslyU3Ed__335_t7F2527CCC86565E516FFF4EEEE2B4966BEA16368* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean MEC.Timing/<_CallContinuously>d__335::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3C_CallContinuouslyU3Ed__335_MoveNext_m7D28028CE34739B88C45BF88829B635998DA82E8 (U3C_CallContinuouslyU3Ed__335_t7F2527CCC86565E516FFF4EEEE2B4966BEA16368* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* V_1 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_1 = __this->___U3CU3E4__this_2;
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_003b;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_0070;
	}

IL_0020:
	{
		// yield return WaitForSecondsOnInstance(period);
		Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_4 = V_1;
		float L_5 = __this->___period_3;
		NullCheck(L_4);
		float L_6;
		L_6 = Timing_WaitForSecondsOnInstance_m739CE37954128787A9E525694F27B727342F52ED(L_4, L_5, NULL);
		__this->___U3CU3E2__current_1 = L_6;
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_003b:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// if (ReferenceEquals(gObject, null) || (gObject != null && gObject.activeInHierarchy))
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7 = __this->___gObject_4;
		if (!L_7)
		{
			goto IL_0065;
		}
	}
	{
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_8 = __this->___gObject_4;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_9;
		L_9 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_8, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_9)
		{
			goto IL_0070;
		}
	}
	{
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_10 = __this->___gObject_4;
		NullCheck(L_10);
		bool L_11;
		L_11 = GameObject_get_activeInHierarchy_m49250F4F168DCC5388D5BE4F6A5681386907B109(L_10, NULL);
		if (!L_11)
		{
			goto IL_0070;
		}
	}

IL_0065:
	{
		// action();
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_12 = __this->___action_5;
		NullCheck(L_12);
		Action_Invoke_m7126A54DACA72B845424072887B5F3A51FC3808E_inline(L_12, NULL);
	}

IL_0070:
	{
		// while (ReferenceEquals(gObject, null) || gObject != null)
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_13 = __this->___gObject_4;
		if (!L_13)
		{
			goto IL_0020;
		}
	}
	{
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_14 = __this->___gObject_4;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_15;
		L_15 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_14, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (L_15)
		{
			goto IL_0020;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Single MEC.Timing/<_CallContinuously>d__335::System.Collections.Generic.IEnumerator<System.Single>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float U3C_CallContinuouslyU3Ed__335_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_m13150D17C6242B01E20D6DEC006301E103E0620C (U3C_CallContinuouslyU3Ed__335_t7F2527CCC86565E516FFF4EEEE2B4966BEA16368* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void MEC.Timing/<_CallContinuously>d__335::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_CallContinuouslyU3Ed__335_System_Collections_IEnumerator_Reset_m425F3856E9D8B543F86885427C7A91DA8AB012C7 (U3C_CallContinuouslyU3Ed__335_t7F2527CCC86565E516FFF4EEEE2B4966BEA16368* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3C_CallContinuouslyU3Ed__335_System_Collections_IEnumerator_Reset_m425F3856E9D8B543F86885427C7A91DA8AB012C7_RuntimeMethod_var)));
	}
}
// System.Object MEC.Timing/<_CallContinuously>d__335::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3C_CallContinuouslyU3Ed__335_System_Collections_IEnumerator_get_Current_m039FBC06C5F63ABFF42D7F69CE81BB90A957F320 (U3C_CallContinuouslyU3Ed__335_t7F2527CCC86565E516FFF4EEEE2B4966BEA16368* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->___U3CU3E2__current_1;
		float L_1 = L_0;
		RuntimeObject* L_2 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Byte MEC.CoroutineHandle::get_Key()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint8_t CoroutineHandle_get_Key_mCB7FA3D0481F9A13B0EC1FA22CB6C9AFA95F830B (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, const RuntimeMethod* method) 
{
	{
		// public byte Key { get { return (byte)(_id & ReservedSpace); } }
		int32_t L_0 = __this->____id_2;
		return (uint8_t)((int32_t)(uint8_t)((int32_t)(L_0&((int32_t)15))));
	}
}
IL2CPP_EXTERN_C  uint8_t CoroutineHandle_get_Key_mCB7FA3D0481F9A13B0EC1FA22CB6C9AFA95F830B_AdjustorThunk (RuntimeObject* __this, const RuntimeMethod* method)
{
	CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*>(__this + _offset);
	uint8_t _returnValue;
	_returnValue = CoroutineHandle_get_Key_mCB7FA3D0481F9A13B0EC1FA22CB6C9AFA95F830B(_thisAdjusted, method);
	return _returnValue;
}
// System.Void MEC.CoroutineHandle::.ctor(System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CoroutineHandle__ctor_mCF4D58FEC43F4D8E816A39148B5C949B78CEB02D (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, uint8_t ___ind0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (ind > ReservedSpace)
		uint8_t L_0 = ___ind0;
		if ((((int32_t)L_0) <= ((int32_t)((int32_t)15))))
		{
			goto IL_000c;
		}
	}
	{
		// ind -= ReservedSpace;
		uint8_t L_1 = ___ind0;
		___ind0 = (uint8_t)((int32_t)(uint8_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_1, ((int32_t)15))));
	}

IL_000c:
	{
		// _id = NextIndex[ind] + ind;
		il2cpp_codegen_runtime_class_init_inline(CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772_il2cpp_TypeInfo_var);
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_2 = ((CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772_StaticFields*)il2cpp_codegen_static_fields_for(CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772_il2cpp_TypeInfo_var))->___NextIndex_1;
		uint8_t L_3 = ___ind0;
		NullCheck(L_2);
		uint8_t L_4 = L_3;
		int32_t L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		uint8_t L_6 = ___ind0;
		__this->____id_2 = ((int32_t)il2cpp_codegen_add(L_5, (int32_t)L_6));
		// NextIndex[ind] += ReservedSpace + 1;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_7 = ((CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772_StaticFields*)il2cpp_codegen_static_fields_for(CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772_il2cpp_TypeInfo_var))->___NextIndex_1;
		uint8_t L_8 = ___ind0;
		NullCheck(L_7);
		int32_t* L_9 = ((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_8)));
		int32_t L_10 = *((int32_t*)L_9);
		*((int32_t*)L_9) = (int32_t)((int32_t)il2cpp_codegen_add(L_10, ((int32_t)16)));
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void CoroutineHandle__ctor_mCF4D58FEC43F4D8E816A39148B5C949B78CEB02D_AdjustorThunk (RuntimeObject* __this, uint8_t ___ind0, const RuntimeMethod* method)
{
	CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*>(__this + _offset);
	CoroutineHandle__ctor_mCF4D58FEC43F4D8E816A39148B5C949B78CEB02D(_thisAdjusted, ___ind0, method);
}
// System.Void MEC.CoroutineHandle::.ctor(MEC.CoroutineHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CoroutineHandle__ctor_m786A900364E72BB4A3D58C80623E072B2E327EB6 (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___other0, const RuntimeMethod* method) 
{
	{
		// _id = other._id;
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_0 = ___other0;
		int32_t L_1 = L_0.____id_2;
		__this->____id_2 = L_1;
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void CoroutineHandle__ctor_m786A900364E72BB4A3D58C80623E072B2E327EB6_AdjustorThunk (RuntimeObject* __this, CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___other0, const RuntimeMethod* method)
{
	CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*>(__this + _offset);
	CoroutineHandle__ctor_m786A900364E72BB4A3D58C80623E072B2E327EB6(_thisAdjusted, ___other0, method);
}
// System.Boolean MEC.CoroutineHandle::Equals(MEC.CoroutineHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CoroutineHandle_Equals_m376A1B4289D9266EA669B42997BD425DC59A81E3 (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___other0, const RuntimeMethod* method) 
{
	{
		// return _id == other._id;
		int32_t L_0 = __this->____id_2;
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_1 = ___other0;
		int32_t L_2 = L_1.____id_2;
		return (bool)((((int32_t)L_0) == ((int32_t)L_2))? 1 : 0);
	}
}
IL2CPP_EXTERN_C  bool CoroutineHandle_Equals_m376A1B4289D9266EA669B42997BD425DC59A81E3_AdjustorThunk (RuntimeObject* __this, CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___other0, const RuntimeMethod* method)
{
	CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*>(__this + _offset);
	bool _returnValue;
	_returnValue = CoroutineHandle_Equals_m376A1B4289D9266EA669B42997BD425DC59A81E3(_thisAdjusted, ___other0, method);
	return _returnValue;
}
// System.Boolean MEC.CoroutineHandle::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CoroutineHandle_Equals_mE9A2F6DD86A6E40A3AC3B2C0B5717FB3154C3E95 (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, RuntimeObject* ___other0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (other is CoroutineHandle)
		RuntimeObject* L_0 = ___other0;
		if (!((RuntimeObject*)IsInstSealed((RuntimeObject*)L_0, CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772_il2cpp_TypeInfo_var)))
		{
			goto IL_0015;
		}
	}
	{
		// return Equals((CoroutineHandle)other);
		RuntimeObject* L_1 = ___other0;
		bool L_2;
		L_2 = CoroutineHandle_Equals_m376A1B4289D9266EA669B42997BD425DC59A81E3(__this, ((*(CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*)((CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*)(CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*)UnBox(L_1, CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772_il2cpp_TypeInfo_var)))), NULL);
		return L_2;
	}

IL_0015:
	{
		// return false;
		return (bool)0;
	}
}
IL2CPP_EXTERN_C  bool CoroutineHandle_Equals_mE9A2F6DD86A6E40A3AC3B2C0B5717FB3154C3E95_AdjustorThunk (RuntimeObject* __this, RuntimeObject* ___other0, const RuntimeMethod* method)
{
	CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*>(__this + _offset);
	bool _returnValue;
	_returnValue = CoroutineHandle_Equals_mE9A2F6DD86A6E40A3AC3B2C0B5717FB3154C3E95(_thisAdjusted, ___other0, method);
	return _returnValue;
}
// System.Boolean MEC.CoroutineHandle::op_Equality(MEC.CoroutineHandle,MEC.CoroutineHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CoroutineHandle_op_Equality_m33AD87122AA5EDE4D103B29073052272E944B5C7 (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___a0, CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___b1, const RuntimeMethod* method) 
{
	{
		// return a._id == b._id;
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_0 = ___a0;
		int32_t L_1 = L_0.____id_2;
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_2 = ___b1;
		int32_t L_3 = L_2.____id_2;
		return (bool)((((int32_t)L_1) == ((int32_t)L_3))? 1 : 0);
	}
}
// System.Boolean MEC.CoroutineHandle::op_Inequality(MEC.CoroutineHandle,MEC.CoroutineHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CoroutineHandle_op_Inequality_mD9AF77FB7736D80D4DE809502E1104EFC7AA461C (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___a0, CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___b1, const RuntimeMethod* method) 
{
	{
		// return a._id != b._id;
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_0 = ___a0;
		int32_t L_1 = L_0.____id_2;
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_2 = ___b1;
		int32_t L_3 = L_2.____id_2;
		return (bool)((((int32_t)((((int32_t)L_1) == ((int32_t)L_3))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 MEC.CoroutineHandle::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t CoroutineHandle_GetHashCode_m146A36001E4646CB9F3F4548A7DD789FF89928EA (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, const RuntimeMethod* method) 
{
	{
		// return _id;
		int32_t L_0 = __this->____id_2;
		return L_0;
	}
}
IL2CPP_EXTERN_C  int32_t CoroutineHandle_GetHashCode_m146A36001E4646CB9F3F4548A7DD789FF89928EA_AdjustorThunk (RuntimeObject* __this, const RuntimeMethod* method)
{
	CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*>(__this + _offset);
	int32_t _returnValue;
	_returnValue = CoroutineHandle_GetHashCode_m146A36001E4646CB9F3F4548A7DD789FF89928EA_inline(_thisAdjusted, method);
	return _returnValue;
}
// System.String MEC.CoroutineHandle::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* CoroutineHandle_ToString_m46BBC999985604BAB9F8DEBDF875F93A986C5298 (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Nullable_1_ToString_m5FDC0CBE068DB2893454257CE6E29846D47B3038_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Nullable_1_get_HasValue_mCF2FD8B3055FA87FC9C504F2122B3B0FAEDE3EC9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral37812EA687D99D91FFF18BD0937BED8AD0E13ED4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7461050B85255933384A142D9959A57EEFAB5448);
		s_Il2CppMethodInitialized = true;
	}
	Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if (Timing.GetTag(this) == null)
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_0 = (*(CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*)__this);
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		String_t* L_1;
		L_1 = Timing_GetTag_m3FFBD6D52CBACFE6084DB6369448FD2773DC490F(L_0, NULL);
		if (L_1)
		{
			goto IL_005d;
		}
	}
	{
		// if (Timing.GetLayer(this) == null)
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_2 = (*(CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*)__this);
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28 L_3;
		L_3 = Timing_GetLayer_mE7DD05B2F69426F64199C1D0D1DDA25E089C73EF(L_2, NULL);
		V_0 = L_3;
		bool L_4;
		L_4 = Nullable_1_get_HasValue_mCF2FD8B3055FA87FC9C504F2122B3B0FAEDE3EC9_inline((&V_0), Nullable_1_get_HasValue_mCF2FD8B3055FA87FC9C504F2122B3B0FAEDE3EC9_RuntimeMethod_var);
		if (L_4)
		{
			goto IL_002e;
		}
	}
	{
		// return Timing.GetDebugName(this);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_5 = (*(CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*)__this);
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		String_t* L_6;
		L_6 = Timing_GetDebugName_m07F6E0F01437B37B14A09E9F9D7A540B86DDCB35(L_5, NULL);
		return L_6;
	}

IL_002e:
	{
		// return Timing.GetDebugName(this) + " Layer: " + Timing.GetLayer(this);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_7 = (*(CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*)__this);
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		String_t* L_8;
		L_8 = Timing_GetDebugName_m07F6E0F01437B37B14A09E9F9D7A540B86DDCB35(L_7, NULL);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_9 = (*(CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*)__this);
		Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28 L_10;
		L_10 = Timing_GetLayer_mE7DD05B2F69426F64199C1D0D1DDA25E089C73EF(L_9, NULL);
		V_0 = L_10;
		String_t* L_11;
		L_11 = Nullable_1_ToString_m5FDC0CBE068DB2893454257CE6E29846D47B3038((&V_0), Nullable_1_ToString_m5FDC0CBE068DB2893454257CE6E29846D47B3038_RuntimeMethod_var);
		String_t* L_12;
		L_12 = String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0(L_8, _stringLiteral37812EA687D99D91FFF18BD0937BED8AD0E13ED4, L_11, NULL);
		return L_12;
	}

IL_005d:
	{
		// if (Timing.GetLayer(this) == null)
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_13 = (*(CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*)__this);
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28 L_14;
		L_14 = Timing_GetLayer_mE7DD05B2F69426F64199C1D0D1DDA25E089C73EF(L_13, NULL);
		V_0 = L_14;
		bool L_15;
		L_15 = Nullable_1_get_HasValue_mCF2FD8B3055FA87FC9C504F2122B3B0FAEDE3EC9_inline((&V_0), Nullable_1_get_HasValue_mCF2FD8B3055FA87FC9C504F2122B3B0FAEDE3EC9_RuntimeMethod_var);
		if (L_15)
		{
			goto IL_0093;
		}
	}
	{
		// return Timing.GetDebugName(this) + " Tag: " + Timing.GetTag(this);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_16 = (*(CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*)__this);
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		String_t* L_17;
		L_17 = Timing_GetDebugName_m07F6E0F01437B37B14A09E9F9D7A540B86DDCB35(L_16, NULL);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_18 = (*(CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*)__this);
		String_t* L_19;
		L_19 = Timing_GetTag_m3FFBD6D52CBACFE6084DB6369448FD2773DC490F(L_18, NULL);
		String_t* L_20;
		L_20 = String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0(L_17, _stringLiteral7461050B85255933384A142D9959A57EEFAB5448, L_19, NULL);
		return L_20;
	}

IL_0093:
	{
		// return Timing.GetDebugName(this) + " Tag: " + Timing.GetTag(this) + " Layer: " + Timing.GetLayer(this);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_21 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_22 = L_21;
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_23 = (*(CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*)__this);
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		String_t* L_24;
		L_24 = Timing_GetDebugName_m07F6E0F01437B37B14A09E9F9D7A540B86DDCB35(L_23, NULL);
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_24);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_24);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_25 = L_22;
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, _stringLiteral7461050B85255933384A142D9959A57EEFAB5448);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral7461050B85255933384A142D9959A57EEFAB5448);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_26 = L_25;
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_27 = (*(CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*)__this);
		String_t* L_28;
		L_28 = Timing_GetTag_m3FFBD6D52CBACFE6084DB6369448FD2773DC490F(L_27, NULL);
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, L_28);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_28);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_29 = L_26;
		NullCheck(L_29);
		ArrayElementTypeCheck (L_29, _stringLiteral37812EA687D99D91FFF18BD0937BED8AD0E13ED4);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral37812EA687D99D91FFF18BD0937BED8AD0E13ED4);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_30 = L_29;
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_31 = (*(CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*)__this);
		Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28 L_32;
		L_32 = Timing_GetLayer_mE7DD05B2F69426F64199C1D0D1DDA25E089C73EF(L_31, NULL);
		V_0 = L_32;
		String_t* L_33;
		L_33 = Nullable_1_ToString_m5FDC0CBE068DB2893454257CE6E29846D47B3038((&V_0), Nullable_1_ToString_m5FDC0CBE068DB2893454257CE6E29846D47B3038_RuntimeMethod_var);
		NullCheck(L_30);
		ArrayElementTypeCheck (L_30, L_33);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_33);
		String_t* L_34;
		L_34 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_30, NULL);
		return L_34;
	}
}
IL2CPP_EXTERN_C  String_t* CoroutineHandle_ToString_m46BBC999985604BAB9F8DEBDF875F93A986C5298_AdjustorThunk (RuntimeObject* __this, const RuntimeMethod* method)
{
	CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*>(__this + _offset);
	String_t* _returnValue;
	_returnValue = CoroutineHandle_ToString_m46BBC999985604BAB9F8DEBDF875F93A986C5298(_thisAdjusted, method);
	return _returnValue;
}
// System.String MEC.CoroutineHandle::get_Tag()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* CoroutineHandle_get_Tag_m98BDDB7DDC278A3F30EBA5E7F7376FDB6F0BA189 (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// get { return Timing.GetTag(this); }
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_0 = (*(CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*)__this);
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		String_t* L_1;
		L_1 = Timing_GetTag_m3FFBD6D52CBACFE6084DB6369448FD2773DC490F(L_0, NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  String_t* CoroutineHandle_get_Tag_m98BDDB7DDC278A3F30EBA5E7F7376FDB6F0BA189_AdjustorThunk (RuntimeObject* __this, const RuntimeMethod* method)
{
	CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*>(__this + _offset);
	String_t* _returnValue;
	_returnValue = CoroutineHandle_get_Tag_m98BDDB7DDC278A3F30EBA5E7F7376FDB6F0BA189(_thisAdjusted, method);
	return _returnValue;
}
// System.Void MEC.CoroutineHandle::set_Tag(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CoroutineHandle_set_Tag_mDF98CC39D94E8F5E16464FDC13545A7C07A7BE43 (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, String_t* ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// set { Timing.SetTag(this, value); }
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_0 = (*(CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*)__this);
		String_t* L_1 = ___value0;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Timing_SetTag_m96D6A4058545C0BBB5504EDF8758E3131F6E8A00(L_0, L_1, (bool)1, NULL);
		// set { Timing.SetTag(this, value); }
		return;
	}
}
IL2CPP_EXTERN_C  void CoroutineHandle_set_Tag_mDF98CC39D94E8F5E16464FDC13545A7C07A7BE43_AdjustorThunk (RuntimeObject* __this, String_t* ___value0, const RuntimeMethod* method)
{
	CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*>(__this + _offset);
	CoroutineHandle_set_Tag_mDF98CC39D94E8F5E16464FDC13545A7C07A7BE43(_thisAdjusted, ___value0, method);
}
// System.Nullable`1<System.Int32> MEC.CoroutineHandle::get_Layer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28 CoroutineHandle_get_Layer_m4A8CE0F0E6F049A8C18A75064C20BE1060D52936 (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// get { return Timing.GetLayer(this); }
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_0 = (*(CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*)__this);
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28 L_1;
		L_1 = Timing_GetLayer_mE7DD05B2F69426F64199C1D0D1DDA25E089C73EF(L_0, NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28 CoroutineHandle_get_Layer_m4A8CE0F0E6F049A8C18A75064C20BE1060D52936_AdjustorThunk (RuntimeObject* __this, const RuntimeMethod* method)
{
	CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*>(__this + _offset);
	Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28 _returnValue;
	_returnValue = CoroutineHandle_get_Layer_m4A8CE0F0E6F049A8C18A75064C20BE1060D52936(_thisAdjusted, method);
	return _returnValue;
}
// System.Void MEC.CoroutineHandle::set_Layer(System.Nullable`1<System.Int32>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CoroutineHandle_set_Layer_mA6B53D3EC1ED25970E3745D1908806C99DCACD9B (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28 ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Nullable_1_get_HasValue_mCF2FD8B3055FA87FC9C504F2122B3B0FAEDE3EC9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Nullable_1_get_Value_m0DF3B826A745419D26A168689AEB4BE6F8698ECA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (value == null)
		bool L_0;
		L_0 = Nullable_1_get_HasValue_mCF2FD8B3055FA87FC9C504F2122B3B0FAEDE3EC9_inline((&___value0), Nullable_1_get_HasValue_mCF2FD8B3055FA87FC9C504F2122B3B0FAEDE3EC9_RuntimeMethod_var);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		// Timing.RemoveLayer(this);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_1 = (*(CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*)__this);
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Timing_RemoveLayer_m4788A8DAF5636900A5A0A88F02BB1ABAE7CCE37F(L_1, NULL);
		return;
	}

IL_0016:
	{
		// Timing.SetLayer(this, (int)value);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_3 = (*(CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*)__this);
		int32_t L_4;
		L_4 = Nullable_1_get_Value_m0DF3B826A745419D26A168689AEB4BE6F8698ECA((&___value0), Nullable_1_get_Value_m0DF3B826A745419D26A168689AEB4BE6F8698ECA_RuntimeMethod_var);
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Timing_SetLayer_m55CB305A12F67D268554C3FA8966C612FB64966D(L_3, L_4, (bool)1, NULL);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void CoroutineHandle_set_Layer_mA6B53D3EC1ED25970E3745D1908806C99DCACD9B_AdjustorThunk (RuntimeObject* __this, Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28 ___value0, const RuntimeMethod* method)
{
	CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*>(__this + _offset);
	CoroutineHandle_set_Layer_mA6B53D3EC1ED25970E3745D1908806C99DCACD9B(_thisAdjusted, ___value0, method);
}
// MEC.Segment MEC.CoroutineHandle::get_Segment()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t CoroutineHandle_get_Segment_m39312DC1EB22C830F2104324B99C3A146F5D38D0 (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// get { return Timing.GetSegment(this); }
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_0 = (*(CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*)__this);
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		int32_t L_1;
		L_1 = Timing_GetSegment_m8EBC2B3D9D10027B6576BD5A722F817A94B68315(L_0, NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  int32_t CoroutineHandle_get_Segment_m39312DC1EB22C830F2104324B99C3A146F5D38D0_AdjustorThunk (RuntimeObject* __this, const RuntimeMethod* method)
{
	CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*>(__this + _offset);
	int32_t _returnValue;
	_returnValue = CoroutineHandle_get_Segment_m39312DC1EB22C830F2104324B99C3A146F5D38D0(_thisAdjusted, method);
	return _returnValue;
}
// System.Void MEC.CoroutineHandle::set_Segment(MEC.Segment)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CoroutineHandle_set_Segment_m6922E71F480119164D54F04D8184EA61EC3EA52D (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, int32_t ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// set { Timing.SetSegment(this, value); }
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_0 = (*(CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*)__this);
		int32_t L_1 = ___value0;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Timing_SetSegment_mE4271A8038182019ED1C10264932B8139946BC04(L_0, L_1, NULL);
		// set { Timing.SetSegment(this, value); }
		return;
	}
}
IL2CPP_EXTERN_C  void CoroutineHandle_set_Segment_m6922E71F480119164D54F04D8184EA61EC3EA52D_AdjustorThunk (RuntimeObject* __this, int32_t ___value0, const RuntimeMethod* method)
{
	CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*>(__this + _offset);
	CoroutineHandle_set_Segment_m6922E71F480119164D54F04D8184EA61EC3EA52D(_thisAdjusted, ___value0, method);
}
// System.Boolean MEC.CoroutineHandle::get_IsRunning()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CoroutineHandle_get_IsRunning_m8E91075AE685F04BC4569D0E455CD4B10AE9B5AF (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// get { return Timing.IsRunning(this); }
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_0 = (*(CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*)__this);
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Timing_IsRunning_mA3836D665CA50666A4A3A56F711EEABF2769A41A(L_0, NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  bool CoroutineHandle_get_IsRunning_m8E91075AE685F04BC4569D0E455CD4B10AE9B5AF_AdjustorThunk (RuntimeObject* __this, const RuntimeMethod* method)
{
	CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*>(__this + _offset);
	bool _returnValue;
	_returnValue = CoroutineHandle_get_IsRunning_m8E91075AE685F04BC4569D0E455CD4B10AE9B5AF(_thisAdjusted, method);
	return _returnValue;
}
// System.Void MEC.CoroutineHandle::set_IsRunning(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CoroutineHandle_set_IsRunning_mF8D1574108AEEB339D75232A75F4E0625D49D659 (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, bool ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// set { if (!value) Timing.KillCoroutines(this); }
		bool L_0 = ___value0;
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		// set { if (!value) Timing.KillCoroutines(this); }
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_1 = (*(CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*)__this);
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		int32_t L_2;
		L_2 = Timing_KillCoroutines_mFF74632A9C79E88FC999E971D02118CCEE9F113D(L_1, NULL);
	}

IL_000f:
	{
		// set { if (!value) Timing.KillCoroutines(this); }
		return;
	}
}
IL2CPP_EXTERN_C  void CoroutineHandle_set_IsRunning_mF8D1574108AEEB339D75232A75F4E0625D49D659_AdjustorThunk (RuntimeObject* __this, bool ___value0, const RuntimeMethod* method)
{
	CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*>(__this + _offset);
	CoroutineHandle_set_IsRunning_mF8D1574108AEEB339D75232A75F4E0625D49D659(_thisAdjusted, ___value0, method);
}
// System.Boolean MEC.CoroutineHandle::get_IsAliveAndPaused()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CoroutineHandle_get_IsAliveAndPaused_mC771E398960AA40366D3EE3BB8642478B3DDFABC (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// get { return Timing.IsAliveAndPaused(this); }
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_0 = (*(CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*)__this);
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Timing_IsAliveAndPaused_m33387BF08F0E6871DF34D62EB6E9AC7B687863BF(L_0, NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  bool CoroutineHandle_get_IsAliveAndPaused_mC771E398960AA40366D3EE3BB8642478B3DDFABC_AdjustorThunk (RuntimeObject* __this, const RuntimeMethod* method)
{
	CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*>(__this + _offset);
	bool _returnValue;
	_returnValue = CoroutineHandle_get_IsAliveAndPaused_mC771E398960AA40366D3EE3BB8642478B3DDFABC(_thisAdjusted, method);
	return _returnValue;
}
// System.Void MEC.CoroutineHandle::set_IsAliveAndPaused(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CoroutineHandle_set_IsAliveAndPaused_m8B313E39221D848E7F6974FE85288ADCA1CE28B2 (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, bool ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// set { if (value) Timing.PauseCoroutines(this); else Timing.ResumeCoroutines(this); }
		bool L_0 = ___value0;
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		// set { if (value) Timing.PauseCoroutines(this); else Timing.ResumeCoroutines(this); }
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_1 = (*(CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*)__this);
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		int32_t L_2;
		L_2 = Timing_PauseCoroutines_mDC51884B61589946D922A013860F23A23A3AA29A(L_1, NULL);
		return;
	}

IL_0010:
	{
		// set { if (value) Timing.PauseCoroutines(this); else Timing.ResumeCoroutines(this); }
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_3 = (*(CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*)__this);
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		int32_t L_4;
		L_4 = Timing_ResumeCoroutines_m4472CFAFC1E76EE2ADB7921DC8AD2B0886BE1BE2(L_3, NULL);
		// set { if (value) Timing.PauseCoroutines(this); else Timing.ResumeCoroutines(this); }
		return;
	}
}
IL2CPP_EXTERN_C  void CoroutineHandle_set_IsAliveAndPaused_m8B313E39221D848E7F6974FE85288ADCA1CE28B2_AdjustorThunk (RuntimeObject* __this, bool ___value0, const RuntimeMethod* method)
{
	CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*>(__this + _offset);
	CoroutineHandle_set_IsAliveAndPaused_m8B313E39221D848E7F6974FE85288ADCA1CE28B2(_thisAdjusted, ___value0, method);
}
// System.Boolean MEC.CoroutineHandle::get_IsValid()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CoroutineHandle_get_IsValid_mA4DA81202B56F396DABF930A009CCF65F34FAD53 (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, const RuntimeMethod* method) 
{
	{
		// get { return Key != 0; }
		uint8_t L_0;
		L_0 = CoroutineHandle_get_Key_mCB7FA3D0481F9A13B0EC1FA22CB6C9AFA95F830B(__this, NULL);
		return (bool)((!(((uint32_t)L_0) <= ((uint32_t)0)))? 1 : 0);
	}
}
IL2CPP_EXTERN_C  bool CoroutineHandle_get_IsValid_mA4DA81202B56F396DABF930A009CCF65F34FAD53_AdjustorThunk (RuntimeObject* __this, const RuntimeMethod* method)
{
	CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*>(__this + _offset);
	bool _returnValue;
	_returnValue = CoroutineHandle_get_IsValid_mA4DA81202B56F396DABF930A009CCF65F34FAD53(_thisAdjusted, method);
	return _returnValue;
}
// MEC.CoroutineHandle MEC.CoroutineHandle::OnDestroy(System.Action,MEC.Segment)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 CoroutineHandle_OnDestroy_mC92C498C04D09E4648811A45E865659914D4A7AD (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___action0, int32_t ___segment1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* V_0 = NULL;
	CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// Timing inst = Timing.GetInstance(Key);
		uint8_t L_0;
		L_0 = CoroutineHandle_get_Key_mCB7FA3D0481F9A13B0EC1FA22CB6C9AFA95F830B(__this, NULL);
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_1;
		L_1 = Timing_GetInstance_m501B99433E8888DB426B38439FFB8FC45BE32411(L_0, NULL);
		V_0 = L_1;
		// if (action == null || inst == null)
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_2 = ___action0;
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_3 = V_0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Equality_mD3DB0D72CE0250C84033DC2A90AEF9D59896E536(L_3, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_4)
		{
			goto IL_0022;
		}
	}

IL_0018:
	{
		// return new CoroutineHandle();
		il2cpp_codegen_initobj((&V_1), sizeof(CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772));
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_5 = V_1;
		return L_5;
	}

IL_0022:
	{
		// return inst.RunCoroutineOnInstance(_OnDestroy(this, action), segment);
		Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_6 = V_0;
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_7 = (*(CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*)__this);
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_8 = ___action0;
		il2cpp_codegen_runtime_class_init_inline(CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772_il2cpp_TypeInfo_var);
		RuntimeObject* L_9;
		L_9 = CoroutineHandle__OnDestroy_m137647C5EE847036CD98B31AA4F576A9261F82CE(L_7, L_8, NULL);
		int32_t L_10 = ___segment1;
		NullCheck(L_6);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_11;
		L_11 = Timing_RunCoroutineOnInstance_m2DF151338C742F53D0D8DD15C7FB624CBBD54FFE(L_6, L_9, L_10, NULL);
		return L_11;
	}
}
IL2CPP_EXTERN_C  CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 CoroutineHandle_OnDestroy_mC92C498C04D09E4648811A45E865659914D4A7AD_AdjustorThunk (RuntimeObject* __this, Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___action0, int32_t ___segment1, const RuntimeMethod* method)
{
	CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*>(__this + _offset);
	CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 _returnValue;
	_returnValue = CoroutineHandle_OnDestroy_mC92C498C04D09E4648811A45E865659914D4A7AD(_thisAdjusted, ___action0, ___segment1, method);
	return _returnValue;
}
// MEC.CoroutineHandle MEC.CoroutineHandle::OnDestroy(System.Collections.Generic.IEnumerator`1<System.Single>,MEC.Segment)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 CoroutineHandle_OnDestroy_m59FB02686E0DE9D99450DBFA50597951B4B3F795 (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, RuntimeObject* ___action0, int32_t ___segment1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* V_0 = NULL;
	CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// Timing inst = Timing.GetInstance(Key);
		uint8_t L_0;
		L_0 = CoroutineHandle_get_Key_mCB7FA3D0481F9A13B0EC1FA22CB6C9AFA95F830B(__this, NULL);
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_1;
		L_1 = Timing_GetInstance_m501B99433E8888DB426B38439FFB8FC45BE32411(L_0, NULL);
		V_0 = L_1;
		// if (action == null || inst == null)
		RuntimeObject* L_2 = ___action0;
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_3 = V_0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Equality_mD3DB0D72CE0250C84033DC2A90AEF9D59896E536(L_3, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_4)
		{
			goto IL_0022;
		}
	}

IL_0018:
	{
		// return new CoroutineHandle();
		il2cpp_codegen_initobj((&V_1), sizeof(CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772));
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_5 = V_1;
		return L_5;
	}

IL_0022:
	{
		// return inst.RunCoroutineOnInstance(_OnDestroy(this, action), segment);
		Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* L_6 = V_0;
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_7 = (*(CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*)__this);
		RuntimeObject* L_8 = ___action0;
		il2cpp_codegen_runtime_class_init_inline(CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772_il2cpp_TypeInfo_var);
		RuntimeObject* L_9;
		L_9 = CoroutineHandle__OnDestroy_mC0A14D883FC5554696AC37A00976393EC544B852(L_7, L_8, NULL);
		int32_t L_10 = ___segment1;
		NullCheck(L_6);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_11;
		L_11 = Timing_RunCoroutineOnInstance_m2DF151338C742F53D0D8DD15C7FB624CBBD54FFE(L_6, L_9, L_10, NULL);
		return L_11;
	}
}
IL2CPP_EXTERN_C  CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 CoroutineHandle_OnDestroy_m59FB02686E0DE9D99450DBFA50597951B4B3F795_AdjustorThunk (RuntimeObject* __this, RuntimeObject* ___action0, int32_t ___segment1, const RuntimeMethod* method)
{
	CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772*>(__this + _offset);
	CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 _returnValue;
	_returnValue = CoroutineHandle_OnDestroy_m59FB02686E0DE9D99450DBFA50597951B4B3F795(_thisAdjusted, ___action0, ___segment1, method);
	return _returnValue;
}
// System.Collections.Generic.IEnumerator`1<System.Single> MEC.CoroutineHandle::_OnDestroy(MEC.CoroutineHandle,System.Action)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* CoroutineHandle__OnDestroy_m137647C5EE847036CD98B31AA4F576A9261F82CE (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___watched0, Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___action1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3C_OnDestroyU3Ed__32_tC237D236161175615D4FE20E0390919C6A0C1E1A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3C_OnDestroyU3Ed__32_tC237D236161175615D4FE20E0390919C6A0C1E1A* L_0 = (U3C_OnDestroyU3Ed__32_tC237D236161175615D4FE20E0390919C6A0C1E1A*)il2cpp_codegen_object_new(U3C_OnDestroyU3Ed__32_tC237D236161175615D4FE20E0390919C6A0C1E1A_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3C_OnDestroyU3Ed__32__ctor_m58B90BD1AF3010BBBC15328A4C0DAC4BD06C51A3(L_0, 0, NULL);
		U3C_OnDestroyU3Ed__32_tC237D236161175615D4FE20E0390919C6A0C1E1A* L_1 = L_0;
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_2 = ___watched0;
		NullCheck(L_1);
		L_1->___watched_2 = L_2;
		U3C_OnDestroyU3Ed__32_tC237D236161175615D4FE20E0390919C6A0C1E1A* L_3 = L_1;
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_4 = ___action1;
		NullCheck(L_3);
		L_3->___action_3 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&L_3->___action_3), (void*)L_4);
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Single> MEC.CoroutineHandle::_OnDestroy(MEC.CoroutineHandle,System.Collections.Generic.IEnumerator`1<System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* CoroutineHandle__OnDestroy_mC0A14D883FC5554696AC37A00976393EC544B852 (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___watched0, RuntimeObject* ___action1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3C_OnDestroyU3Ed__33_tBEBAF5C594D7B23973CC4FD41D86F37596AAA5F2_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3C_OnDestroyU3Ed__33_tBEBAF5C594D7B23973CC4FD41D86F37596AAA5F2* L_0 = (U3C_OnDestroyU3Ed__33_tBEBAF5C594D7B23973CC4FD41D86F37596AAA5F2*)il2cpp_codegen_object_new(U3C_OnDestroyU3Ed__33_tBEBAF5C594D7B23973CC4FD41D86F37596AAA5F2_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3C_OnDestroyU3Ed__33__ctor_m4E8DEDC116D47F081B5144ABFE30D1BDF1CA67B0(L_0, 0, NULL);
		U3C_OnDestroyU3Ed__33_tBEBAF5C594D7B23973CC4FD41D86F37596AAA5F2* L_1 = L_0;
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_2 = ___watched0;
		NullCheck(L_1);
		L_1->___watched_2 = L_2;
		U3C_OnDestroyU3Ed__33_tBEBAF5C594D7B23973CC4FD41D86F37596AAA5F2* L_3 = L_1;
		RuntimeObject* L_4 = ___action1;
		NullCheck(L_3);
		L_3->___action_3 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&L_3->___action_3), (void*)L_4);
		return L_3;
	}
}
// System.Void MEC.CoroutineHandle::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CoroutineHandle__cctor_m02ABB34873766C3200EBD0125589326B0E9494C3 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private readonly static int[] NextIndex = { ReservedSpace + 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_0 = (Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C*)(Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C*)SZArrayNew(Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16));
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_1 = L_0;
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)((int32_t)16));
		((CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772_StaticFields*)il2cpp_codegen_static_fields_for(CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772_il2cpp_TypeInfo_var))->___NextIndex_1 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&((CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772_StaticFields*)il2cpp_codegen_static_fields_for(CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772_il2cpp_TypeInfo_var))->___NextIndex_1), (void*)L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MEC.CoroutineHandle/<_OnDestroy>d__32::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_OnDestroyU3Ed__32__ctor_m58B90BD1AF3010BBBC15328A4C0DAC4BD06C51A3 (U3C_OnDestroyU3Ed__32_tC237D236161175615D4FE20E0390919C6A0C1E1A* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void MEC.CoroutineHandle/<_OnDestroy>d__32::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_OnDestroyU3Ed__32_System_IDisposable_Dispose_mCD068687305E9FB3222A468032A6F2D66EC583DB (U3C_OnDestroyU3Ed__32_tC237D236161175615D4FE20E0390919C6A0C1E1A* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean MEC.CoroutineHandle/<_OnDestroy>d__32::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3C_OnDestroyU3Ed__32_MoveNext_mA73B616D8BE7A9FF540FC065A25B70195FAD5815 (U3C_OnDestroyU3Ed__32_tC237D236161175615D4FE20E0390919C6A0C1E1A* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_0034;
	}

IL_0019:
	{
		// yield return Timing.WaitForOneFrame;
		__this->___U3CU3E2__current_1 = (-std::numeric_limits<float>::infinity());
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_002d:
	{
		__this->___U3CU3E1__state_0 = (-1);
	}

IL_0034:
	{
		// while (watched.IsRunning)
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* L_3 = (&__this->___watched_2);
		bool L_4;
		L_4 = CoroutineHandle_get_IsRunning_m8E91075AE685F04BC4569D0E455CD4B10AE9B5AF(L_3, NULL);
		if (L_4)
		{
			goto IL_0019;
		}
	}
	{
		// action();
		Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* L_5 = __this->___action_3;
		NullCheck(L_5);
		Action_Invoke_m7126A54DACA72B845424072887B5F3A51FC3808E_inline(L_5, NULL);
		// }
		return (bool)0;
	}
}
// System.Single MEC.CoroutineHandle/<_OnDestroy>d__32::System.Collections.Generic.IEnumerator<System.Single>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float U3C_OnDestroyU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_mEA5131781754144F386A8357D7267F6DA7C35298 (U3C_OnDestroyU3Ed__32_tC237D236161175615D4FE20E0390919C6A0C1E1A* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void MEC.CoroutineHandle/<_OnDestroy>d__32::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_OnDestroyU3Ed__32_System_Collections_IEnumerator_Reset_m03890BCA29362C3F164A603A01E22E2DB6496725 (U3C_OnDestroyU3Ed__32_tC237D236161175615D4FE20E0390919C6A0C1E1A* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3C_OnDestroyU3Ed__32_System_Collections_IEnumerator_Reset_m03890BCA29362C3F164A603A01E22E2DB6496725_RuntimeMethod_var)));
	}
}
// System.Object MEC.CoroutineHandle/<_OnDestroy>d__32::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3C_OnDestroyU3Ed__32_System_Collections_IEnumerator_get_Current_m05F45B0E632F63AD9F498EA3130C5649E3524150 (U3C_OnDestroyU3Ed__32_tC237D236161175615D4FE20E0390919C6A0C1E1A* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->___U3CU3E2__current_1;
		float L_1 = L_0;
		RuntimeObject* L_2 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MEC.CoroutineHandle/<_OnDestroy>d__33::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_OnDestroyU3Ed__33__ctor_m4E8DEDC116D47F081B5144ABFE30D1BDF1CA67B0 (U3C_OnDestroyU3Ed__33_tBEBAF5C594D7B23973CC4FD41D86F37596AAA5F2* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void MEC.CoroutineHandle/<_OnDestroy>d__33::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_OnDestroyU3Ed__33_System_IDisposable_Dispose_m6FB2EC238905E5A37384E7E69F63F5A2B58CC72B (U3C_OnDestroyU3Ed__33_tBEBAF5C594D7B23973CC4FD41D86F37596AAA5F2* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean MEC.CoroutineHandle/<_OnDestroy>d__33::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3C_OnDestroyU3Ed__33_MoveNext_m29911FA7FF46B542C64667F3674ECC121F58ABEF (U3C_OnDestroyU3Ed__33_tBEBAF5C594D7B23973CC4FD41D86F37596AAA5F2* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001b;
			}
			case 1:
			{
				goto IL_0038;
			}
			case 2:
			{
				goto IL_0068;
			}
		}
	}
	{
		return (bool)0;
	}

IL_001b:
	{
		__this->___U3CU3E1__state_0 = (-1);
		goto IL_003f;
	}

IL_0024:
	{
		// yield return Timing.WaitForOneFrame;
		__this->___U3CU3E2__current_1 = (-std::numeric_limits<float>::infinity());
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0038:
	{
		__this->___U3CU3E1__state_0 = (-1);
	}

IL_003f:
	{
		// while (watched.IsRunning)
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* L_2 = (&__this->___watched_2);
		bool L_3;
		L_3 = CoroutineHandle_get_IsRunning_m8E91075AE685F04BC4569D0E455CD4B10AE9B5AF(L_2, NULL);
		if (L_3)
		{
			goto IL_0024;
		}
	}
	{
		goto IL_006f;
	}

IL_004e:
	{
		// yield return action.Current;
		RuntimeObject* L_4 = __this->___action_3;
		NullCheck(L_4);
		float L_5;
		L_5 = InterfaceFuncInvoker0< float >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010_il2cpp_TypeInfo_var, L_4);
		__this->___U3CU3E2__current_1 = L_5;
		__this->___U3CU3E1__state_0 = 2;
		return (bool)1;
	}

IL_0068:
	{
		__this->___U3CU3E1__state_0 = (-1);
	}

IL_006f:
	{
		// while (action.MoveNext())
		RuntimeObject* L_6 = __this->___action_3;
		NullCheck(L_6);
		bool L_7;
		L_7 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_6);
		if (L_7)
		{
			goto IL_004e;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Single MEC.CoroutineHandle/<_OnDestroy>d__33::System.Collections.Generic.IEnumerator<System.Single>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float U3C_OnDestroyU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_m3F097EDC19EA0532AB6D461CBA503A95FF693089 (U3C_OnDestroyU3Ed__33_tBEBAF5C594D7B23973CC4FD41D86F37596AAA5F2* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void MEC.CoroutineHandle/<_OnDestroy>d__33::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3C_OnDestroyU3Ed__33_System_Collections_IEnumerator_Reset_m545DAC89CB85230BFB51A9D5F78D7C450F0ED353 (U3C_OnDestroyU3Ed__33_tBEBAF5C594D7B23973CC4FD41D86F37596AAA5F2* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3C_OnDestroyU3Ed__33_System_Collections_IEnumerator_Reset_m545DAC89CB85230BFB51A9D5F78D7C450F0ED353_RuntimeMethod_var)));
	}
}
// System.Object MEC.CoroutineHandle/<_OnDestroy>d__33::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3C_OnDestroyU3Ed__33_System_Collections_IEnumerator_get_Current_m4F1429AD213B305DBB77201358A3A60752A08510 (U3C_OnDestroyU3Ed__33_tBEBAF5C594D7B23973CC4FD41D86F37596AAA5F2* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->___U3CU3E2__current_1;
		float L_1 = L_0;
		RuntimeObject* L_2 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// MEC.CoroutineHandle MEC.MECExtensionMethods1::RunCoroutine(System.Collections.Generic.IEnumerator`1<System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 MECExtensionMethods1_RunCoroutine_m5A55080B8676F4A86B0C0FCEE130967AE214B3C0 (RuntimeObject* ___coroutine0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Timing.RunCoroutine(coroutine);
		RuntimeObject* L_0 = ___coroutine0;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_1;
		L_1 = Timing_RunCoroutine_m305561CB26854D87B49610D24312D68C3E5F5EC0(L_0, NULL);
		return L_1;
	}
}
// MEC.CoroutineHandle MEC.MECExtensionMethods1::RunCoroutine(System.Collections.Generic.IEnumerator`1<System.Single>,UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 MECExtensionMethods1_RunCoroutine_m7C4D592465F06FDD8C4C14CB38102AB6546910CD (RuntimeObject* ___coroutine0, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___gameObj1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Timing.RunCoroutine(coroutine, gameObj);
		RuntimeObject* L_0 = ___coroutine0;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = ___gameObj1;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_2;
		L_2 = Timing_RunCoroutine_m2DFC1726A41097DCF9BA8D0477EED8CC1BF38227(L_0, L_1, NULL);
		return L_2;
	}
}
// MEC.CoroutineHandle MEC.MECExtensionMethods1::RunCoroutine(System.Collections.Generic.IEnumerator`1<System.Single>,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 MECExtensionMethods1_RunCoroutine_m89343BE5B98CE16391BA62B93CBE83EC97F8DEC9 (RuntimeObject* ___coroutine0, int32_t ___layer1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Timing.RunCoroutine(coroutine, layer);
		RuntimeObject* L_0 = ___coroutine0;
		int32_t L_1 = ___layer1;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_2;
		L_2 = Timing_RunCoroutine_mC2FA00211167549EE8A67379A2E5EA51C2B1AB7E(L_0, L_1, NULL);
		return L_2;
	}
}
// MEC.CoroutineHandle MEC.MECExtensionMethods1::RunCoroutine(System.Collections.Generic.IEnumerator`1<System.Single>,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 MECExtensionMethods1_RunCoroutine_mE0D08ABD0742E33CF9959F7B00469F9A2671C6DE (RuntimeObject* ___coroutine0, String_t* ___tag1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Timing.RunCoroutine(coroutine, tag);
		RuntimeObject* L_0 = ___coroutine0;
		String_t* L_1 = ___tag1;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_2;
		L_2 = Timing_RunCoroutine_m2E4FCC8B56FDBA931588D91A3EAE3BD4B930F295(L_0, L_1, NULL);
		return L_2;
	}
}
// MEC.CoroutineHandle MEC.MECExtensionMethods1::RunCoroutine(System.Collections.Generic.IEnumerator`1<System.Single>,UnityEngine.GameObject,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 MECExtensionMethods1_RunCoroutine_m54031F62C5FC009B91DAAAE828008F9EA52D3FD1 (RuntimeObject* ___coroutine0, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___gameObj1, String_t* ___tag2, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Timing.RunCoroutine(coroutine, gameObj, tag);
		RuntimeObject* L_0 = ___coroutine0;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = ___gameObj1;
		String_t* L_2 = ___tag2;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_3;
		L_3 = Timing_RunCoroutine_m6D96D9EF50C4E426D04CCD05321C1EF167B0F291(L_0, L_1, L_2, NULL);
		return L_3;
	}
}
// MEC.CoroutineHandle MEC.MECExtensionMethods1::RunCoroutine(System.Collections.Generic.IEnumerator`1<System.Single>,System.Int32,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 MECExtensionMethods1_RunCoroutine_m5333DF589429A54BF6029FBE2DDA837D88EC5DD1 (RuntimeObject* ___coroutine0, int32_t ___layer1, String_t* ___tag2, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Timing.RunCoroutine(coroutine, layer, tag);
		RuntimeObject* L_0 = ___coroutine0;
		int32_t L_1 = ___layer1;
		String_t* L_2 = ___tag2;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_3;
		L_3 = Timing_RunCoroutine_m6E6FBC58F3524DB68EFFF7EE323D100D17B329D3(L_0, L_1, L_2, NULL);
		return L_3;
	}
}
// MEC.CoroutineHandle MEC.MECExtensionMethods1::RunCoroutine(System.Collections.Generic.IEnumerator`1<System.Single>,MEC.Segment)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 MECExtensionMethods1_RunCoroutine_m0E3F56211AE1F31FC40CD1BDCFCF0F1DC0689E23 (RuntimeObject* ___coroutine0, int32_t ___segment1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Timing.RunCoroutine(coroutine, segment);
		RuntimeObject* L_0 = ___coroutine0;
		int32_t L_1 = ___segment1;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_2;
		L_2 = Timing_RunCoroutine_m0AAC38EFC113F9F0E3863713D0DFEBF89AD3AC02(L_0, L_1, NULL);
		return L_2;
	}
}
// MEC.CoroutineHandle MEC.MECExtensionMethods1::RunCoroutine(System.Collections.Generic.IEnumerator`1<System.Single>,MEC.Segment,UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 MECExtensionMethods1_RunCoroutine_mC2FB83BFAE1091109898C59F8C82A5CDBB98509A (RuntimeObject* ___coroutine0, int32_t ___segment1, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___gameObj2, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Timing.RunCoroutine(coroutine, segment, gameObj);
		RuntimeObject* L_0 = ___coroutine0;
		int32_t L_1 = ___segment1;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = ___gameObj2;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_3;
		L_3 = Timing_RunCoroutine_m9F766688EFD73238C991A00887473588C644B942(L_0, L_1, L_2, NULL);
		return L_3;
	}
}
// MEC.CoroutineHandle MEC.MECExtensionMethods1::RunCoroutine(System.Collections.Generic.IEnumerator`1<System.Single>,MEC.Segment,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 MECExtensionMethods1_RunCoroutine_m2AA1F44B8B593AD375A6EBE7D77F2ECD98D152DB (RuntimeObject* ___coroutine0, int32_t ___segment1, int32_t ___layer2, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Timing.RunCoroutine(coroutine, segment, layer);
		RuntimeObject* L_0 = ___coroutine0;
		int32_t L_1 = ___segment1;
		int32_t L_2 = ___layer2;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_3;
		L_3 = Timing_RunCoroutine_m3D86C97DCD708C04E2B4B95125C261C198F6F118(L_0, L_1, L_2, NULL);
		return L_3;
	}
}
// MEC.CoroutineHandle MEC.MECExtensionMethods1::RunCoroutine(System.Collections.Generic.IEnumerator`1<System.Single>,MEC.Segment,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 MECExtensionMethods1_RunCoroutine_m749B0E30326A9E077BE210C861A22DFE38ABF538 (RuntimeObject* ___coroutine0, int32_t ___segment1, String_t* ___tag2, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Timing.RunCoroutine(coroutine, segment, tag);
		RuntimeObject* L_0 = ___coroutine0;
		int32_t L_1 = ___segment1;
		String_t* L_2 = ___tag2;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_3;
		L_3 = Timing_RunCoroutine_mD29B39A24A15D8D91F327F5BAB1DF1E8B150E693(L_0, L_1, L_2, NULL);
		return L_3;
	}
}
// MEC.CoroutineHandle MEC.MECExtensionMethods1::RunCoroutine(System.Collections.Generic.IEnumerator`1<System.Single>,MEC.Segment,UnityEngine.GameObject,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 MECExtensionMethods1_RunCoroutine_m7E01FB5716EA356750F009530E35D3433827ECEA (RuntimeObject* ___coroutine0, int32_t ___segment1, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___gameObj2, String_t* ___tag3, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Timing.RunCoroutine(coroutine, segment, gameObj, tag);
		RuntimeObject* L_0 = ___coroutine0;
		int32_t L_1 = ___segment1;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = ___gameObj2;
		String_t* L_3 = ___tag3;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_4;
		L_4 = Timing_RunCoroutine_mE568B2CBC2311DD24DCD39B7FC1781E0E84931FA(L_0, L_1, L_2, L_3, NULL);
		return L_4;
	}
}
// MEC.CoroutineHandle MEC.MECExtensionMethods1::RunCoroutine(System.Collections.Generic.IEnumerator`1<System.Single>,MEC.Segment,System.Int32,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 MECExtensionMethods1_RunCoroutine_mFA0BEB7A71CF15564A86C9E55D0ABB911520ABAE (RuntimeObject* ___coroutine0, int32_t ___segment1, int32_t ___layer2, String_t* ___tag3, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Timing.RunCoroutine(coroutine, segment, layer, tag);
		RuntimeObject* L_0 = ___coroutine0;
		int32_t L_1 = ___segment1;
		int32_t L_2 = ___layer2;
		String_t* L_3 = ___tag3;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_4;
		L_4 = Timing_RunCoroutine_m93D5FAC1E8FC571E40C5E315BA2E9912E8E2AB8F(L_0, L_1, L_2, L_3, NULL);
		return L_4;
	}
}
// MEC.CoroutineHandle MEC.MECExtensionMethods1::RunCoroutineSingleton(System.Collections.Generic.IEnumerator`1<System.Single>,MEC.CoroutineHandle,MEC.SingletonBehavior)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 MECExtensionMethods1_RunCoroutineSingleton_mC41B2EA9AE691E9D374F156A93DF318962339AEB (RuntimeObject* ___coroutine0, CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___handle1, int32_t ___behaviorOnCollision2, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Timing.RunCoroutineSingleton(coroutine, handle, behaviorOnCollision);
		RuntimeObject* L_0 = ___coroutine0;
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_1 = ___handle1;
		int32_t L_2 = ___behaviorOnCollision2;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_3;
		L_3 = Timing_RunCoroutineSingleton_mE984BA10D2E2D367B4670A056B38BA7DD6895E9B(L_0, L_1, L_2, NULL);
		return L_3;
	}
}
// MEC.CoroutineHandle MEC.MECExtensionMethods1::RunCoroutineSingleton(System.Collections.Generic.IEnumerator`1<System.Single>,UnityEngine.GameObject,MEC.SingletonBehavior)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 MECExtensionMethods1_RunCoroutineSingleton_mEF49D98D48D58A3111042F50079B725E1E5B5A7D (RuntimeObject* ___coroutine0, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___gameObj1, int32_t ___behaviorOnCollision2, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return gameObj == null ? Timing.RunCoroutine(coroutine) :
		//     Timing.RunCoroutineSingleton(coroutine, gameObj.GetInstanceID(), behaviorOnCollision);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = ___gameObj1;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mD3DB0D72CE0250C84033DC2A90AEF9D59896E536(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (L_1)
		{
			goto IL_0017;
		}
	}
	{
		RuntimeObject* L_2 = ___coroutine0;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = ___gameObj1;
		NullCheck(L_3);
		int32_t L_4;
		L_4 = Object_GetInstanceID_m554FF4073C9465F3835574CC084E68AAEEC6CC6A(L_3, NULL);
		int32_t L_5 = ___behaviorOnCollision2;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_6;
		L_6 = Timing_RunCoroutineSingleton_m86461102E88A1FF95E541A2AD2AEB8FF6AC3FA70(L_2, L_4, L_5, NULL);
		return L_6;
	}

IL_0017:
	{
		RuntimeObject* L_7 = ___coroutine0;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_8;
		L_8 = Timing_RunCoroutine_m305561CB26854D87B49610D24312D68C3E5F5EC0(L_7, NULL);
		return L_8;
	}
}
// MEC.CoroutineHandle MEC.MECExtensionMethods1::RunCoroutineSingleton(System.Collections.Generic.IEnumerator`1<System.Single>,System.Int32,MEC.SingletonBehavior)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 MECExtensionMethods1_RunCoroutineSingleton_mC07995742A5DB5B97D8044633248F3809853E545 (RuntimeObject* ___coroutine0, int32_t ___layer1, int32_t ___behaviorOnCollision2, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Timing.RunCoroutineSingleton(coroutine, layer, behaviorOnCollision);
		RuntimeObject* L_0 = ___coroutine0;
		int32_t L_1 = ___layer1;
		int32_t L_2 = ___behaviorOnCollision2;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_3;
		L_3 = Timing_RunCoroutineSingleton_m86461102E88A1FF95E541A2AD2AEB8FF6AC3FA70(L_0, L_1, L_2, NULL);
		return L_3;
	}
}
// MEC.CoroutineHandle MEC.MECExtensionMethods1::RunCoroutineSingleton(System.Collections.Generic.IEnumerator`1<System.Single>,System.String,MEC.SingletonBehavior)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 MECExtensionMethods1_RunCoroutineSingleton_m3D31795A43FBC331F346B6E61C0BD961471E6E02 (RuntimeObject* ___coroutine0, String_t* ___tag1, int32_t ___behaviorOnCollision2, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Timing.RunCoroutineSingleton(coroutine, tag, behaviorOnCollision);
		RuntimeObject* L_0 = ___coroutine0;
		String_t* L_1 = ___tag1;
		int32_t L_2 = ___behaviorOnCollision2;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_3;
		L_3 = Timing_RunCoroutineSingleton_mE48BB72B57C3EAC7B635C2ABF37B9F84BCEC7CC8(L_0, L_1, L_2, NULL);
		return L_3;
	}
}
// MEC.CoroutineHandle MEC.MECExtensionMethods1::RunCoroutineSingleton(System.Collections.Generic.IEnumerator`1<System.Single>,UnityEngine.GameObject,System.String,MEC.SingletonBehavior)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 MECExtensionMethods1_RunCoroutineSingleton_mBFE7F45C9DD2841FF2E69BECD22054FE8034506A (RuntimeObject* ___coroutine0, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___gameObj1, String_t* ___tag2, int32_t ___behaviorOnCollision3, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return gameObj == null ? Timing.RunCoroutineSingleton(coroutine, tag, behaviorOnCollision)
		//     : Timing.RunCoroutineSingleton(coroutine, gameObj.GetInstanceID(), tag, behaviorOnCollision);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = ___gameObj1;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mD3DB0D72CE0250C84033DC2A90AEF9D59896E536(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		RuntimeObject* L_2 = ___coroutine0;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = ___gameObj1;
		NullCheck(L_3);
		int32_t L_4;
		L_4 = Object_GetInstanceID_m554FF4073C9465F3835574CC084E68AAEEC6CC6A(L_3, NULL);
		String_t* L_5 = ___tag2;
		int32_t L_6 = ___behaviorOnCollision3;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_7;
		L_7 = Timing_RunCoroutineSingleton_m40F0A0B1B877BA62D7F4F85582142D64363C5347(L_2, L_4, L_5, L_6, NULL);
		return L_7;
	}

IL_0018:
	{
		RuntimeObject* L_8 = ___coroutine0;
		String_t* L_9 = ___tag2;
		int32_t L_10 = ___behaviorOnCollision3;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_11;
		L_11 = Timing_RunCoroutineSingleton_mE48BB72B57C3EAC7B635C2ABF37B9F84BCEC7CC8(L_8, L_9, L_10, NULL);
		return L_11;
	}
}
// MEC.CoroutineHandle MEC.MECExtensionMethods1::RunCoroutineSingleton(System.Collections.Generic.IEnumerator`1<System.Single>,System.Int32,System.String,MEC.SingletonBehavior)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 MECExtensionMethods1_RunCoroutineSingleton_m2EF2C518C5463F41907C7A4337E1155647A89F1F (RuntimeObject* ___coroutine0, int32_t ___layer1, String_t* ___tag2, int32_t ___behaviorOnCollision3, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Timing.RunCoroutineSingleton(coroutine, layer, tag, behaviorOnCollision);
		RuntimeObject* L_0 = ___coroutine0;
		int32_t L_1 = ___layer1;
		String_t* L_2 = ___tag2;
		int32_t L_3 = ___behaviorOnCollision3;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_4;
		L_4 = Timing_RunCoroutineSingleton_m40F0A0B1B877BA62D7F4F85582142D64363C5347(L_0, L_1, L_2, L_3, NULL);
		return L_4;
	}
}
// MEC.CoroutineHandle MEC.MECExtensionMethods1::RunCoroutineSingleton(System.Collections.Generic.IEnumerator`1<System.Single>,MEC.CoroutineHandle,MEC.Segment,MEC.SingletonBehavior)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 MECExtensionMethods1_RunCoroutineSingleton_m4A58AA9B30DAF46AE2D08EC245ABA90EB27AE886 (RuntimeObject* ___coroutine0, CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___handle1, int32_t ___segment2, int32_t ___behaviorOnCollision3, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Timing.RunCoroutineSingleton(coroutine, handle, segment, behaviorOnCollision);
		RuntimeObject* L_0 = ___coroutine0;
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_1 = ___handle1;
		int32_t L_2 = ___segment2;
		int32_t L_3 = ___behaviorOnCollision3;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_4;
		L_4 = Timing_RunCoroutineSingleton_m1F53682D5A2464E16C3E9ED35811674552A63CBD(L_0, L_1, L_2, L_3, NULL);
		return L_4;
	}
}
// MEC.CoroutineHandle MEC.MECExtensionMethods1::RunCoroutineSingleton(System.Collections.Generic.IEnumerator`1<System.Single>,MEC.Segment,UnityEngine.GameObject,MEC.SingletonBehavior)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 MECExtensionMethods1_RunCoroutineSingleton_m50903E68894D2C87F442B239801D136AC232CE33 (RuntimeObject* ___coroutine0, int32_t ___segment1, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___gameObj2, int32_t ___behaviorOnCollision3, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return gameObj == null ? Timing.RunCoroutine(coroutine, segment) :
		//     Timing.RunCoroutineSingleton(coroutine, segment, gameObj.GetInstanceID(), behaviorOnCollision);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = ___gameObj2;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mD3DB0D72CE0250C84033DC2A90AEF9D59896E536(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		RuntimeObject* L_2 = ___coroutine0;
		int32_t L_3 = ___segment1;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = ___gameObj2;
		NullCheck(L_4);
		int32_t L_5;
		L_5 = Object_GetInstanceID_m554FF4073C9465F3835574CC084E68AAEEC6CC6A(L_4, NULL);
		int32_t L_6 = ___behaviorOnCollision3;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_7;
		L_7 = Timing_RunCoroutineSingleton_mCC729C918EA4FE33811DB7EAA33DC85939176CB3(L_2, L_3, L_5, L_6, NULL);
		return L_7;
	}

IL_0018:
	{
		RuntimeObject* L_8 = ___coroutine0;
		int32_t L_9 = ___segment1;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_10;
		L_10 = Timing_RunCoroutine_m0AAC38EFC113F9F0E3863713D0DFEBF89AD3AC02(L_8, L_9, NULL);
		return L_10;
	}
}
// MEC.CoroutineHandle MEC.MECExtensionMethods1::RunCoroutineSingleton(System.Collections.Generic.IEnumerator`1<System.Single>,MEC.Segment,System.Int32,MEC.SingletonBehavior)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 MECExtensionMethods1_RunCoroutineSingleton_mBE393301D24D7A3DB3FA620A891DF9C6369373E7 (RuntimeObject* ___coroutine0, int32_t ___segment1, int32_t ___layer2, int32_t ___behaviorOnCollision3, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Timing.RunCoroutineSingleton(coroutine, segment, layer, behaviorOnCollision);
		RuntimeObject* L_0 = ___coroutine0;
		int32_t L_1 = ___segment1;
		int32_t L_2 = ___layer2;
		int32_t L_3 = ___behaviorOnCollision3;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_4;
		L_4 = Timing_RunCoroutineSingleton_mCC729C918EA4FE33811DB7EAA33DC85939176CB3(L_0, L_1, L_2, L_3, NULL);
		return L_4;
	}
}
// MEC.CoroutineHandle MEC.MECExtensionMethods1::RunCoroutineSingleton(System.Collections.Generic.IEnumerator`1<System.Single>,MEC.Segment,System.String,MEC.SingletonBehavior)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 MECExtensionMethods1_RunCoroutineSingleton_m58051CE7BB685035EF5AC335ADD0A5FF6098470D (RuntimeObject* ___coroutine0, int32_t ___segment1, String_t* ___tag2, int32_t ___behaviorOnCollision3, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Timing.RunCoroutineSingleton(coroutine, segment, tag, behaviorOnCollision);
		RuntimeObject* L_0 = ___coroutine0;
		int32_t L_1 = ___segment1;
		String_t* L_2 = ___tag2;
		int32_t L_3 = ___behaviorOnCollision3;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_4;
		L_4 = Timing_RunCoroutineSingleton_mE7A0FEEF326DF483DBDB1172E5C84D4E8D799297(L_0, L_1, L_2, L_3, NULL);
		return L_4;
	}
}
// MEC.CoroutineHandle MEC.MECExtensionMethods1::RunCoroutineSingleton(System.Collections.Generic.IEnumerator`1<System.Single>,MEC.Segment,UnityEngine.GameObject,System.String,MEC.SingletonBehavior)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 MECExtensionMethods1_RunCoroutineSingleton_m79A83EA3854049FDC583497C5EA6AB24880F30EE (RuntimeObject* ___coroutine0, int32_t ___segment1, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___gameObj2, String_t* ___tag3, int32_t ___behaviorOnCollision4, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return gameObj == null ? Timing.RunCoroutineSingleton(coroutine, segment, tag, behaviorOnCollision)
		//     : Timing.RunCoroutineSingleton(coroutine, segment, gameObj.GetInstanceID(), tag, behaviorOnCollision);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = ___gameObj2;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mD3DB0D72CE0250C84033DC2A90AEF9D59896E536(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (L_1)
		{
			goto IL_001a;
		}
	}
	{
		RuntimeObject* L_2 = ___coroutine0;
		int32_t L_3 = ___segment1;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = ___gameObj2;
		NullCheck(L_4);
		int32_t L_5;
		L_5 = Object_GetInstanceID_m554FF4073C9465F3835574CC084E68AAEEC6CC6A(L_4, NULL);
		String_t* L_6 = ___tag3;
		int32_t L_7 = ___behaviorOnCollision4;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_8;
		L_8 = Timing_RunCoroutineSingleton_mA27E8D821C8C70B02EA97C9203FA1F2218C62A11(L_2, L_3, L_5, L_6, L_7, NULL);
		return L_8;
	}

IL_001a:
	{
		RuntimeObject* L_9 = ___coroutine0;
		int32_t L_10 = ___segment1;
		String_t* L_11 = ___tag3;
		int32_t L_12 = ___behaviorOnCollision4;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_13;
		L_13 = Timing_RunCoroutineSingleton_mE7A0FEEF326DF483DBDB1172E5C84D4E8D799297(L_9, L_10, L_11, L_12, NULL);
		return L_13;
	}
}
// MEC.CoroutineHandle MEC.MECExtensionMethods1::RunCoroutineSingleton(System.Collections.Generic.IEnumerator`1<System.Single>,MEC.Segment,System.Int32,System.String,MEC.SingletonBehavior)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 MECExtensionMethods1_RunCoroutineSingleton_m5D66DAFCB29FDA527C34E21751C34CE01244F510 (RuntimeObject* ___coroutine0, int32_t ___segment1, int32_t ___layer2, String_t* ___tag3, int32_t ___behaviorOnCollision4, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Timing.RunCoroutineSingleton(coroutine, segment, layer, tag, behaviorOnCollision);
		RuntimeObject* L_0 = ___coroutine0;
		int32_t L_1 = ___segment1;
		int32_t L_2 = ___layer2;
		String_t* L_3 = ___tag3;
		int32_t L_4 = ___behaviorOnCollision4;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_5;
		L_5 = Timing_RunCoroutineSingleton_mA27E8D821C8C70B02EA97C9203FA1F2218C62A11(L_0, L_1, L_2, L_3, L_4, NULL);
		return L_5;
	}
}
// System.Single MEC.MECExtensionMethods1::WaitUntilDone(System.Collections.Generic.IEnumerator`1<System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MECExtensionMethods1_WaitUntilDone_m69A7F4ABFDDC8457FF2614C224920534170089CF (RuntimeObject* ___newCoroutine0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Timing.WaitUntilDone(newCoroutine);
		RuntimeObject* L_0 = ___newCoroutine0;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		float L_1;
		L_1 = Timing_WaitUntilDone_m2737A996DF3BF053C9FE5E863D0E86EDC86CF6BD(L_0, NULL);
		return L_1;
	}
}
// System.Single MEC.MECExtensionMethods1::WaitUntilDone(System.Collections.Generic.IEnumerator`1<System.Single>,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MECExtensionMethods1_WaitUntilDone_mE3C26C18E635B8CC4D538F210489B0CC262A631B (RuntimeObject* ___newCoroutine0, String_t* ___tag1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Timing.WaitUntilDone(newCoroutine, tag);
		RuntimeObject* L_0 = ___newCoroutine0;
		String_t* L_1 = ___tag1;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		float L_2;
		L_2 = Timing_WaitUntilDone_mE9138099BCA19B24FDD72B0CF2DB652BEC604A68(L_0, L_1, NULL);
		return L_2;
	}
}
// System.Single MEC.MECExtensionMethods1::WaitUntilDone(System.Collections.Generic.IEnumerator`1<System.Single>,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MECExtensionMethods1_WaitUntilDone_mDA207FBD46B9A74FEB8CBDBBC9219127FC89BF6E (RuntimeObject* ___newCoroutine0, int32_t ___layer1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Timing.WaitUntilDone(newCoroutine, layer);
		RuntimeObject* L_0 = ___newCoroutine0;
		int32_t L_1 = ___layer1;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		float L_2;
		L_2 = Timing_WaitUntilDone_m402AEBABC3D70225AB598519246EA7934B4136DF(L_0, L_1, NULL);
		return L_2;
	}
}
// System.Single MEC.MECExtensionMethods1::WaitUntilDone(System.Collections.Generic.IEnumerator`1<System.Single>,System.Int32,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MECExtensionMethods1_WaitUntilDone_m7BB0612A12E37AE8CA2836DAFEFE7C98CBB96E1B (RuntimeObject* ___newCoroutine0, int32_t ___layer1, String_t* ___tag2, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Timing.WaitUntilDone(newCoroutine, layer, tag);
		RuntimeObject* L_0 = ___newCoroutine0;
		int32_t L_1 = ___layer1;
		String_t* L_2 = ___tag2;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		float L_3;
		L_3 = Timing_WaitUntilDone_mCB62F3DAA912CEB2C28DCB3241C025766958B8CC(L_0, L_1, L_2, NULL);
		return L_3;
	}
}
// System.Single MEC.MECExtensionMethods1::WaitUntilDone(System.Collections.Generic.IEnumerator`1<System.Single>,MEC.Segment)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MECExtensionMethods1_WaitUntilDone_m2AF92FEEE015A9E3BD4D9B4F6E2D4BC6BEDFB57E (RuntimeObject* ___newCoroutine0, int32_t ___segment1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Timing.WaitUntilDone(newCoroutine, segment);
		RuntimeObject* L_0 = ___newCoroutine0;
		int32_t L_1 = ___segment1;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		float L_2;
		L_2 = Timing_WaitUntilDone_mD2C9B888066BB955B7F05A8EEDDC8E23350C717A(L_0, L_1, NULL);
		return L_2;
	}
}
// System.Single MEC.MECExtensionMethods1::WaitUntilDone(System.Collections.Generic.IEnumerator`1<System.Single>,MEC.Segment,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MECExtensionMethods1_WaitUntilDone_m732E7CAD38644850B656E8F325248B41AE1AECBB (RuntimeObject* ___newCoroutine0, int32_t ___segment1, String_t* ___tag2, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Timing.WaitUntilDone(newCoroutine, segment, tag);
		RuntimeObject* L_0 = ___newCoroutine0;
		int32_t L_1 = ___segment1;
		String_t* L_2 = ___tag2;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		float L_3;
		L_3 = Timing_WaitUntilDone_m1CE5D130C79F46912ABA99904C4D43BAA497FA8A(L_0, L_1, L_2, NULL);
		return L_3;
	}
}
// System.Single MEC.MECExtensionMethods1::WaitUntilDone(System.Collections.Generic.IEnumerator`1<System.Single>,MEC.Segment,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MECExtensionMethods1_WaitUntilDone_m081470C38D88986A94473C490C197E63F7EB164B (RuntimeObject* ___newCoroutine0, int32_t ___segment1, int32_t ___layer2, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Timing.WaitUntilDone(newCoroutine, segment, layer);
		RuntimeObject* L_0 = ___newCoroutine0;
		int32_t L_1 = ___segment1;
		int32_t L_2 = ___layer2;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		float L_3;
		L_3 = Timing_WaitUntilDone_mC4C05E48616C85A0F5B73C0A1CF29F067670C13D(L_0, L_1, L_2, NULL);
		return L_3;
	}
}
// System.Single MEC.MECExtensionMethods1::WaitUntilDone(System.Collections.Generic.IEnumerator`1<System.Single>,MEC.Segment,System.Int32,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MECExtensionMethods1_WaitUntilDone_m20049CB3312DCF36355741E47EA8B045CA4B3782 (RuntimeObject* ___newCoroutine0, int32_t ___segment1, int32_t ___layer2, String_t* ___tag3, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Timing.WaitUntilDone(newCoroutine, segment, layer, tag);
		RuntimeObject* L_0 = ___newCoroutine0;
		int32_t L_1 = ___segment1;
		int32_t L_2 = ___layer2;
		String_t* L_3 = ___tag3;
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		float L_4;
		L_4 = Timing_WaitUntilDone_m78F49DF57ADB37D5F3C0A7BA22905599B5A57C91(L_0, L_1, L_2, L_3, NULL);
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Thread_t0A773B9DE873D2DCAA7D229EAB36757B500E207F* Timing_get_MainThread_mFEE85EB21F421371C788BFF8E2D61D0FF21FFEAF_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static System.Threading.Thread MainThread { get; private set; }
		il2cpp_codegen_runtime_class_init_inline(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var);
		Thread_t0A773B9DE873D2DCAA7D229EAB36757B500E207F* L_0 = ((Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_StaticFields*)il2cpp_codegen_static_fields_for(Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294_il2cpp_TypeInfo_var))->___U3CMainThreadU3Ek__BackingField_22;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_Invoke_m7126A54DACA72B845424072887B5F3A51FC3808E_inline (Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* __this, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Single_IsNaN_m684B090AA2F895FD91821CA8684CBC11D784E4DD_inline (float ___f0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BitConverter_t6E99605185963BC12B3D369E13F2B88997E64A27_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___f0;
		il2cpp_codegen_runtime_class_init_inline(BitConverter_t6E99605185963BC12B3D369E13F2B88997E64A27_il2cpp_TypeInfo_var);
		int32_t L_1;
		L_1 = BitConverter_SingleToInt32Bits_mA1902D40966CA4C89A8974B10E5680A06E88566B_inline(L_0, NULL);
		return (bool)((((int32_t)((int32_t)(L_1&((int32_t)2147483647LL)))) > ((int32_t)((int32_t)2139095040)))? 1 : 0);
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Timing_set_currentCoroutine_m3DBBE23CAA1674B5622501475023ECCF1BFAD433_inline (Timing_t036548EBA02777FE8D6A2B8B06022E9909F9C294* __this, CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___value0, const RuntimeMethod* method) 
{
	{
		// public CoroutineHandle currentCoroutine { get; private set; }
		CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 L_0 = ___value0;
		__this->___U3CcurrentCoroutineU3Ek__BackingField_23 = L_0;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t CoroutineHandle_GetHashCode_m146A36001E4646CB9F3F4548A7DD789FF89928EA_inline (CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772* __this, const RuntimeMethod* method) 
{
	{
		// return _id;
		int32_t L_0 = __this->____id_2;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Func_1_Invoke_mBB7F37C468451AF57FAF31635C544D6B8C4373B2_gshared_inline (Func_1_t2BE7F58348C9CC544A8973B3A9E55541DE43C457* __this, const RuntimeMethod* method) 
{
	typedef bool (*FunctionPointerType) (RuntimeObject*, const RuntimeMethod*);
	return ((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Func_2_Invoke_m5728ECFB038CFC6FEF889DC2D566EEF49D0E24B9_gshared_inline (Func_2_t2A7432CC4F64D0DF6D8629208B154CF139B39AF2* __this, float ___arg0, const RuntimeMethod* method) 
{
	typedef float (*FunctionPointerType) (RuntimeObject*, float, const RuntimeMethod*);
	return ((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___arg0, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_1_Invoke_mF2422B2DD29F74CE66F791C3F68E288EC7C3DB9E_gshared_inline (Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* __this, RuntimeObject* ___obj0, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___obj0, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* Func_3_Invoke_m050A64479D76DDDE27033C49DBE096F3E5045E80_gshared_inline (Func_3_t54E59EE3AB750BDCF167CEAAB996EAD80E127845* __this, RuntimeObject* ___arg10, CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772 ___arg21, const RuntimeMethod* method) 
{
	typedef RuntimeObject* (*FunctionPointerType) (RuntimeObject*, RuntimeObject*, CoroutineHandle_tD04AC14307310ABFC26600FA1D8C5EC46081A772, const RuntimeMethod*);
	return ((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___arg10, ___arg21, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Nullable_1_get_HasValue_mCF2FD8B3055FA87FC9C504F2122B3B0FAEDE3EC9_gshared_inline (Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = (bool)__this->___hasValue_0;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t BitConverter_SingleToInt32Bits_mA1902D40966CA4C89A8974B10E5680A06E88566B_inline (float ___value0, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = *((int32_t*)((uintptr_t)(&___value0)));
		return L_0;
	}
}
