﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void IntroManager::Start()
extern void IntroManager_Start_m72CB22556491D6380E44B3BC8E9D17B5D91566B6 (void);
// 0x00000002 System.Void IntroManager::OpenLink()
extern void IntroManager_OpenLink_m8C3E8B5AEE139BB64FA53C2290FCD53420D57236 (void);
// 0x00000003 System.Void IntroManager::NextContent()
extern void IntroManager_NextContent_m018CE0746537775F722812E9D9156B376E7EAD07 (void);
// 0x00000004 System.Void IntroManager::GoHome()
extern void IntroManager_GoHome_m5B132DCB9BA21B6AAB75C77560231B7374DE56B5 (void);
// 0x00000005 System.Void IntroManager::.ctor()
extern void IntroManager__ctor_m9FA97A0D02CBF5D1D6B050A1779F3439A7C56D1F (void);
// 0x00000006 System.Void MainMenuManager::LoadIntro()
extern void MainMenuManager_LoadIntro_mB76D04BCA87137A7190F7D256EC7FA5BDD6D4634 (void);
// 0x00000007 System.Void MainMenuManager::LoadVowels()
extern void MainMenuManager_LoadVowels_m51B4E5840D3347F0E394913DDE8E41349F87874D (void);
// 0x00000008 System.Void MainMenuManager::LoadConsonants()
extern void MainMenuManager_LoadConsonants_mBF9D09EED8777670E91DA5332497D255AE93C067 (void);
// 0x00000009 System.Void MainMenuManager::.ctor()
extern void MainMenuManager__ctor_m9CF8E7F30CE180004AA0DFF1229FC615B531542B (void);
// 0x0000000A System.Void MultipleChoiceManager::Start()
extern void MultipleChoiceManager_Start_m2020191F9FEF1644829359D83C9BF0D173D03E6A (void);
// 0x0000000B System.Void MultipleChoiceManager::SetupQuiz()
extern void MultipleChoiceManager_SetupQuiz_m04F22CD6371976421999595B6A909BCDC0805E21 (void);
// 0x0000000C System.Void MultipleChoiceManager::CheckAnswer()
extern void MultipleChoiceManager_CheckAnswer_m00CAE7ED10B220C6D51E3AF339CEB4BCC27CE1DB (void);
// 0x0000000D System.Void MultipleChoiceManager::LoadNextLetter()
extern void MultipleChoiceManager_LoadNextLetter_m55BB385F020BB3E8F6D75012445B242A34ACB6AC (void);
// 0x0000000E System.Void MultipleChoiceManager::.ctor()
extern void MultipleChoiceManager__ctor_mB91A36C3F0CAE1A5C17626A1F1E39FA4197DDF26 (void);
// 0x0000000F System.Void GameManager::Awake()
extern void GameManager_Awake_m4B6E8E2AF58C95C9A2A0C4637A34AE0892CB637F (void);
// 0x00000010 System.Void GameManager::set_GameMode(GameMode)
extern void GameManager_set_GameMode_m471D2FA6B35B98D05C069CF1ECFF538A5FC05BB6 (void);
// 0x00000011 GameMode GameManager::get_GameMode()
extern void GameManager_get_GameMode_m6DFCBD83E0C1EDA9486397AE3205A131B3DCB676 (void);
// 0x00000012 System.Void GameManager::set_ThemeMode(ThemeMode)
extern void GameManager_set_ThemeMode_mDC4BF47BCEBC3588D608203937F5FFA358945DE7 (void);
// 0x00000013 ThemeMode GameManager::get_ThemeMode()
extern void GameManager_get_ThemeMode_m78FAE691D66354A25367219686168C3CE50FA335 (void);
// 0x00000014 System.Collections.Generic.List`1<BaybayinBase> GameManager::ConsonantsBaybayin()
extern void GameManager_ConsonantsBaybayin_m3A424C859ADDBBDC9B05E372865B58CEE908A8AC (void);
// 0x00000015 System.Collections.Generic.List`1<BaybayinBase> GameManager::VowelsBaybayin()
extern void GameManager_VowelsBaybayin_m393B423E6ACE436AD952EDC9C2B62C1941064238 (void);
// 0x00000016 System.Void GameManager::set_CurrentBaybayinIndex(System.Int32)
extern void GameManager_set_CurrentBaybayinIndex_mE7FD4D1BD3FDBB9FE02EC6DC7B559B2D553CCC85 (void);
// 0x00000017 System.Int32 GameManager::get_CurrentBaybayinIndex()
extern void GameManager_get_CurrentBaybayinIndex_mA4C35B78F8F64D61E89FFED787F67CDF26722992 (void);
// 0x00000018 System.Void GameManager::set_MaxBaybayinIndex(System.Int32)
extern void GameManager_set_MaxBaybayinIndex_m7A5EDA55997570EFFDC3BE438FF267BBA6FE96F0 (void);
// 0x00000019 System.Int32 GameManager::get_MaxBaybayinIndex()
extern void GameManager_get_MaxBaybayinIndex_m98F854F746FBF7F6E332B12EC5650A062E5B9350 (void);
// 0x0000001A System.Collections.Generic.List`1<BaybayinBase> GameManager::GetBaybayinBaseList()
extern void GameManager_GetBaybayinBaseList_mA1E5D7B5B4C6A596EE12D418060AEEAF6A015F92 (void);
// 0x0000001B System.Void GameManager::set_SelectedBaybayinBase(BaybayinBase)
extern void GameManager_set_SelectedBaybayinBase_m79770B297F69D86DD930E110106AFD306FABA480 (void);
// 0x0000001C BaybayinBase GameManager::get_SelectedBaybayinBase()
extern void GameManager_get_SelectedBaybayinBase_m1FB587A638DBB1A6BB189D4D5B45B89590A98B91 (void);
// 0x0000001D System.Void GameManager::HandleLoadedBaybayinList()
extern void GameManager_HandleLoadedBaybayinList_mD61C3ADCFB67F413AE4FC8CC3DBB373B3F3E692A (void);
// 0x0000001E System.Void GameManager::LoadNextBaybayin()
extern void GameManager_LoadNextBaybayin_m0679F727E0942B2533278FBAF4F1043531BF905C (void);
// 0x0000001F System.Void GameManager::ResetLoadedData()
extern void GameManager_ResetLoadedData_m1EABA355229827F40D52D74C4651BB964338A5F2 (void);
// 0x00000020 System.Void GameManager::.ctor()
extern void GameManager__ctor_mF453CED520617BFB65C52405A964E06CF17DB368 (void);
// 0x00000021 System.Void LessonManager::Start()
extern void LessonManager_Start_m4947A2415CB2CE46E0FCF11EC9380F1DA17C3FDE (void);
// 0x00000022 System.Void LessonManager::SetupBaybayinLetter()
extern void LessonManager_SetupBaybayinLetter_mA40A3E8D65569A42DC17BA887D9BF33B611E6482 (void);
// 0x00000023 System.Void LessonManager::LoadNextInfo()
extern void LessonManager_LoadNextInfo_mD82782F869497D8FB8AE8FF6D79658BB8DD68303 (void);
// 0x00000024 System.Void LessonManager::LoadNextLetter()
extern void LessonManager_LoadNextLetter_m46437C3FEA627CF9C7C50FB6F0A93B602630D777 (void);
// 0x00000025 System.Void LessonManager::.ctor()
extern void LessonManager__ctor_m44F408BFBBD41F97F7C06EAA77E4A6DC2D3ECDA1 (void);
// 0x00000026 System.Void LoadingManager::Awake()
extern void LoadingManager_Awake_m4054A0E1EACFF1401568416F28C049D49BB40F65 (void);
// 0x00000027 System.Void LoadingManager::Start()
extern void LoadingManager_Start_mDF557E72AA2D61B453930B5C3826ACD12B90B363 (void);
// 0x00000028 System.Void LoadingManager::OnDestroy()
extern void LoadingManager_OnDestroy_m206872C92E38F67F59F97C2DE95D3D7D4F4A5870 (void);
// 0x00000029 System.Void LoadingManager::OnSceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void LoadingManager_OnSceneLoaded_mE2A76A975F38E62C2D4616113A152CC7F817B39F (void);
// 0x0000002A System.Void LoadingManager::LoadScene(System.Int32)
extern void LoadingManager_LoadScene_mB81A8AB9DD5CE3B5E09ABD244E23EA1DC232CD7B (void);
// 0x0000002B System.Void LoadingManager::LoadScene(System.String)
extern void LoadingManager_LoadScene_m53272DE8E7A4E833C5BA21D7779B23EB6B337F22 (void);
// 0x0000002C System.Collections.Generic.IEnumerator`1<System.Single> LoadingManager::_LoadScene(System.Int32)
extern void LoadingManager__LoadScene_m693F40DCE68EBAFEFE3638836D6660F073A6CD82 (void);
// 0x0000002D System.Collections.Generic.IEnumerator`1<System.Single> LoadingManager::_LoadScene(System.String)
extern void LoadingManager__LoadScene_m209CFC219F38252442E941FC6F5E5640C3C3015C (void);
// 0x0000002E System.Collections.Generic.IEnumerator`1<System.Single> LoadingManager::_CanvasFade(LoadingManager/FadeType,System.Int32)
extern void LoadingManager__CanvasFade_m53128DF9151BA4521C532679E23F3719234353A3 (void);
// 0x0000002F System.Collections.Generic.IEnumerator`1<System.Single> LoadingManager::_CanvasFade(LoadingManager/FadeType,System.String)
extern void LoadingManager__CanvasFade_mF9C5F7FDDA481B510AAB57AA3E76758D6F79C2E0 (void);
// 0x00000030 System.String LoadingManager::GetActiveSceneName()
extern void LoadingManager_GetActiveSceneName_mB3F7B4A24B41A926B1DB5E33FB71BFA7E351F58D (void);
// 0x00000031 System.Void LoadingManager::.ctor()
extern void LoadingManager__ctor_m18973FD9B46C4E60C5BDDA03BB8E1EB5D8FE3C2B (void);
// 0x00000032 System.Void LoadingManager/<_LoadScene>d__9::.ctor(System.Int32)
extern void U3C_LoadSceneU3Ed__9__ctor_m6DEEF39F993665FD8912966FE6388F707F611631 (void);
// 0x00000033 System.Void LoadingManager/<_LoadScene>d__9::System.IDisposable.Dispose()
extern void U3C_LoadSceneU3Ed__9_System_IDisposable_Dispose_mF252F1287C8DD8322C1748E52363BCABF2E12387 (void);
// 0x00000034 System.Boolean LoadingManager/<_LoadScene>d__9::MoveNext()
extern void U3C_LoadSceneU3Ed__9_MoveNext_mA83077F75D94319DEFD617FD7E512C0DECFD753E (void);
// 0x00000035 System.Single LoadingManager/<_LoadScene>d__9::System.Collections.Generic.IEnumerator<System.Single>.get_Current()
extern void U3C_LoadSceneU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_m977EB076A23A442476E1736CEFC4D71AA152C12C (void);
// 0x00000036 System.Void LoadingManager/<_LoadScene>d__9::System.Collections.IEnumerator.Reset()
extern void U3C_LoadSceneU3Ed__9_System_Collections_IEnumerator_Reset_mD287D03ADCAFAA14C65BAE9C502D2753918E0226 (void);
// 0x00000037 System.Object LoadingManager/<_LoadScene>d__9::System.Collections.IEnumerator.get_Current()
extern void U3C_LoadSceneU3Ed__9_System_Collections_IEnumerator_get_Current_m5E10CE207AEB00D55E8315FA9D38ED1D4B896CDF (void);
// 0x00000038 System.Void LoadingManager/<_LoadScene>d__10::.ctor(System.Int32)
extern void U3C_LoadSceneU3Ed__10__ctor_m36594B1CF4A8068F4075134A52E6E8663371D769 (void);
// 0x00000039 System.Void LoadingManager/<_LoadScene>d__10::System.IDisposable.Dispose()
extern void U3C_LoadSceneU3Ed__10_System_IDisposable_Dispose_mDC3E14549AEF5F558938CC1D7FB158532EE13F90 (void);
// 0x0000003A System.Boolean LoadingManager/<_LoadScene>d__10::MoveNext()
extern void U3C_LoadSceneU3Ed__10_MoveNext_m8D266CC2A03D91CA6D97AF818E9F7B86B475B6E0 (void);
// 0x0000003B System.Single LoadingManager/<_LoadScene>d__10::System.Collections.Generic.IEnumerator<System.Single>.get_Current()
extern void U3C_LoadSceneU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_mAD47E35A55784C82149D736BF8C419C057A68305 (void);
// 0x0000003C System.Void LoadingManager/<_LoadScene>d__10::System.Collections.IEnumerator.Reset()
extern void U3C_LoadSceneU3Ed__10_System_Collections_IEnumerator_Reset_mC0D142F899A5BE6F9227BB2FB6B0684836779417 (void);
// 0x0000003D System.Object LoadingManager/<_LoadScene>d__10::System.Collections.IEnumerator.get_Current()
extern void U3C_LoadSceneU3Ed__10_System_Collections_IEnumerator_get_Current_m3BE51820EC52F3E4CFE61C8330637759709CE268 (void);
// 0x0000003E System.Void LoadingManager/<_CanvasFade>d__11::.ctor(System.Int32)
extern void U3C_CanvasFadeU3Ed__11__ctor_m126163D264C422D20ED467ECC97B0EA183F718AC (void);
// 0x0000003F System.Void LoadingManager/<_CanvasFade>d__11::System.IDisposable.Dispose()
extern void U3C_CanvasFadeU3Ed__11_System_IDisposable_Dispose_mE5BEBE9804B05B1F7556579CBBE79E0EBDCFF849 (void);
// 0x00000040 System.Boolean LoadingManager/<_CanvasFade>d__11::MoveNext()
extern void U3C_CanvasFadeU3Ed__11_MoveNext_m728E0CF956EBAC7B2F606028F83568B314517181 (void);
// 0x00000041 System.Single LoadingManager/<_CanvasFade>d__11::System.Collections.Generic.IEnumerator<System.Single>.get_Current()
extern void U3C_CanvasFadeU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_m972AEDB679D350637C1F7AF0CFF7F690F9098E11 (void);
// 0x00000042 System.Void LoadingManager/<_CanvasFade>d__11::System.Collections.IEnumerator.Reset()
extern void U3C_CanvasFadeU3Ed__11_System_Collections_IEnumerator_Reset_m22048D426A4B76B3EAE8BF502EE9C2BE0D3DA25F (void);
// 0x00000043 System.Object LoadingManager/<_CanvasFade>d__11::System.Collections.IEnumerator.get_Current()
extern void U3C_CanvasFadeU3Ed__11_System_Collections_IEnumerator_get_Current_m6257918634978F3DBB9771194D4154B1052AEE2F (void);
// 0x00000044 System.Void LoadingManager/<_CanvasFade>d__12::.ctor(System.Int32)
extern void U3C_CanvasFadeU3Ed__12__ctor_mEC3C07634A3AA4632ACE21FE1693B0E41B851F00 (void);
// 0x00000045 System.Void LoadingManager/<_CanvasFade>d__12::System.IDisposable.Dispose()
extern void U3C_CanvasFadeU3Ed__12_System_IDisposable_Dispose_mDF733F79F6A1C00A5E870F517F026F55B4BF1033 (void);
// 0x00000046 System.Boolean LoadingManager/<_CanvasFade>d__12::MoveNext()
extern void U3C_CanvasFadeU3Ed__12_MoveNext_m79114688005910DA71956D55709B85F92031CA45 (void);
// 0x00000047 System.Single LoadingManager/<_CanvasFade>d__12::System.Collections.Generic.IEnumerator<System.Single>.get_Current()
extern void U3C_CanvasFadeU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_m81E5B276C73C5F1628B643B5F2943E2F43F4DAA8 (void);
// 0x00000048 System.Void LoadingManager/<_CanvasFade>d__12::System.Collections.IEnumerator.Reset()
extern void U3C_CanvasFadeU3Ed__12_System_Collections_IEnumerator_Reset_mCF42F605ED4B0E7D0C21D9585107C9C00027C899 (void);
// 0x00000049 System.Object LoadingManager/<_CanvasFade>d__12::System.Collections.IEnumerator.get_Current()
extern void U3C_CanvasFadeU3Ed__12_System_Collections_IEnumerator_get_Current_m062029D1398765D3CC2A5741A8475440BC5F0E13 (void);
// 0x0000004A System.Void NavigationManager::Start()
extern void NavigationManager_Start_m28E0AB6F7905B8B02D78215B32B46FD2FC8CA325 (void);
// 0x0000004B System.Void NavigationManager::Update()
extern void NavigationManager_Update_m5B5ED4BFD2CC8133AD7FA4954B28574C7D8B6C3F (void);
// 0x0000004C System.Void NavigationManager::.ctor()
extern void NavigationManager__ctor_mD08DB46B078F5206C963B59B283D7A1948B1E9EE (void);
// 0x0000004D System.Void BaybayinBase::.ctor()
extern void BaybayinBase__ctor_mAA970A9C4000DE002DC944254730AC808015254E (void);
// 0x0000004E System.Void IllustrationsBase::.ctor()
extern void IllustrationsBase__ctor_mEC98838F985A8F2592BC58060CA15C6763DD31CF (void);
// 0x0000004F System.Void MultipleChoiceScriptable::.ctor()
extern void MultipleChoiceScriptable__ctor_mD77E14C0A6BBB74712D72DDDE78330FF02FD2905 (void);
// 0x00000050 UnityEngine.Vector3 ExtendedLerp::EaseOut(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void ExtendedLerp_EaseOut_m511D479BDCF93053BBF74CDD76F63C173B6E8E9C (void);
// 0x00000051 UnityEngine.Vector3 ExtendedLerp::EaseIn(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void ExtendedLerp_EaseIn_m3914D2F2103B31303CAB2426C704D150E0C3D822 (void);
// 0x00000052 UnityEngine.Vector3 ExtendedLerp::Smoothstep(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void ExtendedLerp_Smoothstep_m65650F55B2FE12070BB03A3A4DF349B342B5E6EF (void);
// 0x00000053 UnityEngine.Vector4 ExtendedLerp::Smootherstep(UnityEngine.Vector4,UnityEngine.Vector4,System.Single)
extern void ExtendedLerp_Smootherstep_m5ABAE74F29E0D2A97E00150E1CDAD5ABE09DEFD5 (void);
// 0x00000054 UnityEngine.Vector3 ExtendedLerp::Smootherstep(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void ExtendedLerp_Smootherstep_m021304A37439A213C6DFB21E5B4605E1EB1CC7E9 (void);
// 0x00000055 System.Single ExtendedLerp::Smoothstep(System.Single,System.Single,System.Single)
extern void ExtendedLerp_Smoothstep_m80BB86A8354BC3B563B9F5B85AC2D0E0D6E1A87C (void);
// 0x00000056 System.Single ExtendedLerp::Smootherstep(System.Single,System.Single,System.Single)
extern void ExtendedLerp_Smootherstep_m4A97A07E0D88AF5E67DD68CFAED9FEC74B919D6C (void);
// 0x00000057 System.Void Shuffler::Shuffle(System.Collections.Generic.IList`1<T>)
// 0x00000058 System.Void Shuffler::.cctor()
extern void Shuffler__cctor_mC98D3C5FDF9F0A27731530DF7C89CE50CE6C5BAC (void);
// 0x00000059 System.Void SplashScreenManager::Start()
extern void SplashScreenManager_Start_m35F83B6A1DFA039FCEA68E10ACA5C964A2DC8C36 (void);
// 0x0000005A System.Collections.Generic.IEnumerator`1<System.Single> SplashScreenManager::_ShowSponsors()
extern void SplashScreenManager__ShowSponsors_mFF9D8C300C825A1D06010F5B325A015CE1E66570 (void);
// 0x0000005B System.Void SplashScreenManager::.ctor()
extern void SplashScreenManager__ctor_m6E8994BA463F50F00B37339D3CCFAAF0B874BE37 (void);
// 0x0000005C System.Void SplashScreenManager/<_ShowSponsors>d__2::.ctor(System.Int32)
extern void U3C_ShowSponsorsU3Ed__2__ctor_m3CD1B81B28306061B8F73EC07B308971EB17384B (void);
// 0x0000005D System.Void SplashScreenManager/<_ShowSponsors>d__2::System.IDisposable.Dispose()
extern void U3C_ShowSponsorsU3Ed__2_System_IDisposable_Dispose_m358612FBD807BF30793353BBFCA5DF79B599D0BC (void);
// 0x0000005E System.Boolean SplashScreenManager/<_ShowSponsors>d__2::MoveNext()
extern void U3C_ShowSponsorsU3Ed__2_MoveNext_mEA4C1972C8DB8047E0547C5B6ECC9E4FF41A93CC (void);
// 0x0000005F System.Single SplashScreenManager/<_ShowSponsors>d__2::System.Collections.Generic.IEnumerator<System.Single>.get_Current()
extern void U3C_ShowSponsorsU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_m23ACA2B0296A671A297C3462411B366D9711BB72 (void);
// 0x00000060 System.Void SplashScreenManager/<_ShowSponsors>d__2::System.Collections.IEnumerator.Reset()
extern void U3C_ShowSponsorsU3Ed__2_System_Collections_IEnumerator_Reset_mAB8F65B501B4E772FA40137F6F9D153F41D3F68C (void);
// 0x00000061 System.Object SplashScreenManager/<_ShowSponsors>d__2::System.Collections.IEnumerator.get_Current()
extern void U3C_ShowSponsorsU3Ed__2_System_Collections_IEnumerator_get_Current_m2EE039D0DDAB6292B9647CA29B6A556545F1039E (void);
static Il2CppMethodPointer s_methodPointers[97] = 
{
	IntroManager_Start_m72CB22556491D6380E44B3BC8E9D17B5D91566B6,
	IntroManager_OpenLink_m8C3E8B5AEE139BB64FA53C2290FCD53420D57236,
	IntroManager_NextContent_m018CE0746537775F722812E9D9156B376E7EAD07,
	IntroManager_GoHome_m5B132DCB9BA21B6AAB75C77560231B7374DE56B5,
	IntroManager__ctor_m9FA97A0D02CBF5D1D6B050A1779F3439A7C56D1F,
	MainMenuManager_LoadIntro_mB76D04BCA87137A7190F7D256EC7FA5BDD6D4634,
	MainMenuManager_LoadVowels_m51B4E5840D3347F0E394913DDE8E41349F87874D,
	MainMenuManager_LoadConsonants_mBF9D09EED8777670E91DA5332497D255AE93C067,
	MainMenuManager__ctor_m9CF8E7F30CE180004AA0DFF1229FC615B531542B,
	MultipleChoiceManager_Start_m2020191F9FEF1644829359D83C9BF0D173D03E6A,
	MultipleChoiceManager_SetupQuiz_m04F22CD6371976421999595B6A909BCDC0805E21,
	MultipleChoiceManager_CheckAnswer_m00CAE7ED10B220C6D51E3AF339CEB4BCC27CE1DB,
	MultipleChoiceManager_LoadNextLetter_m55BB385F020BB3E8F6D75012445B242A34ACB6AC,
	MultipleChoiceManager__ctor_mB91A36C3F0CAE1A5C17626A1F1E39FA4197DDF26,
	GameManager_Awake_m4B6E8E2AF58C95C9A2A0C4637A34AE0892CB637F,
	GameManager_set_GameMode_m471D2FA6B35B98D05C069CF1ECFF538A5FC05BB6,
	GameManager_get_GameMode_m6DFCBD83E0C1EDA9486397AE3205A131B3DCB676,
	GameManager_set_ThemeMode_mDC4BF47BCEBC3588D608203937F5FFA358945DE7,
	GameManager_get_ThemeMode_m78FAE691D66354A25367219686168C3CE50FA335,
	GameManager_ConsonantsBaybayin_m3A424C859ADDBBDC9B05E372865B58CEE908A8AC,
	GameManager_VowelsBaybayin_m393B423E6ACE436AD952EDC9C2B62C1941064238,
	GameManager_set_CurrentBaybayinIndex_mE7FD4D1BD3FDBB9FE02EC6DC7B559B2D553CCC85,
	GameManager_get_CurrentBaybayinIndex_mA4C35B78F8F64D61E89FFED787F67CDF26722992,
	GameManager_set_MaxBaybayinIndex_m7A5EDA55997570EFFDC3BE438FF267BBA6FE96F0,
	GameManager_get_MaxBaybayinIndex_m98F854F746FBF7F6E332B12EC5650A062E5B9350,
	GameManager_GetBaybayinBaseList_mA1E5D7B5B4C6A596EE12D418060AEEAF6A015F92,
	GameManager_set_SelectedBaybayinBase_m79770B297F69D86DD930E110106AFD306FABA480,
	GameManager_get_SelectedBaybayinBase_m1FB587A638DBB1A6BB189D4D5B45B89590A98B91,
	GameManager_HandleLoadedBaybayinList_mD61C3ADCFB67F413AE4FC8CC3DBB373B3F3E692A,
	GameManager_LoadNextBaybayin_m0679F727E0942B2533278FBAF4F1043531BF905C,
	GameManager_ResetLoadedData_m1EABA355229827F40D52D74C4651BB964338A5F2,
	GameManager__ctor_mF453CED520617BFB65C52405A964E06CF17DB368,
	LessonManager_Start_m4947A2415CB2CE46E0FCF11EC9380F1DA17C3FDE,
	LessonManager_SetupBaybayinLetter_mA40A3E8D65569A42DC17BA887D9BF33B611E6482,
	LessonManager_LoadNextInfo_mD82782F869497D8FB8AE8FF6D79658BB8DD68303,
	LessonManager_LoadNextLetter_m46437C3FEA627CF9C7C50FB6F0A93B602630D777,
	LessonManager__ctor_m44F408BFBBD41F97F7C06EAA77E4A6DC2D3ECDA1,
	LoadingManager_Awake_m4054A0E1EACFF1401568416F28C049D49BB40F65,
	LoadingManager_Start_mDF557E72AA2D61B453930B5C3826ACD12B90B363,
	LoadingManager_OnDestroy_m206872C92E38F67F59F97C2DE95D3D7D4F4A5870,
	LoadingManager_OnSceneLoaded_mE2A76A975F38E62C2D4616113A152CC7F817B39F,
	LoadingManager_LoadScene_mB81A8AB9DD5CE3B5E09ABD244E23EA1DC232CD7B,
	LoadingManager_LoadScene_m53272DE8E7A4E833C5BA21D7779B23EB6B337F22,
	LoadingManager__LoadScene_m693F40DCE68EBAFEFE3638836D6660F073A6CD82,
	LoadingManager__LoadScene_m209CFC219F38252442E941FC6F5E5640C3C3015C,
	LoadingManager__CanvasFade_m53128DF9151BA4521C532679E23F3719234353A3,
	LoadingManager__CanvasFade_mF9C5F7FDDA481B510AAB57AA3E76758D6F79C2E0,
	LoadingManager_GetActiveSceneName_mB3F7B4A24B41A926B1DB5E33FB71BFA7E351F58D,
	LoadingManager__ctor_m18973FD9B46C4E60C5BDDA03BB8E1EB5D8FE3C2B,
	U3C_LoadSceneU3Ed__9__ctor_m6DEEF39F993665FD8912966FE6388F707F611631,
	U3C_LoadSceneU3Ed__9_System_IDisposable_Dispose_mF252F1287C8DD8322C1748E52363BCABF2E12387,
	U3C_LoadSceneU3Ed__9_MoveNext_mA83077F75D94319DEFD617FD7E512C0DECFD753E,
	U3C_LoadSceneU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_m977EB076A23A442476E1736CEFC4D71AA152C12C,
	U3C_LoadSceneU3Ed__9_System_Collections_IEnumerator_Reset_mD287D03ADCAFAA14C65BAE9C502D2753918E0226,
	U3C_LoadSceneU3Ed__9_System_Collections_IEnumerator_get_Current_m5E10CE207AEB00D55E8315FA9D38ED1D4B896CDF,
	U3C_LoadSceneU3Ed__10__ctor_m36594B1CF4A8068F4075134A52E6E8663371D769,
	U3C_LoadSceneU3Ed__10_System_IDisposable_Dispose_mDC3E14549AEF5F558938CC1D7FB158532EE13F90,
	U3C_LoadSceneU3Ed__10_MoveNext_m8D266CC2A03D91CA6D97AF818E9F7B86B475B6E0,
	U3C_LoadSceneU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_mAD47E35A55784C82149D736BF8C419C057A68305,
	U3C_LoadSceneU3Ed__10_System_Collections_IEnumerator_Reset_mC0D142F899A5BE6F9227BB2FB6B0684836779417,
	U3C_LoadSceneU3Ed__10_System_Collections_IEnumerator_get_Current_m3BE51820EC52F3E4CFE61C8330637759709CE268,
	U3C_CanvasFadeU3Ed__11__ctor_m126163D264C422D20ED467ECC97B0EA183F718AC,
	U3C_CanvasFadeU3Ed__11_System_IDisposable_Dispose_mE5BEBE9804B05B1F7556579CBBE79E0EBDCFF849,
	U3C_CanvasFadeU3Ed__11_MoveNext_m728E0CF956EBAC7B2F606028F83568B314517181,
	U3C_CanvasFadeU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_m972AEDB679D350637C1F7AF0CFF7F690F9098E11,
	U3C_CanvasFadeU3Ed__11_System_Collections_IEnumerator_Reset_m22048D426A4B76B3EAE8BF502EE9C2BE0D3DA25F,
	U3C_CanvasFadeU3Ed__11_System_Collections_IEnumerator_get_Current_m6257918634978F3DBB9771194D4154B1052AEE2F,
	U3C_CanvasFadeU3Ed__12__ctor_mEC3C07634A3AA4632ACE21FE1693B0E41B851F00,
	U3C_CanvasFadeU3Ed__12_System_IDisposable_Dispose_mDF733F79F6A1C00A5E870F517F026F55B4BF1033,
	U3C_CanvasFadeU3Ed__12_MoveNext_m79114688005910DA71956D55709B85F92031CA45,
	U3C_CanvasFadeU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_m81E5B276C73C5F1628B643B5F2943E2F43F4DAA8,
	U3C_CanvasFadeU3Ed__12_System_Collections_IEnumerator_Reset_mCF42F605ED4B0E7D0C21D9585107C9C00027C899,
	U3C_CanvasFadeU3Ed__12_System_Collections_IEnumerator_get_Current_m062029D1398765D3CC2A5741A8475440BC5F0E13,
	NavigationManager_Start_m28E0AB6F7905B8B02D78215B32B46FD2FC8CA325,
	NavigationManager_Update_m5B5ED4BFD2CC8133AD7FA4954B28574C7D8B6C3F,
	NavigationManager__ctor_mD08DB46B078F5206C963B59B283D7A1948B1E9EE,
	BaybayinBase__ctor_mAA970A9C4000DE002DC944254730AC808015254E,
	IllustrationsBase__ctor_mEC98838F985A8F2592BC58060CA15C6763DD31CF,
	MultipleChoiceScriptable__ctor_mD77E14C0A6BBB74712D72DDDE78330FF02FD2905,
	ExtendedLerp_EaseOut_m511D479BDCF93053BBF74CDD76F63C173B6E8E9C,
	ExtendedLerp_EaseIn_m3914D2F2103B31303CAB2426C704D150E0C3D822,
	ExtendedLerp_Smoothstep_m65650F55B2FE12070BB03A3A4DF349B342B5E6EF,
	ExtendedLerp_Smootherstep_m5ABAE74F29E0D2A97E00150E1CDAD5ABE09DEFD5,
	ExtendedLerp_Smootherstep_m021304A37439A213C6DFB21E5B4605E1EB1CC7E9,
	ExtendedLerp_Smoothstep_m80BB86A8354BC3B563B9F5B85AC2D0E0D6E1A87C,
	ExtendedLerp_Smootherstep_m4A97A07E0D88AF5E67DD68CFAED9FEC74B919D6C,
	NULL,
	Shuffler__cctor_mC98D3C5FDF9F0A27731530DF7C89CE50CE6C5BAC,
	SplashScreenManager_Start_m35F83B6A1DFA039FCEA68E10ACA5C964A2DC8C36,
	SplashScreenManager__ShowSponsors_mFF9D8C300C825A1D06010F5B325A015CE1E66570,
	SplashScreenManager__ctor_m6E8994BA463F50F00B37339D3CCFAAF0B874BE37,
	U3C_ShowSponsorsU3Ed__2__ctor_m3CD1B81B28306061B8F73EC07B308971EB17384B,
	U3C_ShowSponsorsU3Ed__2_System_IDisposable_Dispose_m358612FBD807BF30793353BBFCA5DF79B599D0BC,
	U3C_ShowSponsorsU3Ed__2_MoveNext_mEA4C1972C8DB8047E0547C5B6ECC9E4FF41A93CC,
	U3C_ShowSponsorsU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_SingleU3E_get_Current_m23ACA2B0296A671A297C3462411B366D9711BB72,
	U3C_ShowSponsorsU3Ed__2_System_Collections_IEnumerator_Reset_mAB8F65B501B4E772FA40137F6F9D153F41D3F68C,
	U3C_ShowSponsorsU3Ed__2_System_Collections_IEnumerator_get_Current_m2EE039D0DDAB6292B9647CA29B6A556545F1039E,
};
static const int32_t s_InvokerIndices[97] = 
{
	3867,
	3867,
	3867,
	3867,
	3867,
	3867,
	3867,
	3867,
	3867,
	3867,
	3867,
	3867,
	3867,
	3867,
	3867,
	3093,
	3752,
	3093,
	3752,
	3771,
	3771,
	3093,
	3752,
	3093,
	3752,
	3771,
	3110,
	3771,
	3867,
	3867,
	3867,
	3867,
	3867,
	3867,
	3867,
	3867,
	3867,
	3867,
	3867,
	3867,
	1722,
	3093,
	3110,
	2311,
	2313,
	1150,
	1151,
	3771,
	3867,
	3093,
	3867,
	3803,
	3808,
	3867,
	3771,
	3093,
	3867,
	3803,
	3808,
	3867,
	3771,
	3093,
	3867,
	3803,
	3808,
	3867,
	3771,
	3093,
	3867,
	3803,
	3808,
	3867,
	3771,
	3867,
	3867,
	3867,
	3867,
	3867,
	3867,
	4934,
	4934,
	4934,
	4936,
	4934,
	4914,
	4914,
	0,
	7237,
	3867,
	3771,
	3867,
	3093,
	3867,
	3803,
	3808,
	3867,
	3771,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x06000057, { 0, 5 } },
};
extern const uint32_t g_rgctx_ICollection_1_tA20C5063C67A3FEC8279EDECAB995D1A2200C0F3;
extern const uint32_t g_rgctx_ICollection_1_get_Count_mE2DD8F3284949093F7A6DA0F35E35DEC0A94051B;
extern const uint32_t g_rgctx_IList_1_t763D3E1E5ED1F9DF552B46FAF67B0E92A2E39C2F;
extern const uint32_t g_rgctx_IList_1_get_Item_mD71FE8554317C557836F116726C48DAAD009A663;
extern const uint32_t g_rgctx_IList_1_set_Item_m1D4F47F2A39F6AFC43C582A2D9684D0E7D59C1BF;
static const Il2CppRGCTXDefinition s_rgctxValues[5] = 
{
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_ICollection_1_tA20C5063C67A3FEC8279EDECAB995D1A2200C0F3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ICollection_1_get_Count_mE2DD8F3284949093F7A6DA0F35E35DEC0A94051B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IList_1_t763D3E1E5ED1F9DF552B46FAF67B0E92A2E39C2F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IList_1_get_Item_mD71FE8554317C557836F116726C48DAAD009A663 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IList_1_set_Item_m1D4F47F2A39F6AFC43C582A2D9684D0E7D59C1BF },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	97,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	5,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
